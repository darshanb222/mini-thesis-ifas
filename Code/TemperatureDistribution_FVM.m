function [T,T_bar] = TemperatureDistribution_FVM(P,Z_half,n,m)
% TemperatureDistribution  Compute temperature distribution in the
% discretized grid for x, y and z using FVM
% The conduction in x and y are neglected
% Along z temperaure is approximated by approximation polynomial using body
% temperaure and average fluid temperature.
%
% Input P is pressure distribution in grid. Each row is for particular
% theta and each column is for particular y. Along z i.e gap height
% presuure is constant.
% Input Z_half is height variation with theta at middle of cell, 
% discretized into divisions. Each row of Z is for particular theta.
%
R = 10e-3;          % Radius of Journal(inner) in m
theta_end = 2*pi;   % angular domain in radians
Y_end = 10e-3;      % axial domain in m
nn = n*(m+2);
rho_bar = 828.70;   % Density in Kg/m3
lamda_bar = 0.1219; % Heat conductivity in [ W / (mK) ]
C_p_bar = 2000;     % Heat capacity cp(T) [J/kggrd]
eta = 0.013;        % dynamics viscosity in Pascal second
rps = 1050;            % rotation speed in rpm
U = R*rps;          % Velocity of journal
C = 10e-6;          % Clearance in m
e_r = 0.9;          % eccentricity ratio
e = e_r*C;          % eccentricity

del_theta = theta_end/n;
del_y = Y_end/m;
fun_h = @(theta)(e*cos(theta) + C);
theta = 0:del_theta:theta_end;
h_theta = fun_h(theta)';

T_1 = 303;          % Inner body temperature in Kelvin
T_2 = 298;          % Outer body temperature in Kelvin
T_amb = 298;        % Ambient temperature in Kelvin
T_bar = 305*ones(n,m);     % Initial averaged temperature of fluid. Each row is for particular theta and each column is for particular y
tolerence = ones(size(T_bar,1),size(T_bar,2));

theta_half = zeros(size(theta,2)-1,1);
for i=1:length(theta)-1
    theta_half(i) = (theta(i+1)+theta(i))/2;
end
half_h = fun_h(theta_half);

T_bar_old = T_bar;
% Approximation of temperature profile in gap height
T_Z_fun = @(z,h,T_b)(((3/(h^2))*((-2*T_b)+T_1+T_2)*(z^2))+((2/h)*((3*T_b)-(2*T_1)-T_2)*z)+T_1);
T = zeros(n,size(Z_half,2),m);
for i=1:n   % for each theta
    for j=1:size(Z_half,2)   % for each z
        for k=1:m   % for each y
            T(i,j,k) = T_Z_fun(Z_half(i,j),half_h(i),T_bar(i,k)); % each row is for particular theta and column is for different z values and k for different y
        end
    end
end

% Calculation of temperature gradient in gap height with limits for
% averaged energy equation
deltaTdeltaZLimits_fun = @(h,T_b)((6/h)*((-2*T_b)+T_1+T_2));
dTdZ = zeros(n,m);
for i = 1:n
    for k = 1:m
        dTdZ(i,k) = deltaTdeltaZLimits_fun(half_h(i),T_bar(i,k)); % Temperature gradient for different theta values and y vales. i for theta and k for y
    end
end

% Calculation of pressure gradient in theta and y at nodal points
[deltaP_deltaTheta,deltaP_deltaY] = gradient(P',del_theta,del_y);
deltaP_deltaTheta = deltaP_deltaTheta';
deltaP_deltaY = deltaP_deltaY';
% Calculation of pressure gradient at faces
deltaP_deltaTheta_face = 0.5 * (deltaP_deltaTheta (:,1: end-1) + deltaP_deltaTheta (:,2: end));
deltaP_deltaY_face = 0.5 * (deltaP_deltaY (1: end-1,:) + deltaP_deltaY (2: end,:));
dP_dTheta_centre = 0.5 * (deltaP_deltaTheta_face (1: end-1,:) + deltaP_deltaTheta_face (2: end,:));
% Calculation of averaged u and v velocity at faces
% u_bar is u velocity at east and west faces of each cell
% v_bar is v velocity at north and south faces of each cell
u_bar = zeros(n+1,m);
v_bar = zeros(n,m+1);
for i = 1:n+1
    for k = 1:m
        u_bar(i,k) = (U/2)-(((h_theta(i)^2)/(12*eta*R))*(deltaP_deltaTheta_face(i,k)));
    end
end
for i = 1:n
    for k = 1:m+1
        v_bar(i,k) = -1*(((h_theta(i))^2)/(12*eta))*deltaP_deltaY_face(i,k);
    end
end

%Calculation of source term at cell centre
S = zeros(n,m+2);
for i =1:n
    for k = 2:m+1
       S(i,k) = ((eta*(U^2))/(rho_bar*(half_h(i)^2)*C_p_bar))+((lamda_bar*dTdZ(i,k-1))/(rho_bar*half_h(i)*C_p_bar))+(((half_h(i)*dP_dTheta_centre(i,k-1))^2)/(12*eta*rho_bar*C_p_bar*(R^2)));
    end
end
% Allocate memory and initialization of co-efficients for temperature
Aw = zeros(n,m+2);
An = zeros(n,m+2);
As = zeros(n,m+2);
Ap = zeros(n,m+2);
B = S*del_theta*R*del_y;
for i=1:n
    for j=2:m+1
        Ap(i,j) = u_bar(i+1,j-1)*del_y;
        Aw(i,j) = -1*u_bar(i,j-1)*del_y;
    end
end
for i=1:n
    for j=2:m+1
        if v_bar(i,j)>0
            Ap(i,j) = Ap(i,j)+(v_bar(i,j)*R*del_theta);
        else
            An(i,j) = v_bar(i,j)*R*del_theta;
        end
    end
end
for i=1:n
    for j=2:m+1
        if v_bar(i,j-1)>0
            As(i,j) = -1*v_bar(i,j-1)*R*del_theta;
        else
            Ap(i,j) = Ap(i,j)+(-1*v_bar(i,j-1)*R*del_theta);
        end
    end
end


% Adjustment of coeeficients for top and bottom boundary
% For bottom boundary
for i=1:n
    Ap(i,1) = 1;
    B(i,1) = T_amb;       % Setting of temperature of ambient in Kelvin
end
% For top boundary
for i=1:n
    Ap(i,m+2) = 1;
    B(i,m+2) = T_amb;     % Setting of temperature of ambient in Kelvin
end
Aw = reshape(Aw,nn,1);
As = reshape(As,nn,1);
An = reshape(An,nn,1);
Ap = reshape(Ap,nn,1);
B = reshape(B,nn,1);

% Setting up coeefficient matrix
A = zeros(nn,nn);
for i=1:n
    A(i,i) = Ap(i);
end
for i = nn-(n+1):nn
    A(i,i) = Ap(i);
end
for i = n+1:nn-n
    if rem(i,n+1) == 1
        A(i,i) = Ap(i);
        A(i,i-n) = As(i);
        A(i,i+(n-1)) = Aw(i);
        A(i,i+n) = An(i);
    else
        A(i,i) = Ap(i);
        A(i,i-n) = As(i);
        A(i,i-1) = Aw(i);
        A(i,i+n) = An(i);
    end
end

%scaling the values
% [P1,R1,C1] = equilibrate(A);
% A1 = R1*P1*A*C1;
% B1 = R1*P1*B;

T_bar = Solve(A,B);  % Solve for temperature field at internal nodes
T_bar = reshape(T_bar,n,m+2); % colums for y and rows for theta


end