function [T,T_bar] = TemperatureDistribution_ByVelocityAverage(P,u,v,Z_half,Z,n,m)
% TemperatureDistribution  Compute temperature distribution in the
% discretized grid for x and y and z
% Along z temperaure is approximated by approximation polynomial using body
% temperaure and average fluid temperature.
%
% Input u velocity values in i is for theta, j is for z, k is for y
%
% Input v velocity values in i is for theta, j is for z, k is for y
%
% Input Z_half is height variation at mid points of theta. This is used for
% u and deltaP_deltaTheta. Each row if for particular theta
%
% Input Z is height variation with theta, discretized into divisions. Each
% row of Z is for particular theta.
%
R = 10e-3;          % Radius of Journal(inner) in m
theta_end = 2*pi;   % angular domain in radians
Y_end = 10e-3;      % axial domain in m
nn = n*(m+2);
rho_bar = 828.70;   % Density in Kg/m3
lamda_bar = 0.1219; % Heat conductivity in [ W / (mK) ]
C_p_bar = 2000;     % Heat capacity cp(T) [J/kggrd]
eta = 0.013;        % dynamics viscosity in Pascal second
rps = 1050;            % rotation speed in radians per second
U = R*rps;          % Velocity of journal
C = 10e-6;          % Clearance in m
e_r = 0.9;          % eccentricity ratio
e = e_r*C;          % eccentricity

del_theta = theta_end/n;
del_y = Y_end/m;
fun_h = @(theta)(e*cos(theta) + C);
theta = 0:del_theta:theta_end;
h_theta = fun_h(theta)';
theta_half = zeros(size(theta,2)-1,1);
for i=1:length(theta)-1
    theta_half(i) = (theta(i+1)+theta(i))/2;
end
half_h = fun_h(theta_half);

T_1 = 303;          % Inner body temperature in Kelvin
T_2 = 298;          % Outer body temperature in Kelvin
T_amb = 298;        % Ambient temperature in Kelvin
T_bar = 305*ones(n,m+2);     % Initial averaged temperature of fluid. Each row is for particular theta and each column is for particular y
tolerence = ones(size(T_bar,1),size(T_bar,2));


T_bar_old = T_bar;
% Approximation of temperature profile in gap height
T_Z_fun = @(z,h,T_b)(((3/(h^2))*((-2*T_b)+T_1+T_2)*(z^2))+((2/h)*((3*T_b)-(2*T_1)-T_2)*z)+T_1);
T = zeros(n,size(Z_half,2),m);
for i=1:n   % for each theta
    for j=1:size(Z_half,2)   % for each z
        for k=1:m   % for each y
            T(i,j,k) = T_Z_fun(Z_half(i,j),half_h(i),T_bar(i,k)); % each row is for particular theta and column is for different z values and k for different y
        end
    end
end

% Calculation of temperature gradient in gap height with limits for
% averaged energy equation
deltaTdeltaZLimits_fun = @(h,T_b)((6/h)*((-2*T_b)+T_1+T_2));
dTdZ = zeros(n,m);
for i = 1:n
    for k = 1:m
        dTdZ(i,k) = deltaTdeltaZLimits_fun(half_h(i),T_bar(i,k)); % Temperature gradient for different theta values and y vales. i for theta and k for y
    end
end

% Calculation of pressure gradient in theta
deltaP_deltaTheta = zeros(n,m+1);
deltaP_deltaTheta_half = zeros(n,m);
for i = 1:n
    for j = 1:m+1
        deltaP_deltaTheta(i,j) = (P(i+1,j)-P(i,j))/(del_theta);
    end
end
for i = 1:n
    for j = 1:m
        deltaP_deltaTheta_half(i,j) = (deltaP_deltaTheta(i,j+1)+deltaP_deltaTheta(i,j))/2;
    end
end
% Calculation of u and v average
u_bar = zeros(n,m+1);
u_bar_half = zeros(n,m);
for i = 1:n
    for k = 1:m+1
        u_bar(i,k) = sum(u(i,:,k))/size(u,2);
    end
end
for i = 1:n
    for k = 1:m
        u_bar_half(i,k) = (u_bar(i,k)+u_bar(i,k+1))/2;
    end
end
v_bar = zeros(n+1,m);
v_bar_half = zeros(n,m);
for i = 1:n+1
    for k = 1:m
        v_bar(i,k) = sum(v(i,k,:))/size(v,2);
    end
end
for i = 1:n
    for k = 1:m
        v_bar_half(i,k) = (v_bar(i,k)+v_bar(i+1,k))/2;
    end
end
% Allocate memory and initialization of co-efficients for temperature
Ae = zeros(n,m+2);
An = zeros(n,m+2);
As = zeros(n,m+2);
Ap = ones(n,m+2);
B = zeros(n,m+2);

Ae_fun = @(h,u)((rho_bar*C_p_bar*h*u)/(R*del_theta));
An_fun = @(h,v)((rho_bar*C_p_bar*h*v)/(2*del_y));
B_fun = @(h,dp_dx,dt_dz)(((eta/h)*(U^2))+(((h^3)/(12*eta*(R^2)))*(dp_dx^2))+(lamda_bar*dt_dz));

for i=1:n
    for j=2:m+1
        Ae(i,j) = -1;
        An(i,j) = (-1*An_fun(half_h(i),v_bar_half(i,j-1)))/Ae_fun(half_h(i),u_bar_half(i,j-1));
        As(i,j) = -An(i,j);
        B(i,j) = (-1*B_fun(half_h(i),deltaP_deltaTheta_half(i,j-1),dTdZ(i,j-1)))/Ae_fun(half_h(i),u_bar_half(i,j-1));
    end
end
% Adjustment of coeeficients for top and bottom boundary
% For bottom boundary
for i=1:n
    B(i,1) = T_amb;       % Setting of temperature of ambient in Kelvin
    As(i,2) = 2*As(i,2);
end
% For top boundary
for i=1:n
    B(i,m+2) = T_amb;     % Setting of temperature of ambient in Kelvin
    An(i,m+1) = 2*An(i,m+1);
end

Ae = reshape(Ae,n*(m+2),1);
As = reshape(As,n*(m+2),1);
Ap = reshape(Ap,n*(m+2),1);
B = reshape(B,n*(m+2),1);

% Setting up coeefficient matrix
A = zeros(nn,nn);
for i=1:n
    A(i,i) = Ap(i);
end
for i = (nn-n)+1:nn
    A(i,i) = Ap(i);
end
for i = n+1:nn-n
    if rem(i,n) == 0
        A(i,i) = Ap(i);
        A(i,i-(n-1)) = Ae(i);
        A(i,i-n) = As(i);
        A(i,i+n) = An(i);
    else
        A(i,i) = Ap(i);
        A(i,i+1) = Ae(i);
        A(i,i-n) = As(i);
        A(i,i+n) = An(i);
    end
end

[P1,R1,C1] = equilibrate(A);
A1 = R1*P1*A*C1;
B1 = R1*P1*B;

T_bar = A1\B1;  % Solve for temperature field at internal nodes
T_bar = reshape(T_bar,n,m+2); % colums for y and rows for theta

end