function [x,xu] = UniformMesh(nx,xlen)
% fvUniformMesh  Compute finite-volume grid variables for a uniform mesh.
%
% Input:     nx   = number of control volumes in the x direction
%            xlen = length of domain in the x direction
%
% Output:    x = vector of x-coordinates for nodes in center of *interior* control volumes
%                x(1) is the x,-coordinate of the *first interior* CV, not the boundary
%                node at x=0.  For convenience in computing the control-volume
%                finite-difference coefficients, the location of the node on the "far" (east)
%                boundary stored as x(nx+1).  Thus, the number of elements in x is nx+1,
%                and x(nx+1) = xlen.
%            xu = vector of x-coordinates of the control volume faces.  xu(i) is the
%                 location of the control volume face to the left (negative x-direction)
%                 of a node at x(i).  xu(1)=0.  For convenience, xu(nx+1) = xlen
%
%                    x(i)
%               |---------->
%                      |         |       Vertical lines are the C.V. faces.
%                      |    o    |       "o" is location of node that is centered
%                      |         |       between the cell faces
%               |----->
%                 xu(i)
%
%  Note:  This routine can be used to define grids in any coordinate direction, e.g.,
%             [x,xu] = UniformMesh(nx,xLen);
%             [y,yv] = UniformMesh(ny,yLen);
%             [z,zw] = UniformMesh(nz,zLen);

dx = xlen/nx;               %  Locate Ccontrol Volume faces, xu
xu = (0:dx:xlen)';          %  Column vector of nx+1 values equally spaced by dx
x = [ 0.5*( xu(2:nx+1) + xu(1:nx) ); xu(nx+1)];   %  x are located halfway between xu, except x(nx+1)

end