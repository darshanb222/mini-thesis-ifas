function X = Solve(A,B)
X=zeros(size(B,1),1);
X_init=305*ones(size(B,1),1);
error=ones(size(B,1),1);
iteration=0;
while(max(error)>.0001)
    iteration = iteration+1;
    
    X = X_init - (A\((A*X_init)-(B)));
    
    error = X - X_init;
    if iteration >100
        break;
    end
    X_init = X;
end
end
