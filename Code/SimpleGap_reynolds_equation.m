%  Implementation of Reynold's Equation
clear all;
clc;
%% Simple rectangular gap definition
PIn = 1000;                                % Inlet Pressure in bar
POut = 0;                                  % Outlet Pressure in bar

nx = 20;    % Number of control volumes in the x direction.
ny = 5;     % Number of control volumes in the y direction.
Lx = 20;    % Length of domain in the x direction.
Ly = 5;     % Length of domain in the y direction.
nn = nx*ny;

%% --------Mesh generation
[x,xu] = UniformMesh(nx,Lx);
[y,yv] = UniformMesh(ny,Ly);
%% --------Boundary data structure
ebc = zeros(ny,5);
wbc = ebc;
nbc = zeros(nx,5);
sbc = nbc;
fluxb = 0;
% North boundary
nbc(:,1) = ones(nx,1);
nbc(:,3) = fluxb*ones(nx,1);
% South boundary
sbc(:,1) = 2*ones(nx,1);
sbc(:,3) = fluxb*ones(nx,1);
% East Boundary
ebc(:,1) = ones(ny,1);
ebc(:,2) = POut*ebc(:,1);
% West Boundary
wbc(:,1) = ebc(:,1);
wbc(:,2) = PIn*wbc(:,1);

%% --------Compute finite-volume coefficients
%Allocate memory and initilize coefficients
ae = zeros(nn,1);
aw = zeros(nn,1);
an = zeros(nn,1);
as = zeros(nn,1);
%Compute grid quantities that are reused
dx = diff(xu);          %  control volume width in x-direction
dy = diff(yv);
dxw = [x(1); diff(x)];  %  distance between adjacent nodes  dxw(i) = x(i)-x(i-1)
dys = [y(1); diff(y)];  %  dys = y(j)-y(j-1)
%Coefficients calculations
for j=1:ny-1
    np = (1:nx)' + (j-1)*nx;   %  for i=1:nx,  np = i + (j-1)*nx
    an(np)    = 1/(dy(j)*dys(j+1));
    as(np+nx) = 1/(dy(j+1)*dys(j+1));
end
for i=1:nx-1
    np = i + ( ( (1:ny)' - 1) *nx );  %  for j=1:ny,  np = i + (j-1)*nx
    ae(np)   = 1/(dx(i)*dxw(i+1));
    aw(np+1) = 1/(dx(i+1)*dxw(i+1));
end

%% ---------Adjust coefficients to enforce boundary conditions
ap = zeros(size(ae));
b = zeros(size(ap));
dinv = 1.0 / ( (xu(nx+1)-xu(nx)) * (xu(nx+1)-x(nx)) );
% East Boundary
for j=1:ny
    np = nx + (j-1)*nx;               %  i=nx
    ae(np) = 0.0;
    b(np) = b(np) + dinv*ebc(j,2);
    ap(np) = ap(np) + dinv;
end

% West boundary
dinv =  1.0/( xu(2)*x(1) );    %  xu(1) = x(0) = 0
for j=1:ny
    np = 1 + (j-1)*nx;         %  i=1
    aw(np) = 0;
    b(np) = b(np) + dinv*wbc(j,2);
    ap(np) = ap(np) + dinv;
end

%North boundary
dinv = 1.0 / ( (yv(ny+1)-yv(ny)) * (yv(ny+1)-y(ny)) );
for i=1:nx
    np = i + (ny-1)*nx;          %  j=ny
    an(np) = 0;
    b(np) = b(np) + nbc(i,3)/dy(ny);
end

% South boundary
dinv =  1.0/( yv(2)*y(1) );    %  yv(1) = y(0) = 0
dyi = y(1) - yv(1);
for i=1:nx
    np = i;                    %  j=1
    as(np) = 0;
    b(np) = b(np) + sbc(i,3)/dy(1);
end
ap = ap + ae + aw + an + as;    %  Conservative scheme results in ap = sum of neighbors

%% ----------- Set up coefficient matrix
%Note:   A is created with spdiags, which takes an n-by-5 matrix formed from
%        [-as  -aw  ap  -ae   -an].  The columns of this temporary matrix contain
%        shifted versions of the input vectors that must are shifted so that
%        coefficients are stored in correct locations on the diagonals of A
n = length(ap);
znx = zeros(nx,1);   %  temporary vector used for as and an
A = spdiags([ [-as(nx+1:n); znx] [-aw(2:n); 0] ap [0; -ae(1:n-1)] [znx; -an(1:n-nx)] ],...
    [-nx -1 0 1 nx], n,n);

%% ---------------------------- Solve -------------------------------------
% P=zeros(1,n);
% Z=zeros(1,n);
% error=ones(1,n);
% iteration=0;
% while(max(error)>.0001)
%     iteration=(iteration)+1;
%     Z=P;
%     for i=1:1:n
%         sum=0;
%         for j=1:1:n
%             if(i~=j)
%                 sum=sum+(A(i,j)*P(j));
%             else
%                 continue;
%             end
%         end
%         P(i)=(b(i)-sum)/A(i,i);
%     end
%     error=P-Z;
% end

P = A\b;  % Solve for pressure field at internal nodes

%% ---------------- Make surface, and contour plots -----------------------
% Reshape P and add boundary values
P = reshape(P,nx,ny);
P = [P(:,1),P,P(:,5)]; % Add north and south boundary
PinWB = PIn*ones (1,size(P,2));
PinEB = POut*ones (1,size(P,2));
P = [PinWB;P;PinEB];   % Add west and east boundary pressure
P = P';
% Contour plot of pressure
xp = [0; x];
yp = flipud([0; y]);
[xx,yy] = meshgrid(xp,yp);
figure;  pcolor(xx,yy,P);  colorbar('vert');  shading('interp');    title ( 'Contour plot of P in the rectangular domain' );
xlabel('x(mm)');  ylabel('y(mm)'); xlim ([0 Lx]); ylim ([0 Ly]);
% Variation of P along x
figure;  plot(xp,P(2,:),'*-');   xlabel('x(mm)');   ylabel('P(bar)'); xlim ([0 Lx+2]); ylim ([0 PIn+100]); grid on; title ( 'Variation of P along x' );