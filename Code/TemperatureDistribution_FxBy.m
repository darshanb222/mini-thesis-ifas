function [T,T_bar] = TemperatureDistribution_FxBy(P,Z,n,m)
% TemperatureDistribution  Compute temperature distribution in the
% discretized grid for x and y and z
% Along z temperaure is approximated by approximation polynomial using body
% temperaure and average fluid temperature.
%
% Input P is pressure distribution in grid. Each row is for particular
% theta and each column is for particular y. Along z i.e gap height
% presuure is constant.
% Input Z is height variation with theta, discretized into divisions. Each
% row of Z is for particular theta.
%
R = 10e-3;          % Radius of Journal(inner) in m
theta_end = 2*pi;   % angular domain in radians
Y_end = 10e-3;      % axial domain in m
nn = (n+1)*(m+1);
rho_bar = 828.70;   % Density in Kg/m3
lamda_bar = 0.1219; % Heat conductivity in [ W / (mK) ]
C_p_bar = 2000;     % Heat capacity cp(T) [J/kggrd]
eta = 0.013;        % dynamics viscosity in Pascal second
rps = 1050;            % rotation speed in radians per second
U = R*rps;          % Velocity of journal
C = 10e-6;          % Clearance in m
e_r = 0.9;          % eccentricity ratio
e = e_r*C;          % eccentricity

del_theta = theta_end/n;
del_y = Y_end/m;
fun_h = @(theta)(e*cos(theta) + C);
theta = 0:del_theta:theta_end;
h_theta = fun_h(theta)';

T_1 = 303;          % Inner body temperature in Kelvin
T_2 = 298;          % Outer body temperature in Kelvin
T_amb = 298;        % Ambient temperature in Kelvin
T_bar = 305*ones(n+1,m+1);     % Initial averaged temperature of fluid. Each row is for particular theta and each column is for particular y
tolerence = ones(size(T_bar,1),size(T_bar,2));

T_bar_old = T_bar;
% Approximation of temperature profile in gap height
T_Z_fun = @(z,h,T_b)(((3/(h^2))*((-2*T_b)+T_1+T_2)*(z^2))+((2/h)*((3*T_b)-(2*T_1)-T_2)*z)+T_1);
T = zeros(size(P,1),size(Z,2),size(P,2));
for i=1:size(P,1)   % for each theta
    for j=1:size(Z,2)   % for each z
        for k=1:size(P,2)   % for each y
            T(i,j,k) = T_Z_fun(Z(i,j),Z(i,size(Z,2)),T_bar(i,k)); % each row is for particular theta and column is for different z values and k for different y
        end
    end
end

% Calculation of temperature gradient in gap height with limits for
% averaged energy equation
deltaTdeltaZLimits_fun = @(h,T_b)((6/h)*((-2*T_b)+T_1+T_2));
dTdZ = zeros(size(P,1),size(P,2));
for i = 1:size(P,1)
    for k = 1:size(P,2)
        dTdZ(i,k) = deltaTdeltaZLimits_fun(Z(i,size(Z,2)),T_bar(i,k)); % Temperature gradient for different theta values and y vales. i for theta and k for y
    end
end

% Calculation of pressure gradient in theta and y at nodal points
deltaP_deltaTheta = zeros(n+1,m+1);
deltaP_deltaY = zeros(n+1,m+1);
for i = 1:n+1
    for j = 1:m+1
        if i == 1
            deltaP_deltaTheta(i,j) = (P(i+1,j)-P(i+n-1,j))/(2*del_theta);
        elseif i == n+1
            deltaP_deltaTheta(i,j) = (P(i+1-n,j)-P(i-1,j))/(2*del_theta);
        else
            deltaP_deltaTheta(i,j) = (P(i+1,j)-P(i-1,j))/(2*del_theta);
        end
    end
end
for i = 1:n+1
    for j = 1:m+1
        if j == 1
            deltaP_deltaY(i,j) = (P(i,j+1)-P(i,j))/(del_y);
        elseif j == m+1
            deltaP_deltaY(i,j) = (P(i,j)-P(i,j-1))/(del_y);
        else
            deltaP_deltaY(i,j) = (P(i,j+1)-P(i,j-1))/(2*del_y);
        end
    end
end

% Allocate memory and initialization of co-efficients for temperature
Ae = zeros(n+1,m+1);
As = zeros(n+1,m+1);
Ap = ones(n+1,m+1);
B = zeros(n+1,m+1);

Ae_fun = @(h,dp_dx)(((rho_bar*C_p_bar*h)/(R*del_theta))*((U/2)-(((h^2)/(12*eta*R))*(dp_dx))));
As_fun = @(h,dp_dy)(((rho_bar*C_p_bar*h)/(del_y))*((((h^2)/(12*eta))*(dp_dy))));
B_fun = @(h,dp_dx,dt_dz)(((eta/h)*(U^2))+(((h^3)/(12*eta*(R^2)))*(dp_dx^2))+(lamda_bar*dt_dz));

for i=1:n+1
    for j=1:m+1
        Ae(i,j) = (-1*Ae_fun(h_theta(i),deltaP_deltaTheta(i,j)))/(Ae_fun(h_theta(i),deltaP_deltaTheta(i,j))+As_fun(h_theta(i),deltaP_deltaY(i,j)));
        As(i,j) = (-1*As_fun(h_theta(i),deltaP_deltaY(i,j)))/(Ae_fun(h_theta(i),deltaP_deltaTheta(i,j))+As_fun(h_theta(i),deltaP_deltaY(i,j)));
        B(i,j) = (-1*B_fun(h_theta(i),deltaP_deltaTheta(i,j),dTdZ(i,j)))/(Ae_fun(h_theta(i),deltaP_deltaTheta(i,j))+As_fun(h_theta(i),deltaP_deltaY(i,j)));
    end
end
% Adjustment of coeeficients for top and bottom boundary
% For bottom boundary
for i=1:n+1
    Ae(i,1) = 0;
    As(i,1) = 0;
    B(i,1) = T_amb;       % Setting of temperature of ambient in Kelvin
end
% For top boundary
for i=1:n+1
    Ae(i,m+1) = 0;
    As(i,m+1) = 0;
    B(i,m+1) = T_amb;     % Setting of temperature of ambient in Kelvin
end

Ae = reshape(Ae,(n+1)*(m+1),1);
As = reshape(As,(n+1)*(m+1),1);
Ap = reshape(Ap,(n+1)*(m+1),1);
B = reshape(B,(n+1)*(m+1),1);

% Setting up coeefficient matrix
A = zeros(nn,nn);
for i=1:n+1
    A(i,i) = Ap(i);
end
for i = nn-n:nn
    A(i,i) = Ap(i);
end
for i = n+2:nn-(n+1)
    if rem(i,n+1) == 0
        A(i,i) = Ap(i);
        A(i,i-(n-1)) = Ae(i);
        A(i,i-(n+1)) = As(i);
    else
        A(i,i) = Ap(i);
        A(i,i+1) = Ae(i);
        A(i,i-(n+1)) = As(i);
    end
end

%scaling the values
[P1,R1,C1] = equilibrate(A);
A1 = R1*P1*A*C1;
B1 = R1*P1*B;

T_bar = A1\B1;  % Solve for temperature field at internal nodes
T_bar = reshape(T_bar,n+1,m+1); % colums for y and rows for theta

end