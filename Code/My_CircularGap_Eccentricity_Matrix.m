%% ----------------Cylindrical gap with eccentricity-----------------------
% -------------------------------------------------------------------------
% The mean gap height varies with theta
% y ------> axial co-ordinate
% theta ------> angular co-ordinate
% Pressure variation with x and theta is calculated
% -------------------------------------------------------------------------
clear all;
clc;

R = 10e-3;          % Radius of Journal(inner) in m
theta_end = 2*pi;   % angular domain in m
Y_end = 10e-3;      % axial domain in m
n = 50;             % Number of nodes in theta direction
m = 50;             % Number of nodes in y direction
nn = (n+1)*(m+1);
del_theta = theta_end/n;
del_y = Y_end/m;
%% for SRS
% eta = 0.013;        % dynamics viscosity in Pascal second for 1000 bar
% eta = 0.044;        % dynamics viscosity in Pascal second for 2000 bar
% eta = 0.173;        % dynamic viscosity in Pascal second for 3000 bar
%% for hydraulic oil 32
% eta = 0.0714;       % dynamics viscosity in Pascal second for 1000 bar
% eta = 0.3979;        % dynamics viscosity in Pascal second for 2000 bar
 eta = 2.3986;        % dynamic viscosity in Pascal second for 3000 bar
P_b = 3000;         % Pressure in bar = 1000bar
Ecc = 0.7;          % At what eccentricity the values have to be plotted
RadPerSec = 500;    % At what angular velocity the values have to be plotted


Ecc = round(Ecc/0.05)+1;
RadPerSec = (RadPerSec/50)+1;
RPS = 0:50:1050;    % rotation speed in radians per second
ov_Data = cell(1,length(RPS));

for rps = 1:length(RPS)
    U = R*RPS(rps);          % Velocity of journal
    C = 10e-6;          % Clearance in m
    e_r = 0:0.05:0.95;  % eccentricity ratio
    
    % cell storage for Data structure
    P_Data = cell(1,length(e_r));
    P_max_Data = cell(1,length(e_r));
    F_Data = cell(1,length(e_r));
    F_max_Data = cell(1,length(e_r));
    u_Data = cell(1,length(e_r));
    v_Data = cell(1,length(e_r));
    P = zeros(n+1,m+1,length(e_r));
    for er = 1:length(e_r)
        e = e_r(er)*C;          % eccentricity
        
        fun_h = @(theta)(e*cos(theta) + C);
        theta = 0:del_theta:theta_end;
        Y = 0:del_y:Y_end;
        h_theta = fun_h(theta)';
        
        %% Calculation of height at half plus and minus
        
        theta_half = zeros(size(theta,2)-1,1);
        for i=1:length(theta)-1
            theta_half(i) = (theta(i+1)+theta(i))/2;
        end
        half_h = fun_h(theta_half);
        
        h_halfplus = [half_h;half_h(1)];
        h_halfminus = [half_h(length(half_h));half_h];
        
        
        h_plus = h_halfplus.^3;
        h_minus = h_halfminus.^3;
        
        %% Initialization of pressure and coefficients
        % h_p = h plus half and h_m = h minus half
        
        %Allocate memory and initilize coefficients
        ae = zeros(n+1,m+1);
        aw = zeros(n+1,m+1);
        an = zeros(n+1,m+1);
        as = zeros(n+1,m+1);
        ap = ones(n+1,m+1);
        b = zeros(size(ae));
        
        A_fun = @(h,h_p,h_m)((-h_p*(del_y^2))/((h_p + h_m)*(del_y^2) + (2*(R^2)*(h^3)*(del_theta^2))));
        B_fun = @(h,h_p,h_m)((-h_m*(del_y^2))/((h_p + h_m)*(del_y^2) + (2*(R^2)*(h^3)*(del_theta^2))));
        C_fun = @(h,h_p,h_m)((-(R^2)*(h^3)*(del_theta^2))/((h_p + h_m)*(del_y^2) + (2*(R^2)*(h^3)*(del_theta^2))));
        E_fun = @(theta,h,h_p,h_m)((e*sin(theta)*6*eta*(R^2)*RPS(rps)*(del_y^2)*(del_theta^2))/((h_p + h_m)*(del_y^2) + (2*(R^2)*(h^3)*(del_theta^2))));
        
        for i=1:n+1
            for j=1:m+1
                ae(i,j) = A_fun(h_theta(i),h_plus(i),h_minus(i));
                aw(i,j) = B_fun(h_theta(i),h_plus(i),h_minus(i));
                an(i,j) = C_fun(h_theta(i),h_plus(i),h_minus(i));
                as(i,j) = C_fun(h_theta(i),h_plus(i),h_minus(i));
                b(i,j) = E_fun(theta(i),h_theta(i),h_plus(i),h_minus(i));
            end
        end
        % Adjustment of coeeficients for top and bottom boundary
        % For bottom boundary
        for i=1:n+1
            ae(i,1) = 0;
            aw(i,1) = 0;
            an(i,1) = 0;
            as(i,1) = 0;
            b(i,1) = P_b*1e5;       % conversion of pressure from bar to pascal
        end
        % For top boundary
        for i=1:n+1
            ae(i,m+1) = 0;
            aw(i,m+1) = 0;
            an(i,m+1) = 0;
            as(i,m+1) = 0;
            b(i,m+1) = P_b*1e5;     % conversion of pressure from bar to pascal
        end
        
        ae = reshape(ae,(n+1)*(m+1),1);
        aw = reshape(aw,(n+1)*(m+1),1);
        as = reshape(as,(n+1)*(m+1),1);
        an = reshape(an,(n+1)*(m+1),1);
        ap = reshape(ap,(n+1)*(m+1),1);
        b = reshape(b,(n+1)*(m+1),1);
        %% Solver
        % Setting up coeefficient matrix
        A = zeros(nn,nn);
        for i=1:n+1
            A(i,i) = ap(i);
        end
        for i = nn-n:nn
            A(i,i) = ap(i);
        end
        for i = n+2:nn-(n+1)
            if rem(i,n+1) == 1
                A(i,i) = ap(i);
                A(i,i-(n+1)) = as(i);
                A(i,(i+n)-1) = aw(i);
                A(i,i+1) = ae(i);
                A(i,i+n+1) = an(i);
            elseif rem(i,n) == 0
                A(i,i) = ap(i);
                A(i,i-(n+1)) = as(i);
                A(i,i-1) = aw(i);
                A(i,(i-n)+1) = ae(i);
                A(i,i+n+1) = an(i);
            else
                A(i,i) = ap(i);
                A(i,i-(n+1)) = as(i);
                A(i,i-1) = aw(i);
                A(i,i+1) = ae(i);
                A(i,i+n+1) = an(i);
            end
        end
        
        P = A\b;  % Solve for pressure field at internal nodes
        P = reshape(P,n+1,m+1); % colums for y and rows for theta
        P_Data{er} = P';
        P_max_Data{er} = max(max(P));
        %% Solving for forces
        [F_Data{er},F_x,F_y] = ForceCalculation(P,P_b);
        F_max_Data{er} = max(max(F_Data{er}));
        Fx_Total = sum(F_x(:,25));  % Total x component of force at mid length
        Fy_Total = sum(F_y(:,25));  % Total y component of force at mid length
        %% Solving for velocity profile
        % u is tangential velocity
        % v is velocity in axial direction
        % No velocity in z direction ( i.e in thickness of film direction) as there
        % is no pressure gradient
        %
        % u velocity profile for different theta and z.
        % u velocity is along theta direction.
        Z_final = half_h;
        z_init = 0 * ones(n,1);
        Z_half = zeros(n,n+1);
        for i = 1:n
            Z_half(i,:) =  linspace (z_init(i),Z_final(i), n+1);   %Z values in each row is for particular theta. For example: row 1 is for theta(1)
        end
        
        deltaP_deltaTheta = diff(P)/del_theta; % finding difference of pressure in theta for different y value
        u_fun = @(delP_delTheta,z,h)(((1/(2*eta))*(delP_delTheta/R)*((z^2)-(z*h)))+(U*(1-(z/h))));
        u = zeros(n,size(Z_half,2),m+1);
        for i=1:n
            for j = 1:size(Z_half,2)
                for k =1:m+1
                    u(i,j,k) = u_fun(deltaP_deltaTheta(i,k),Z_half(i,j),half_h(i));   %u velocity values in i is for theta, j is for z, k is for y
                end
            end
        end
        
        % v velocity profile
        % v velocity is along axial direction.
        Z_final = h_theta;
        z_init = zeros(n+1,1);
        Z = zeros(n+1,n+1);
        for i = 1:n+1
            Z(i,:) =  linspace (z_init(i),Z_final(i), n+1);   %Z values in each row is for particular theta. For example: row 1 is for theta(1)
        end
        deltaP_deltaY = diff(P,[],2)/del_y; % finding difference of pressure in Y for different values of theta
        v_fun = @(delP_delY,z,h)((1/(2*eta))*delP_delY*z*(z-h));
        v = zeros(n+1,m,size(Z,2));
        for i=1:n+1
            for j = 1:m
                for k = 1:size(Z,2)
                    v(i,j,k) = v_fun(deltaP_deltaY(i,j),Z(i,k),h_theta(i));   %v velocity values in i is for theta, j is for y, k is for z
                end
            end
        end
        u_Data{er} = u;
        v_Data{er} = v;
    end
    
    %% Structure for data storage for different eccentricity
    f1 = 'eccentricity_ratio';  v1 = num2cell(e_r);
    f2 = 'Pressure_max';  v2 = P_max_Data;
    f3 = 'Pressure';  v3 = P_Data;
    f4 = 'Force';  v4 = F_Data;
    f5 = 'Force_max';  v5 = F_max_Data;
    f6 = 'tangential_velocity';  v6 = u_Data;
    f7 = 'axial_velocity';  v7 = v_Data;
    Data = struct(f1,v1,f2,v2,f3,v3,f4,v4,f5,v5,f6,v6,f7,v7);
    ov_Data{rps} = Data;
end
%% Structure for data storage for different angular velocity
F1 = 'angular_velocity';  V1 = num2cell(RPS);
F2 = 'Data';  V2 = ov_Data;
save_Data = struct(F1,V1,F2,V2);

%% Solving Temperature profile
% Temperature distribution along gap height for different theta
%[T,T_bar] = TemperatureDistribution_BxCyUpwind(P,Z,n,m); % each row is for particular theta and column is for different z values and k for each y
%[T,T_bar] = TemperatureDistribution_ByVelocityAvgUpwind(P,u,v,Z_half,Z,n,m);
%% Post processing
figure('Name',[num2str((RadPerSec-1)*50),' rad/s and eccentricity ratio ',num2str((Ecc-1)*0.05)]);
subplot(2,2,1);
colormap(jet);
surf(theta,Y,(save_Data(RadPerSec).Data(Ecc).Pressure)./1e5);
title('Pressure profile');
xlabel('Theta(radians)');
ylabel('Y(m)');
zlabel('Pressure(bar)');

subplot(2,2,2);
plot(theta,(save_Data(RadPerSec).Data(Ecc).Pressure(25,:))./1e5,'-o','LineWidth',2,'MarkerEdgeColor','k')
title('Pressure vs Theta (mid axial position)');
xlabel('Theta(radians)');
ylabel('Pressure(bar)');
grid on

subplot(2,2,3);
plot(save_Data(RadPerSec).Data(Ecc).tangential_velocity(24,:,25),Z(24,:),'-x','LineWidth',2,'MarkerEdgeColor','k')
title('Velocity profile in gap for mid axial position');
xlabel('u-velocity');
ylabel('z(m)');
grid on

subplot(2,2,4);
plot(squeeze(save_Data(RadPerSec).Data(Ecc).axial_velocity(25,35,:)),Z(25,:),'-x','LineWidth',2,'MarkerEdgeColor','k')
title('Axial velocity');
xlabel('v-velocity');
ylabel('z(m)');
grid on

figure
subplot(2,2,1);
for i = 1:length(e_r)
    P_max_Data{i} = save_Data(RadPerSec).Data(i).Pressure_max;
end
plot(e_r,cell2mat(P_max_Data)./1e5,'-x','LineWidth',2,'MarkerEdgeColor','k')
title(['Maximum Pressure vs eccentricity ratio (',num2str((RadPerSec-1)*50),' rad/s)']);
xlabel('Eccentricity Ratio');
ylabel('Maximum Pressure (bar)');
grid on

subplot(2,2,2);
for i = 1:length(e_r)
    F_max_Data{i} = save_Data(RadPerSec).Data(i).Force_max;
end
plot(e_r,cell2mat(F_max_Data),'-x','LineWidth',2,'MarkerEdgeColor','k')
title(['Maximum Force vs eccentricity ration (',num2str((RadPerSec-1)*50),' rad/s)']);
xlabel('Eccentricity Ratio');
ylabel('Maximum Force (N)');
grid on

subplot(2,2,3);
for i = 1:length(RPS)
    P_max_Data{i} = save_Data(i).Data(Ecc).Pressure_max;
end
plot(RPS,cell2mat(P_max_Data)./1e5,'-x','LineWidth',2,'MarkerEdgeColor','k')
title(['Maximum Pressure vs angular velocity (',num2str((Ecc-1)*0.05),' eccentricity ratio)']);
xlabel('Angular velocity (rad/s)');
ylabel('Maximum Pressure (bar)');
grid on

subplot(2,2,4);
for i = 1:length(RPS)
    F_max_Data{i} = save_Data(i).Data(Ecc).Force_max;
end
plot(RPS,cell2mat(F_max_Data),'-x','LineWidth',2,'MarkerEdgeColor','k')
title(['Maximum Force vs angular velocity (',num2str((Ecc-1)*0.05),' eccentricity ratio)']);
xlabel('Angular velocity (rad/s)');
ylabel('Maximum Force (N)');
grid on
