function [eta] = viscosity(P,T)
A = 0.06861*0.001;
B = 862.5;
C = 102.5;
a00 = 2.659*0.001;
a01 = -8.459*0.001;
a10 = 1.106*0.0000001;
a11 = -1.731*0.01;

a1 = a10*exp(a11*T);
a0 = a00*exp(a01*T);
a = a0+(a1*P);
eta_i = A*exp(B/(C+T));
eta = eta_i*exp(a*P);
end