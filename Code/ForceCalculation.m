function [F,F_x,F_y] = ForceCalculation(P,P_b)
% ForceCalculation  Compute force due to pressure in the gap.
%
% Input P is pressure distribution in grid. Each row is for particular
% theta and each column is for particular y. Along z i.e gap height
% presuure is constant.
%
R = 10e-3;          % Radius of Journal(inner)
theta_end = 2*pi;   % angular domain in m
Y_end = 10e-3;      % axial domain in m
n = 50;             % Number of nodes in theta direction
m = 50;             % Number of nodes in y direction
del_S = (theta_end/n)*R;
del_y = Y_end/m;

theta = 0:theta_end/n:theta_end;
F = (P-(P_b*1e5))*(del_S*del_y);
F_x = zeros(size(P,1),size(P,2));
F_y = zeros(size(P,1),size(P,2));
for i = 1:size(P,1)
    F_x(i,:) = F(i,:)*cos(theta(i));
    F_y(i,:) = F(i,:)*sin(theta(i));
end