function [m,er] = sgs(A, f, x, max_iter, TOL)

er = [];
n = size(A,1);
D = diag(diag(A));
L = -tril(A,-1);
U = -triu(A,1);
normf = norm(f);

i = 1;
while (i <= max_iter)
  x = ((D-U)\L)*((D-L)\U)*x + ((D-U)\D)*((D-L)\f);
  if (normf)
    er = [er; norm(A*x - f)/normf];
  else
    er = [er; norm(x)];
  end
  if (er(i) < TOL)
    m = i;
    i = max_iter;
  end
  i = i + 1;
end
