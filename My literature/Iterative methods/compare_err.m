function compare_err(TOL, N)

% Different iterative schemes are compared:
% Jacobi,
% Gauss-Seidel (forward and symmetric),
% conjugate gradient method.
%
% The function solves the system arising from the standard
% FD/FE discretization of the Poisson equation with zero right hand
% side on the unit square. It plots the error decay versus the
% number of iterations until the error reaches TOL.
%
% INPUT: TOL  tolerance
%        N    number of unknowns in ONE direction
%
% OUTPUT: Plot
%
% WARNING: The implementation was done following one rule: Keep it
% simple and clear. Therefore the iterative schemes are by no means
% implemented in an efficient way. To avoid overlong waiting
% periods N should not exceed 20.
%
% Date: 2002-08-05
% Author: Bernd Flemisch
% Email: flemisch@mathematik.uni-stuttgart.de

max_iter = 1000; % maximum number of iterations 
m = 1:1:max_iter;

A = gallery('poisson', N); % FD approximation of the Laplacian
f = zeros(N*N, 1); % right hand side
x0 = ones(N*N, 1); % starting guess

[it_jac, err_jac] = jacobi(A, f, x0, max_iter, TOL);
[it_gs, err_gs] = gs(A, f, x0, max_iter, TOL);
[it_sgs, err_sgs] = sgs(A, f, x0, max_iter, TOL);
[it_cg, err_cg] = cong(A, f, x0, max_iter, TOL);

clf;
semilogy(m(1:it_jac), err_jac, 'm-', ...
	 m(1:it_gs), err_gs, 'g-', ...
	 m(1:it_sgs), err_sgs, 'r-', ...
	 m(1:it_cg), err_cg, 'b-', ...
	 'linewidth', 3);
ylim([TOL err_jac(1)]);
set(gca, 'fontsize', 18);
legend('Jacobi', 'GS', 'SGS', 'CG',1);
c = sprintf('error decay', TOL);
title(c);
ylabel('error');
xlabel('iterations');
