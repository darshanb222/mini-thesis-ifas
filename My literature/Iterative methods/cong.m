function [m,er] = cong(A, f, x, max_iter, TOL)

er = [];
normf = norm(f);
ro = f - A*x;
d = ro;

i = 1;
while (i <= max_iter)
  alpha = (ro'*ro/(d'*A*d));
  x = x + alpha*d;
  rn = ro - alpha*A*d;
  beta = rn'*rn/(ro'*ro);
  d = rn + beta*d;
  ro = rn;
  if (normf)
    er(i) = norm(A*x - f)/normf;
  else
    er(i) = norm(x);
  end
  if (er(i) < TOL)
    m = i;
    i = max_iter;
  end
  i = i + 1;
end
