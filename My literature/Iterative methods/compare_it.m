function compare_it(TOL, Nmax)

% Different iterative schemes are compared:
% Jacobi,
% Gauss-Seidel (forward and symmetric),
% conjugate gradient method.
%
% Starting from 1 unknown in one direction to Nmax unknowns,
% the function solves the system arising from the standard
% FD/FE discretization of the Poisson equation with zero right hand
% side on the unit square. It plots the number of iterations needed
% until the error reaches TOL versus the number of unknowns.
%
% INPUT: TOL  tolerance
%        Nmax maximum number of unknowns in ONE direction
%
% OUTPUT: Plot
%
% WARNING: The implementation was done following one rule: Keep it
% simple and clear. Therefore the iterative schemes are by no means
% implemented in an efficient way. To avoid overlong waiting
% periods Nmax should not exceed 15.
%
% Date: 2002-08-05
% Author: Bernd Flemisch
% Email: flemisch@mathematik.uni-stuttgart.de

max_iter = 1000; % maximum number of iterations 
m = zeros(Nmax,1); % vector containing the number of unknowns

for n = 2:1:Nmax
  m(n) = n^2; % number of unknowns
  A = gallery('poisson', n); % FD approximation of the Laplacian
  f = zeros(n*n, 1); % right hand side
  x0 = ones(n*n, 1); % starting guess
  
  [it_jac(n), err_jac] = jacobi(A, f, x0, max_iter, TOL);
  [it_gs(n), err_gs] = gs(A, f, x0, max_iter, TOL);
  [it_sgs(n), err_sgs] = sgs(A, f, x0, max_iter, TOL);
  [it_cg(n), err_cg] = cong(A, f, x0, max_iter, TOL);
end

clf;
semilogy(m, it_jac, 'm-*', ...
	 m, it_gs, 'g-o', ...
	 m, it_sgs, 'r-x', ...
	 m, it_cg, 'b-s', ...
	 'linewidth', 3);
set(gca, 'fontsize', 18);
legend('Jacobi', 'GS', 'SGS', 'CG',4);
c = sprintf('iterations, until error < %.1e', TOL);
title(c);
xlabel('dof');
ylabel('iterations');
