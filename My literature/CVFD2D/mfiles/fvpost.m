function [T,ebout,qe,qw,qn,qs,qgen] = fvpost(x,xu,y,yv,con,src,ebc,wbc,nbc,sbc,t,verbose)
% fvpost  Post-process solution to extract boundary fluxes, energy balance
%
% Synopsis:  [T,ebal] = fvpost(x,xu,y,yv,con,src,wbc,nbc,sbc,t)
%            [T,ebal,qe,qw,qn,qs,qgen] = fvpost(x,xu,y,yv,con,src,wbc,nbc,sbc,t)
%
% Input:  x,xu,y,yv = vectors defining the grid.  See mesh routines
%         con = vector of thermal conductivities for internal control volumes
%         src = vector of heat source terms for internal control volumes
%         ebc,wbc,nbc,sbc = data structures used to enforce boundary conditions
%         t = solution to the temperature field at the internal nodes
%
% Output:  T = matrix representation of the temperature field.  T(i,j) is the
%              temperature at x(i) and y(j), and includes the boundary conditions
%          ebal = energy balance.  If ebal is not zero, or very, very small,
%                 there is an error in the code.
%          qe = (optional) total heat transfer rate per unit depth (into page)
%               across the east boundary.
%          qw = (optional) total heat transfer rate per unit depth (into page)
%               across the west boundary.
%          qn = (optional) total heat transfer rate per unit depth (into page)
%               across the north boundary.
%          qs = (optional) total heat transfer rate per unit depth (into page)
%               across the south boundary.
%
% Sign Convention:   All heat flows into the domain are positive.  Thus, the
%                    net inflow of heat is qe + qw + qn + qs

if nargin<12,  verbose = 1;  end 

% --- Allocate memory and initilize coefficients
nx = length(x) - 1;  ny = length(y) - 1;
dx = diff(xu);       dy = diff(yv);
qsrc = 0;

% --- Contribution of heat source at interior control volumes
if any(src)
	for i=1:nx
      for j = 1:ny
        np = i + (j-1)*nx;
        qsrc = qsrc + dx(i)*dy(j)*src(np);
      end
	end
end

% --- Heat flux on east boundary
dxi = xu(nx+1) - x(nx);   %  distance between first internal node and boundary
for j=1:ny
  np = nx + (j-1)*nx;               %  i=nx
  switch ebc(j,1)
    case 1  %  constant T
      ebc(j,3) = con(np)*(ebc(j,2)-t(np))/dxi;   %  local heat flux into domain
    case 2  %  constant q
      ebc(j,2) = t(np) + ebc(j,3)*dxi/con(np);   %  decorate temperature
    case 3  %  Convection
      kond = con(np)/dxi;
      ebc(j,2) = (ebc(j,4)*ebc(j,5)+kond*t(np))/(ebc(j,4)+kond); % decorate temperature
      ebc(j,3) = ebc(j,4)*(ebc(j,5)-ebc(j,2));                   % compute q = h*(Tinf - Tb)
    case 4  %  Symmetry
      ebc(j,2) = t(np);   %  near boundary and boundary values are equal, q = 0
    otherwise
      error('BC type = %d not defined on east boundary',ebc(j,1));
  end
end

% --- Heat flux on west boundary.  Analogous to east boundary
dxi = x(1) - xu(1);   %  distance between first internal node and boundary
for j=1:ny
  np = 1 + (j-1)*nx;                %  i=1
  switch wbc(j,1)
    case 1  %  constant T
      wbc(j,3) = con(np)*(wbc(j,2)-t(np))/dxi;   %  local heat flux into domain
    case 2  %  constant q
      wbc(j,2) = t(np) + wbc(j,3)*dxi/con(np);   %  decorate temperature
    case 3  %  Convection
      % -- Tb = (h*Tinf + (k/delta)*Ti)/(h + (k/delta))
      kond = con(np)/dxi;
      wbc(j,2) = (wbc(j,4)*wbc(j,5)+kond*t(np))/(wbc(j,4)+kond); % decorate temperature
      wbc(j,3) = wbc(j,4)*(wbc(j,5)-wbc(j,2));                   % compute q = h*(Tb - Tinf)
    case 4  %  Symmetry
      wbc(j,2) = t(np);   %  near boundary and boundary values are equal, q = 0
    otherwise
      error('BC type = %d not defined on west boundary',wbc(j,1));
  end
end

% --- Heat flux on north boundary
dyi = yv(ny+1) - y(ny);   %  distance between first internal node and boundary
for i=1:nx
  np = i + (ny-1)*nx;               %  j=ny
  switch nbc(i,1)
    case 1  %  constant T
      nbc(i,3) = con(np)*(nbc(i,2)-t(np))/dyi;   %  local heat flux into domain
    case 2  %  constant q
      nbc(i,2) = t(np) + nbc(i,3)*dyi/con(np);   %  decorate temperature
    case 3  %  Convection
      % -- Tb = (h*Tinf + (k/delta)*Ti)/(h + (k/delta))
      kond = con(np)/dyi;
      nbc(i,2) = (nbc(i,4)*nbc(i,5)+kond*t(np))/(nbc(i,4)+kond); % decorate temperature
      nbc(i,3) = nbc(i,4)*(nbc(i,5)-nbc(i,2));                   % compute q = h*(Tinf-Tb)
    case 4  %  Symmetry
      nbc(i,2) = t(np);   %  near boundary and boundary values are equal, q = 0
    otherwise
      error('BC type = %d not defined on north boundary',nbc(i,1));
  end
end

%  --- Heat flux on south boundary.  Analogous to north boundary
dyi = y(1) - yv(1);   %  distance between first internal node and boundary
for i=1:nx
  np = i;                           %  j=1
  switch sbc(i,1)
    case 1  %  constant T
      sbc(i,3) = con(np)*(sbc(i,2)-t(np))/dyi;   %  local heat flux into domain
    case 2  %  constant q
      sbc(i,2) = t(np) + sbc(i,3)*dyi/con(np);   %  decorate temperature
    case 3  %  Convection
      % -- Tb = (h*Tinf + (k/delta)*Ti)/(h + (k/delta))
      kond = con(np)/dyi;
      sbc(i,2) = (sbc(i,4)*sbc(i,5)+kond*t(np))/(sbc(i,4)+kond); % decorate temperature
      sbc(i,3) = sbc(i,4)*(sbc(i,5)-sbc(i,2));                   % compute q = h*(Tinf - Tb)
    case 4  %  Symmetry
      sbc(i,2) = t(np);   %  near boundary and boundary values are equal, q = 0
    otherwise
      error('BC type = %d not defined on south boundary',sbc(i,1));
  end
end

qetot = sum(ebc(:,3).*dy);   %  qtot = sum( q(i).*dA(i) )
qwtot = sum(wbc(:,3).*dy);
qntot = sum(nbc(:,3).*dx);
qstot = sum(sbc(:,3).*dx);

% --- Energy balance = heat generated - net outflow?
ebal = qsrc + ( qetot + qwtot + qntot + qstot );

if verbose
	fprintf('\nEnergy balance:\n');
	fprintf('\t qsrc    = %14.6f\n',qsrc);
	fprintf('\t Qeast   = %14.6f\n',qetot);
	fprintf('\t Qwest   = %14.6f\n',qwtot);
	fprintf('\t Qnorth  = %14.6f\n',qntot);
	fprintf('\t Qsouth  = %14.6f\n',qstot);
	fprintf('\t ----------------------------\n');
	fprintf('\t Balance = %14.6f\n',ebal);
end

% --- Copy t field into T matrix
T= make2DijField(nx,ny,x,xu,y,yv,t,ebc(:,2),wbc(:,2),nbc(:,2),sbc(:,2));

if nargin>1,  ebout = ebal;  end
if nargout>2
  qgen = qsrc;  qe = qetot;   qw = qwtot;    qn = qntot;  qs = qstot;
end

end
