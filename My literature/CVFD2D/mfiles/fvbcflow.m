function [ap,ae,aw,an,as,b] = fvbcflow(x,xu,y,yv,rho,gam,src,ebc,wbc,nbc,sbc,ae,aw,an,as,scheme)
% fvbcFlow  Modify coefficients of 5pt finite volume scheme to account for boundary conditions
%           where convective boundary conditions are allowed
%
% Synopsis:  
%
% Input:  x,xu,y,yv = vectors defining the grid.  See mesh routines
%         u,v = velocity vectors pointing to the cell centers
%         gam = diffusion coefficient for each cell;  nx*ny values in a vector
%         src = source term for each cell;  nx*ny values in a vector
%         ebc,wbc,nbc,sbc = boundary condition matrices
%         ae,aw,an,as = coefficients for interior faces of cells
%         scheme = (string) indicates convection modeling scheme
%                    scheme = 'UDS' for upwind differencing
%                             'CDS' for central differencing
%
% Output:
%         ap = diagonal coefficient for the 5 point CVFD stencil
%         ae, aw, an, as, b = neighbor coefficients and source terms for
%             the 5 point CVFD stencil that have been adjusted to enforce
%             boundary conditions specified in the ebc, wbc, nbc, and sbc matrices

%  printGrid(x,xu,y,yv,dx,dy)

%  Allocate memory and initilize coefficients
nx = length(x) - 1;  ny = length(y) - 1;   % nn = nx*ny;
ap = zeros(size(ae));
dx = diff(xu);
dy = diff(yv);
if isempty(src)
  b = zeros(size(ap));
else
  b = src;
end

% -----------------------------------
%  Coefficients on the east boundary
%  No harmonic mean is used to compute gam_e because the east neighbor (boundary point)
%  is on the interface.  Compute the aeb value, use it to eliminate the boundary
%  value, then set the ae(np) to zero
dinv = 1.0 / ( (xu(nx+1)-xu(nx)) * (xu(nx+1)-x(nx)) );
dxi = xu(nx+1) - x(nx);        %  distance from near boundary node to boundary
ue = ebc(:,6);                 %  normal velocity component
for j=1:ny
  np = nx + (j-1)*nx;               %  i=nx
  ae(np) = 0.0;
  switch ebc(j,1)
    case 1  %  constant T
      aeb = gam(np)*dinv;             %  true boundary coefficient
      b(np) = b(np) + aeb*ebc(j,2);
      ap(np) = ap(np) + aeb;
    case 2  %  constant q
      b(np) = b(np) + ebc(j,3)/dx(nx);
    case 3  %  Convection
      tmp = ebc(j,4)*gam(np)/(ebc(j,4)*dxi + gam(np));  %  h*k/(h*dyi + k)
      b(np) = b(np) + tmp*ebc(j,5)/dx(nx);              % + tmp*Tinf/deltax
      ap(np) = ap(np) + tmp/dx(nx);
    case 4  %  Symmetry
            %  do nothing
    case 6  %  Inflow
      % error('Inflow BC not finished on east boundary');
      if strcmpi(scheme,'UDS')
        be = ue(j)<0;   %  upwind only if velocity is negative
        awb = gam(np)*dinv + be*ue(j)/dx(end);
      elseif strcmpi(scheme,'CDS')
        awb = gam(np)*dinv - ue(j)/dx(end);
      else
        error(springf('scheme = %s is not supported'))
      end
      ap(np) = ap(np) + aeb;
      b(np) = b(np) + aeb*ebc(j,2);
    case 7  %  Outflow
      %  do nothing
      
      % error('Outflow BC not finished on east boundary');
%       if ue(j)<0
%         warning(sprintf('Inflow at outflow boundary at j=%d on east boundary',j));
%       end
      if strcmpi(scheme,'UDS')
        aeb = 0;
      elseif strcmpi(scheme,'CDS')
        aeb = - ue(j)/dx(end);
      else
        error('scheme = %s is not supported');
      end
      ap(np) = ap(np) + aeb;
      b(np) = b(np) + aeb*ebc(j,2);
    otherwise
      error('BC type = %d not defined on east boundary',ebc(j,1));
  end
end

% ----------------------------------
%  Coefficients on the west boundary.  Analogous to east boundary
dinv =  1.0/( xu(2)*x(1) );    %  dinv = 1/(xu(2)-xu(1))/(x(1)-x(0));   xu(1) = x(0) = 0
dxi = x(1) - xu(1);            %  distance from boundary to near boundary node
uw = wbc(:,6);                 %  normal velocity component
for j=1:ny
  np = 1 + (j-1)*nx;           %  i=1
  aw(np) = 0;
  switch wbc(j,1)
    case 1  %  constant T
      awb = gam(np)*dinv;             %  true boundary coefficients
      b(np) = b(np) + awb*wbc(j,2);
      ap(np) = ap(np) + awb;
    case 2  %  constant q
      b(np) = b(np) + wbc(j,3)/dx(1);
    case 3  %  Convection
      tmp = wbc(j,4)*gam(np)/(wbc(j,4)*dxi + gam(np));  %  h*k/(h*dyi + k)
      b(np) = b(np) + tmp*wbc(j,5)/dx(1);               % + tmp*Tinf/deltax
      ap(np) = ap(np) + tmp/dx(1);
    case 4  %  Symmetry
            %  do nothing
    case 6  %  Inflow
      if strcmpi(scheme,'UDS')
        bw = uw(j)>=0;
        awb = rho*bw*uw(j)/dx(1) + gam(np)*dinv;
      elseif strcmpi(scheme,'CDS')
        awb = gam(np)*dinv + rho*uw(j)/dx(1);
      else
        error(springf('scheme = %s is not supported'))
      end
      ap(np) = ap(np) + awb;
      b(np) = b(np) + awb*wbc(j,2);
    case 7  %  Outflow
            %  do nothing
            
      % error('Outflow BC not finished on west boundary');
%       if uw(j)>0
%         warning(sprintf('Inflow at outflow boundary at j=%d on east boundary',j));
%       end
%       if strcmp(upper(scheme),'UDS')
%         awb = 0;
%       elseif strcmp(upper(scheme),'CDS')
%         awb = uw(j)/dx(1);
%       else
%         error(springf('scheme = %s is not supported'))
%       end
%       ap(np) = ap(np) + awb;
%       b(np) = b(np) + awb*wbc(j,2);
    otherwise
      error('BC type = %d not defined on west boundary',wbc(j,1));
  end
end

% ----------------------------------
%  Coefficients on the north boundary.  No harmonic mean is used to
%  compute gam_n because the north neighbor (boundary point) is on the
%  interface.  Follow analogous procedure to computing aeb, above.
dinv = 1.0 / ( (yv(ny+1)-yv(ny)) * (yv(ny+1)-y(ny)) );
dyi = yv(ny+1) - y(ny);
vn = nbc(:,6);                  %  normal velocity component
for i=1:nx
  np = i + (ny-1)*nx;               %  j=ny
  an(np) = 0;
  switch nbc(i,1)
    case 1  %  constant T
      anb = gam(np)*dinv;           %  true boundary coefficient
      b(np) = b(np) + anb*nbc(i,2);
      ap(np) = ap(np) + anb;
    case 2  %  constant q
      b(np) = b(np) + nbc(i,3)/dy(ny);  %  heat flux * area/volume
    case 3  %  Convection
      tmp = nbc(i,4)*gam(np)/(nbc(i,4)*dyi + gam(np));  %  h*k/(h*dyi + k)
      b(np) = b(np) + tmp*nbc(i,5)/dy(ny);              % + tmp*Tinf/deltay
      ap(np) = ap(np) + tmp/dy(ny);
    case 4  %  Symmetry
            %  do  nothing
    case 6  %  Inflow
      % error('Inflow BC not finished on north boundary');
      if strcmpi(scheme,'UDS')
        bn = vn(i)>=0;
        asb = bn*vn(i)/dy(end) + gam(np)*dinv;
      elseif strcmpi(scheme,'CDS')
        asb = gam(np)*dinv - vn(i)/dy(end);
      else
        error(springf('scheme = %s is not supported'))
      end
      ap(np) = ap(np) + anb;
      b(np) = b(np) + anb*nbc(i,2);
    case 7  %  Outflow
            %  do nothing
      % error('Outflow BC not finished on north boundary');
%       if vn(i)<0
%         warning(sprintf('Inflow at outflow boundary at i=%d on north boundary',i));
%       end
%       if strcmp(upper(scheme),'UDS')
%         anb = 0;
%       elseif strcmp(upper(scheme),'CDS')
%         anb = - vn(i)/dy(end);
%       else
%         error(springf('scheme = %s is not supported'))
%       end
%       ap(np) = ap(np) + anb;
%       b(np) = b(np) + anb*nbc(i,2);
    otherwise
      error('BC type = %d not defined on north boundary',nbc(i,1));
  end
end

% ----------------------------------
%  Coefficients on the south boundary.  Analogous to north boundary
dinv =  1.0/( yv(2)*y(1) );    %  yv(1) = y(0) = 0
dyi = y(1) - yv(1);
vs = sbc(:,6);            %  normal velocity component
for i=1:nx
  np = i;                           %  j=1
  as(np) = 0;
  switch sbc(i,1)
    case 1  %  constant T
      asb = gam(np)*dinv;           %  true boundary coefficient
      b(np) = b(np) + asb*sbc(i,2);
      ap(np) = ap(np) + asb;
    case 2  %  constant q
      b(np) = b(np) + sbc(i,2)/dy(1);
    case 3  %  Convection
      tmp = sbc(i,4)*gam(np)/(sbc(i,4)*dyi + gam(np));
      b(np) = b(np) + tmp*sbc(i,5)/dy(1);
      ap(np) = ap(np) + tmp/dy(1);
    case 4  %  Symmetry
            %  do  nothing
    case 6  %  Inflow
      % error('Inflow BC not finished on south boundary');
      if strcmpi(scheme,'UDS')
        bs = vs(i)>=0;
        asb = bs*vs(i)/dy(1) + gam(np)*dinv;
      elseif strcmpi(scheme,'CDS')
        asb = gam(np)*dinv - vs(i)/dy(1);
      else
        error(springf('scheme = %s is not supported'))
      end
      ap(np) = ap(np) + asb;
      b(np) = b(np) + asb*sbc(i,2);
    case 7  %  Outflow
            %  do nothing
            
      % error('Outflow BC not finished on south boundary');
%       if vs(i)>0
%         warning(sprintf('Inflow at outflow boundary at i=%d on south boundary',i));
%       end
%       if strcmp(upper(scheme),'UDS')
%         asb = 0;
%       elseif strcmp(upper(scheme),'CDS')
%         asb = vs(i)/dy(1);
%       else
%         error(springf('scheme = %s is not supported'))
%       end
%       ap(np) = ap(np) + asb;
%       b(np) = b(np) + asb*sbc(i,2);
    otherwise
      error('BC type = %d not defined on south boundary',sbc(i,1));
  end
end

% --- Conservative scheme results in ap = sum of neighbors
ap = ap + ae + aw + an + as;

end