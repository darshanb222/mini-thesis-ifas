function fbar = fvave(xu,yv,f)
% fvpave  Compute volume-weighted average of field variable defined at cell centers
%
% Synopsis:  fbar = fvave(x,xu,y,yv,f)
%
% Input:  xu,yv = vectors defining locations of the control volume faces.  xu(i) is
%                 face to the left of x(i).  yv(j) is the face below yv(j)
%         f = dependent variable at the internal nodes
%
% Output:  fbar = volume-weighted average of f

verbose = 1; 

%  Allocate memory and initilize coefficients
nx = length(xu) - 1;  ny = length(yv) - 1;
dx = diff(xu);  dy = diff(yv);

% --- Contribution of heat source at interior control volumes
fsum = 0;
vol = 0;
for i=1:nx
    for j = 1:ny
      np = i + (j-1)*nx;
      dv = dx(i)*dy(j);
      fsum = fsum + dv*f(np);
      vol = vol + dv;
    end
end
fbar = fsum/vol;

end