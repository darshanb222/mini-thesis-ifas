function rout = demoJacobi(pnum,nx,ny,Lx,Ly,makePlots,verbose)
% demoJacobi  Jacobi solution to 2D heat conduction in a rectangle
%             with constant q on all but one boundary
%             All remaining boundaries have constant T.
%
% Synopsis:  demoJacobi
%            demoJacobi(pnum)
%            demoJacobi(pnum,nx)
%            demoJacobi(pnum,nx,ny)
%            demoJacobi(pnum,nx,ny,Lx)
%            demoJacobi(pnum,nx,ny,Lx,Ly)
%            demoJacobi(pnum,nx,ny,Lx,Ly,makePlots)
%            demoJacobi(pnum,nx,ny,Lx,Ly,makePlots,verbose)
%
% Input: pnum = integer value 1 through 4 used to indicate which boundary has
%               uniform heat flux. pnum=1 for heat flux on north boundary,
%               pnum=2 for heat flux on east boundary, pnum=3 for heat flux
%               on south boundary.  pnum=4 for heat flux on west boundary.
%        nx = number of control volumes in the x direction.  Default:  nx = 8
%        ny = number of control volumes in the y direction.  Default:  ny = nx
%        Lx = length of domain in the x direction.  Default:  Lx = 1.23
%        Ly = length of domain in the y direction.  Default:  Ly = 1.68
%        makePlots = flag to control whether 2D and 1D plots of solutions
%                    are created.  Default:  makePlots = true
%        verbose = flag to control whether iteration history is plotted
%                  Default: verbose = false
%
% Output: r = (optional) vector of residuals for iterations

% -- Set default inputs
if nargin<1 || isempty(pnum),       pnum = 1;         end
if nargin<2 || isempty(nx),         nx = 8;           end
if nargin<3 || isempty(ny),         ny = nx;          end
if nargin<4 || isempty(Lx),         Lx  = 1.23;       end
if nargin<5 || isempty(Ly),         Ly  = 1.68;       end
if nargin<6 || isempty(makePlots),  makePlots = true; end
if nargin<7,  verbose = false;  end

% --- Define the mesh, properties, source terms, and boundary conditions
[x,xu,y,yv,src,con,ebc,wbc,nbc,sbc] = model2Da(pnum,nx,Lx,ny,Ly);
if verbose,  showBC(xu,yv,ebc,wbc,nbc,sbc);  end

% --- Compute finite-volume coefficients
[ae,aw,an,as] = fvdiff(x,xu,y,yv,con);

% --- Adjust coefficients to enforce boundary conditions
[ap,ae,aw,an,as,b] = fvbc(x,xu,y,yv,con,src,ebc,wbc,nbc,sbc,ae,aw,an,as);

% --- Jacobi splitting
A = fvAmatrix(nx,ap,ae,aw,an,as);
D = diag(diag(A));
B = D - A;

n = nx*ny;
itermax = 1000;   tol = 5e-4;
rnorm = zeros(n,1);  t = zeros(n,1);
i = 0;

if verbose,  fprintf('\nJacobi iterations\n   it      norm(r)\n');  end
tic
while i<itermax
  
  r = b - A*t;
  i = i + 1;
  rnorm(i) = norm(r,1);  
  t = D\(b + B*t);
  if verbose, fprintf(' %4d  %12.3e\n',i,rnorm(i));  end
  if rnorm(i)/rnorm(1)<tol,  break;  end

end
etime = toc;
rrel = rnorm(1:i)/rnorm(1);

fprintf('\nAfter %d Jacobi iterations, norm(r,1) = %12.2e  rrel = %12.2e\n',i,rnorm(i),rrel(i));
fprintf('\tElapsed solve time = %f seconds\n',etime);

% --- Compute energy balance and create T matrix for plotting
[T,~] = fvpost(x,xu,y,yv,con,src,ebc,wbc,nbc,sbc,t);

if makePlots
  % -- Plot convergence history
  figure('Name','Jacobi Residual History');
  semilogy(rnorm/rnorm(1));  xlabel('Iteration');   ylabel('||r||_1 / ||r_0||_1');

  % --- Make surface, contour, and 1D profile plots
  xp = [0; x];  yp = flipud([0; y]);  [xx,yy] = meshgrid(xp,yp);
  figure('Name','Jacobi Mesh plot') ;    meshc(xx,yy,T);
  figure('Name','Jacobi Contour plot');
  contour(xx,yy,T); colorbar('vert');  axis('equal');
  figure('Name','Jacobi False color plot');
  pcolor(xx,yy,T);  colorbar('vert');  shading('interp');   axis('equal');
  xlabel('x');  ylabel('y');
  figure('Name','Jacobi Profile plot');
  plot(T(:,2),yp,'o-');   xlabel('T(y)');   ylabel('y');
end

if nargout>0, rout = rrel;  end
end
