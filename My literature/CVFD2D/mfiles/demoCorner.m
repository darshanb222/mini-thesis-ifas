function demoCorner(nnx,nny,Lx,Ly)
% demoCorner  2D heat conduction in an insulated corner.
%
%  Synopsis:  demoCorner
%             demoCorner(nnx)
%             demoCorner(nnx,nny)
%             demoCorner(nnx,nny,Lx)
%             demoCorner(nnx,nny,Lx,Ly)
%
%  Input: nnx = (optional) 2-element vector specifying the number of control
%               volumes in the x direction.  nnx(1) is the number of CV on the
%               constant T boundary on the north surface.  nnx(2) is the number of
%               CV on the insulated boundary on the north surface.
%               Default: nnx = [4,2];
%         nny = (optional) 2-element vector analogous to nnx that specifies the
%               number of control volumes in the y direction.  nny(1) is the number
%               of CV on the constant T boundary on the east surface.  nny(2) is the
%               number of CV on the insulated east surface.  Default:  nny = nnx.
%         Lx = (optional) 2-element vector specifying the length of boundary
%               segments on the north surface.  Lx(1) is the length of the constant
%               T surface.  Lx(2) is the length of the insulated surface.
%               Default:  Lx = [ 2 2];
%         Ly = (optional) 2-element vector analogous to Ly that specifies the length
%               of boundary segments on the east surface.  Ly(1) is the length of the
%               constant T surface.  Ly(2) is the length of the insulated surface.
%               Default:  Ly = Lx;

% --- set defaults
if nargin<1,  nnx = [4 2];   end
if nargin<2,  nny = nnx;     end
if nargin<3,  Lx  = [2 2];   end
if nargin<4;  Ly  = Lx;      end
verbose = 1;

% --- Define the mesh and boundar conditions
[x,xu,y,yv,ebc,wbc,nbc,sbc] = setupCorner(nnx,Lx,nny,Ly);
if verbose,  showBC(xu,yv,ebc,wbc,nbc,sbc);  end

% --- Define conductivity and source term for this problem
nx = length(x) - 1;   ny = length(y) - 1;   nn = nx*ny;
src = zeros(nn,1);
con = ones(nn,1);

% --- Compute finite-volume coefficients
[ae,aw,an,as] = fvdiff(x,xu,y,yv,con);

% --- Adjust coefficients to enforce boundary conditions
[ap,ae,aw,an,as,b] = fvbc(x,xu,y,yv,con,src,ebc,wbc,nbc,sbc,ae,aw,an,as);

% --- Set up coefficient matrix
nx = sum(nnx);   %  ny = sum(nny);
% printFivePtCoeff(nx,ny,ae,aw,an,as,ap,b);
A = fvAmatrix(nx,ap,ae,aw,an,as);

% --- Solve for temperature field at internal nodes
t = A\b;

% --- Compute energy balance and create T matrix for plotting
[T,~] = fvpost(x,xu,y,yv,con,src,ebc,wbc,nbc,sbc,t);

% --- Create mesh then make surface, and contour plots
xp = [0; x];  yp = flipud([0; y]);  [xx,yy] = meshgrid(xp,yp);
figure;  meshc(xx,yy,T);
figure;  contour(xx,yy,T);  axis('equal');
figure;  pcolor(xx,yy,T);   colorbar('vert');  shading('interp');   axis('equal'); 
end