function A = fvAmatrix(nx,ap,ae,aw,an,as)
% fvAmatrix  Create sparse penta-diagonal matrix for 2D finite volume scheme.  Coefficients
%            are supplied as five column vectors.
%
% Synopsis:  A = fvAmatrix(nx,ap,ae,aw,an,as)
%
% Input: nx = number of control volumes in the x-direction
%        ap = vector of coefficients for central node in the five point stencil
%        ae = vector of coefficients for east neighbor in the five point stencil
%        aw = vector of coefficients for west neighbor in the five point stencil
%        an = vector of coefficients for north neighbor in the five point stencil
%        as = vector of coefficients for south neighbor in the five point stencil
%
% Output: A = sparse, pentadiagonal matrix

% Note:  A is created with spdiags, which takes an n-by-5 matrix formed from
%        [-as  -aw  ap  -ae   -an].  The columns of this temporary matrix contain
%        shifted versions of the input vectors that must are shifted so that
%        coefficients are stored in correct locations on the diagonals of A

n = length(ap);
znx = zeros(nx,1);   %  temporary vector used for as and an
A = spdiags([ [-as(nx+1:n); znx] [-aw(2:n); 0] ap [0; -ae(1:n-1)] [znx; -an(1:n-nx)] ],...
            [-nx -1 0 1 nx], n,n);
end