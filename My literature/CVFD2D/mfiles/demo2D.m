function demo2D(pnum,nx,ny,Lx,Ly)
% demo2D  2D heat conduction in a rectangle with constant q three boundaries
%         Remaining boundary has constant T.
%
% Synopsis:  demo2D
%            demo2D(pnum)
%            demo2D(pnum,nx)
%            demo2D(pnum,nx,ny)
%            demo2D(pnum,nx,ny,Lx)
%            demo2D(pnum,nx,ny,Lx,Ly)
%
% Input: pnum = integer value 1 through 4 used to indicate which boundary has
%               uniform heat flux. pnum=1 for heat flux on north boundary,
%               pnum=2 for heat flux on east boundary, pnum=3 for heat flux
%               on south boundary.  pnum=4 for heat flux on west boundary.
%        nx = number of control volumes in the x direction.  Default:  nx = 8
%        ny = number of control volumes in the y direction.  Default:  ny = 10
%        Lx = length of domain in the x direction.  Default:  Lx = 1.23
%        Ly = length of domain in the y direction.  Default:  Ly = 1.68

% --- set defaults
if nargin<1,  pnum = 1;    end
if nargin<2,  nx = 8;      end
if nargin<3,  ny = 10;     end
if nargin<4,  Lx  = 1.23;  end
if nargin<5;  Ly  = 1.68;  end
verbose = true;

% --- Define the mesh, properties, source terms, and boundary conditions
[x,xu,y,yv,src,con,ebc,wbc,nbc,sbc] = model2Da(pnum,nx,Lx,ny,Ly);
if verbose,  showBC(xu,yv,ebc,wbc,nbc,sbc);  end

% --- Compute finite-volume coefficients
[ae,aw,an,as] = fvdiff(x,xu,y,yv,con);

% --- Adjust coefficients to enforce boundary conditions
[ap,ae,aw,an,as,b] = fvbc(x,xu,y,yv,con,src,ebc,wbc,nbc,sbc,ae,aw,an,as);

A = fvAmatrix(nx,ap,ae,aw,an,as);  % Set up coefficient matrix
t = A\b;                           % Solve for temperature field at internal nodes

% --- Compute energy balance and create T matrix for plotting
[T,~] = fvpost(x,xu,y,yv,con,src,ebc,wbc,nbc,sbc,t);

% --- Make surface, and contour plots
xp = [0; x];  yp = flipud([0; y]);  [xx,yy] = meshgrid(xp,yp);
figure;  meshc(xx,yy,T);
figure;  contour(xx,yy,T); colorbar('vert');  axis('equal');
figure;  pcolor(xx,yy,T);  colorbar('vert');  shading('interp');   axis('equal');
         xlabel('x');  ylabel('y');
figure;  plot(T(:,2),yp,'o-');   xlabel('T(y)');   ylabel('y');

end