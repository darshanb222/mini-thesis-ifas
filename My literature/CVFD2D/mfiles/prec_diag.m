function y = prec_diag(x,d)
% prec_diag  Diagonal preconditioner for use with conjugate gradient

y = x./d;    %  d is a vector of elements from the diagonal of A

end