function errout = demoModel1(nx,ny,Lx,Ly,verbose)
% demoModel1  2D heat conduction in a rectangle with Dirichlet B.C and
%             sinusoidal source term that gives easy form of exact solution
%
% Synopsis:  demoModel1
%            demoModel1(nx)
%            demoModel1(nx,ny)
%            demoModel1(nx,ny,Lx)
%            demoModel1(nx,ny,Lx,Ly)
%            err = demoModel1(...)
%
% Input: nx = number of control volumes in the x direction.  Default:  nx = 8
%        ny = number of control volumes in the y direction.  Default:  ny = 10
%        Lx = length of domain in the x direction.  Default:  Lx = 1.23
%        Ly = length of domain in the y direction.  Default:  Ly = 1.68
%
% Output: err = L1 norm of error -- compared to exact solution

% --- set defaults
if nargin<1,  nx = 8;          end
if nargin<2,  ny = 10;         end
if nargin<3,  Lx = 1;          end
if nargin<4;  Ly = 2;          end
if nargin<5;  verbose = true;  end

% --- Define uniform mesh in x and y directions
nn = nx*ny;
[x,xu] = fvUniformMesh(nx,Lx);
[y,yv] = fvUniformMesh(ny,Ly);

% --- Preallocate and define boundary data structures
%     All BC are Dirichlet type, so no more changes are needed
ebc = ones(ny,1)*[1 0 0 0 0];  wbc = ebc;
nbc = ones(nx,1)*[1 0 0 0 0];  sbc = nbc;

% --- Source term and exact solution
cx = pi/Lx;    cy = 2*pi/Ly;
tmpx = sin(cx*x(1:nx));
tmpy = sin(cy*y(1:ny));
f = tmpx*tmpy';             %  outer product
src = (cx^2 + cy^2)*f(:);   %  f(:) converts matrix to vector, by columns
exact = f(:);

if verbose,  showBC(xu,yv,ebc,wbc,nbc,sbc);  end

% --- Compute finite-volume coefficients
con = ones(nn,1);     %  uniform conductivity
[ae,aw,an,as] = fvdiff(x,xu,y,yv,con);

% --- Adjust coefficients to enforce boundary conditions
[ap,ae,aw,an,as,b] = fvbc(x,xu,y,yv,con,src,ebc,wbc,nbc,sbc,ae,aw,an,as);

A = fvAmatrix(nx,ap,ae,aw,an,as);  % Set up coefficient matrix
t = A\b;                           % Solve for temperature field at internal nodes
err = norm(t-exact)/nn;
if nargout>0
  errout = err;
else
  fprintf('\nL1 norm of error = %12.5e\n',err);
end

% --- Compute energy balance and create T matrix for plotting
T = fvpost(x,xu,y,yv,con,src,ebc,wbc,nbc,sbc,t,verbose);

% --- Make surface, and contour plots
if ~verbose,  return;  end
xp = [0; x];  yp = flipud([0; y]);  [xx,yy] = meshgrid(xp,yp);
figure;  meshc(xx,yy,T);  xlabel('x');  ylabel('y');  view(-122,16)
figure;  contour(xx,yy,T); colorbar('vert');  axis('equal');
figure;  pcolor(xx,yy,T);  colorbar('vert');  shading('interp');   axis('equal');
         xlabel('x');  ylabel('y');
figure;  plot(T(:,2),yp,'o-');   xlabel('T(y)');   ylabel('y');
end
