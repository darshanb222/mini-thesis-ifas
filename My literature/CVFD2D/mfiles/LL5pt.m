function [mp,mew,mns] = LL5pt(nx,ny,ap,aw,as)
%LL5pt  Incomplete Cholesky decomposition of a PDE discretized on a 5 point stencil.
%       The coefficient matrix is stored as 5 vectors of length n.  The decomposition
%       is stored as 3 vectors of length n
%
% Synopsis:  [mp,mew,mns] = LL5pt(nx,ny,ap,aw,as)
%
% Input:  nx,ny = number of unknowns (interior nodes) in x and y directions.
%         ap,aw,as = diagonals of the coefficient matrix corresponding to the following
%                    stencil.  Mesh points are referred to with compass point notation where
%                    neighors of point "P" are "west" = (i-1,j), "north" = (i,j+1), etc.
%
%                                       N
%                                       |
%                                       |
%                                 W --- P --- E
%                                       |
%                                       |
%                                       S
%
%          Using Fortran notation one row of the matrix product A*f = b is written
%
%            -as(i,j)*f(i,j-1) - aw(i,j)*f(i-1,j) + ap(i,j)*f(i,j)
%                            - ae(i,j)*f(i+1,j) - an(i,j)*f(i,j+1) = b(i,j)                  
%
%          where f is the unknown field (phi).  Natural ordering is used to map
%          the (i,j) mesh notation to the (k,l) indices of the a coefficient matrix
%                     k = i + (j-1)*nx
%          Thus, the (i,j) notation of the grid is mapped to the kth row of A as
%
%            -as(k)*f(k-nx) - aw(k)*f(k-1) + ap(k)*f(k) - ae(k)*f(k+1) - an(k)*f(k+nx) = b(k)                  
%
%          Finally, since the A matrix must be symmetric for the Cholesky decomposition
%          to exist, only ap, aw and as must be input (aw = ae, as = an).  In the A
%          matrix ap is the main diagonal, ae and aw are the first super and sub
%          diagonals, and an and as are the super and sub diagonals a distance nx
%          from the main diagonal.
%
%          NOTE:  The Cholesky factorization requires that A is symmetric.  Thus
%                 aw(k) = ae(k) and an(k) = as(k).  The redundant inputs are used
%                 anyway for compatibility (?) with nonsymmetric solvers to be
%                 developed later
%
% Output: mp,mew,mns = coefficients of the lower triangular matrix approximating a
%                      Cholesky factorization of A.  Specifically, A = L*L' + R, where
%                      L*L' has the same pattern of nonzeros as A, and R contains the
%                      leftovers, which are ignored.  mp is main diagonal.  mew is first
%                      subdiagonal (corresponds) to the east and west coefficients.
%                      mns is the subdiagonal a disance nx from the main diagonal

% Reference:  See, e.g., Figure 3.4-1, p. 212 of "Introduction to Parallel and
%             Vector Solution of Linear Systems", J.M. Ortega, 1988, Plenum Press
%             or Figure 9.2.2, p. 390"Scientific Computing: and Introduction with
%             Parallel Computing", G. Golub and J.M. Ortega, 1993, Academic Press

n = nx*ny;

mp = zeros(n,1);  mew = mp;  mns = mp;   %  preallocate for speed

mp(1) = sqrt(ap(1));
mew(2) = aw(2)/mp(1);
mns(1+nx) = as(1+nx)/mp(1);

for i=2:nx
   mp(i) = sqrt(ap(i) - mew(i)^2);
   mew(i+1) = aw(i+1)/mp(i);
   mns(i+nx) = as(i+nx)/mp(i);
end

for i=nx+1:n-nx
   mp(i) = sqrt(ap(i) - mns(i)^2 - mew(i)^2);
   mew(i+1) = aw(i+1)/mp(i);
   mns(i+nx) = as(i+nx)/mp(i);
end

for i=n-nx+1:n-1
   mp(i) = sqrt(ap(i) - mns(i)^2 - mew(i)^2);
   mew(i+1) = aw(i+1)/mp(i);
end

mp(n) = sqrt(ap(n) - mns(n)^2 - mew(n)^2);

end
