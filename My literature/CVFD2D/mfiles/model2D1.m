function [x,xu,y,yv,src,con,ebc,wbc,nbc,sbc] = model2D1(nx,Lx,ny,Ly)
% model2D1  Create mesh and BC data structures for 2D test problem
%           Constant q on pnum boundary.  Constant T elsewhere

% --- Define uniform mesh in x and y directions
[x,xu] = fvUniformMesh(nx,Lx);
[y,yv] = fvUniformMesh(ny,Ly);

% --- Uniform conductivity
nx = length(x) - 1;   ny = length(y) - 1;   nn = nx*ny;
con = ones(nn,1);

% --- Preallocate data structures by initializing to zeros
%     All BC are Dirichlet type, so no more changes are needed
ebc = ones(ny,5);  wbc = ebc;
nbc = ones(nx,5);  sbc = nbc;

% --- Source term
cx = pi/Lx;    cy = 2*pi/Ly;
tmpx = sin(cx*x(1:nx));
tmpy = sin(cy*y(1:ny));
f = tmpx*tmpy';               %  outer product
src = (cx^2 + cy^2)*f(:);     %  f(:) converts matrix to vector, by columns

end