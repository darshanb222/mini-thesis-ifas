function [ae,aw,an,as] = fvcoef(x,xu,y,yv,con)
% fvdiff  Coefficients of diffusion equation for finite volume scheme on a 2D grid
%
% Synopsis:  [ap,ae,aw,an,as] = fvdiff(x,xu,y,yv,con)
%
% Input:  x, xu = vectors defining position of cell centers (x) and control volume
%                 interfaces (xu) in x direction.  nx+1 elements each
%         y, yv = vectors defining position of cell centers (y) and control volume
%                 interfaces (yv) in y direction.  ny+1 elements each.
%         con = vector of gamma values for each cell.  con has nx*ny elements
%
% Output:  ae, aw, an, as = vectors of coefficients for the five point finite
%               volume stencil

% --- Allocate memory and initilize coefficients
nx = length(x) - 1;  ny = length(y) - 1;   nn = nx*ny;
ae = zeros(nn,1);  aw = ae;  an = ae;  as = ae;

% --- Compute grid quantities that are reused
dx = diff(xu);          %  control volume width in x-direction
dy = diff(yv);
dxw = [x(1); diff(x)];  %  distance between adjacent nodes  dxw(i) = x(i)-x(i-1)
dys = [y(1); diff(y)];  %  dys = y(j)-y(j-1)

% --- Diffusion coefficients for the north face of interior control volumes
%     Loop over j, vectorized computation of all values along i
for j=1:ny-1
  bbeta = (yv(j+1) - y(j))/dys(j+1);
  np = (1:nx)' + (j-1)*nx;   %  for i=1:nx,  np = i + (j-1)*nx
  con_n = con(np).*con(np+nx)./(bbeta*con(np+nx) + (1-bbeta)*con(np));
  an(np)    = con_n/(dy(j)*dys(j+1));
  as(np+nx) = con_n/(dy(j+1)*dys(j+1));
end

% --- Diffusion coefficients for the east face of interior control volumes
%     Loop over i, vectorized computation of all values along j
for i=1:nx-1
  bbeta = (xu(i+1) - x(i))/dxw(i+1);
  np = i + ( ( (1:ny)' - 1) *nx );  %  for j=1:ny,  np = i + (j-1)*nx
  con_e = con(np).*con(np+1)./(bbeta*con(np+1) + (1-bbeta)*con(np));
  ae(np)   = con_e/(dx(i)*dxw(i+1));
  aw(np+1) = con_e/(dx(i+1)*dxw(i+1));
end

end