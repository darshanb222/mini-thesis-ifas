function demoModel3(nx,ny,kratio)
% demoModel3  2D heat conduction with non-uniform constant phi BC, nonuniform
%             conductivity, and source term
%
% Synopsis:  demoModel3
%            demoModel3(nx)
%            demoModel3(nx,ny)
%            demoModel3(nx,ny,kratio)
%
% Input: nx = number of control volumes in the x direction.  Default:  nx = 8
%        ny = number of control volumes in the y direction.  Default:  ny = 10
%        kratio = ratio of thermal conductivity in the core region to the
%                 thermal conductivity in outer region.  Default:  kratio = 4

% --- Set defaults
if nargin<1,  nx = 16;     end
if nargin<2,  ny = nx;     end
if nargin<3,  kratio = 4;  end
verbose = false;

% --- Check for mesh compatibility
if rem(nx,4) ~= 0
    error('Mesh cannot fit the model: value of nx must be divisible by 4');
end
if rem(ny,4) ~= 0
    error('Mesh cannot fit the model: value of ny must be divisible by 4');
end
% --- Indices used to subdivide the mesh
nx1 = nx/4;  nx2 = nx/2;   ny1 = ny/4;  ny2 = ny/2;  nn = nx*ny;

% --- Define uniform mesh in x and y directions
Lx = 1;  Ly = 1;
[x,xu] = fvUniformMesh(nx,Lx);
[y,yv] = fvUniformMesh(ny,Ly);

% --- Preallocate and define boundary data structures
%     All BC are Dirichlet type, so no more changes are needed
ebc = ones(ny,1)*[1 0 0 0 0];  wbc = ones(ny,1)*[1 20 0 0 0];
nbc = ones(nx,1)*[1 0 0 0 0];  sbc = ones(nx,1)*[1 10 0 0 0];

%  Allocate memory for gamma and source and set tentative values
k1 = 1.0;
k2 = kratio*k1;
con = k1*ones(nn,1);
src = zeros(nn,1);

%  Overwrite central region with different value of gamma and source
source = 1000;
for i=nx1+1:nx2+nx1
   for j=ny1+1:ny2+ny1
      ii = i + (j-1)*nx;
      con(ii) = k2;
      src(ii) = source;
   end
end

if verbose,  showBC(xu,yv,ebc,wbc,nbc,sbc);  end

% --- Compute finite-volume coefficients
[ae,aw,an,as] = fvdiff(x,xu,y,yv,con);

% --- Adjust coefficients to enforce boundary conditions
[ap,ae,aw,an,as,b] = fvbc(x,xu,y,yv,con,src,ebc,wbc,nbc,sbc,ae,aw,an,as);

A = fvAmatrix(nx,ap,ae,aw,an,as);  % Set up coefficient matrix
t = A\b;                           % Solve for temperature field at internal nodes

% --- Compute energy balance and create T matrix for plotting
T = fvpost(x,xu,y,yv,con,src,ebc,wbc,nbc,sbc,t);

% --- Make surface, and contour plots
xp = [0; x];  yp = flipud([0; y]);  [xx,yy] = meshgrid(xp,yp);
figure;  meshc(xx,yy,T);  xlabel('x');  ylabel('y');  view(-122,16)
figure;  contour(xx,yy,T); colorbar('vert');  axis('equal');
figure;  pcolor(xx,yy,T);  colorbar('vert');  shading('interp');   axis('equal');
         xlabel('x');  ylabel('y');
figure;  plot(T(:,2),yp,'o-');   xlabel('T(y)');   ylabel('y');

end