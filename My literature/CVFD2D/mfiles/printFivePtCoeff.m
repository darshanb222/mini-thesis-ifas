function printFivePtCoeff(nx,ny,ae,aw,an,as,ap,b)
% printFivePtCoeff  Display CVFD coefficients for debugging

printa(nx,ny,ae,'ae');
printa(nx,ny,aw,'aw');
printa(nx,ny,an,'an');
printa(nx,ny,as,'as');
printa(nx,ny,ap,'ap');
printa(nx,ny,b,'b');

end

% ================
function printa(nx,ny,a,aname)

i = find(a~=0);
s = 1 + round(log10( min(min(abs(a(i)))) ) );
as = a/10^s;

fprintf('\n\n%s (*10^(%d)) = \n',aname,-s);
fprintf('   i =');  fprintf('%9d',1:nx);  fprintf('\n');

for j=ny:-1:1
  fprintf('j =%3d',j);
  n = (1:nx) + (j-1)*nx;
  fprintf('%9.2f',as(n));
  fprintf('\n');
end

end