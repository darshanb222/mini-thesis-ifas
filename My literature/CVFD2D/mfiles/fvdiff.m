function [ae,aw,an,as] = fvdiff(x,xu,y,yv,gam)
% fvdiff  Coefficients of diffusion equation for finite volume scheme on a 2D grid
%
% Synopsis:  [ap,ae,aw,an,as] = fvdiff(x,xu,y,yv,gam)
%
% Input:  x,xu = vectors defining position of cell centers (x) and control volume
%                interfaces (xu) in x direction.  nx+1 elements each
%         y,yv = vectors defining position of cell centers (y) and control volume
%                interfaces (yv) in y direction.  ny+1 elements each.
%         gam = vector of gamma values for each cell.  gam has nx*ny elements
%
% Output:  ae,aw,an,as = vectors of diffusion coefficients for the five point
%                        finite volume stencil on a Cartesian mesh.  Mesh spacing
%                        is not assumed to be uniform

% --- Allocate memory and initilize coefficients
nx = length(x) - 1;  ny = length(y) - 1;   nn = nx*ny;
ae = zeros(nn,1);  aw = ae;  an = ae;  as = ae;

% --- Compute grid quantities that are reused
dx = diff(xu);          %  control volume width in x-direction
dy = diff(yv);
dxw = [x(1); diff(x)];  %  distance between adjacent nodes  dxw(i) = x(i)-x(i-1)
dys = [y(1); diff(y)];  %  dys = y(j)-y(j-1)

% --- Diffusion coefficients for the north face of interior control volumes
%     Loop over j, vectorized computation of all values along i
for j=1:ny-1
  bbeta = (yv(j+1) - y(j))/dys(j+1);
  np = (1:nx)' + (j-1)*nx;   %  for i=1:nx,  np = i + (j-1)*nx
  gam_n = gam(np).*gam(np+nx)./(bbeta*gam(np+nx) + (1-bbeta)*gam(np));
  an(np)    = gam_n/(dy(j)*dys(j+1));
  as(np+nx) = gam_n/(dy(j+1)*dys(j+1));
end

% --- Diffusion coefficients for the east face of interior control volumes
%     Loop over i, vectorized computation of all values along j
for i=1:nx-1
  bbeta = (xu(i+1) - x(i))/dxw(i+1);
  np = i + ( ( (1:ny)' - 1) *nx );  %  for j=1:ny,  np = i + (j-1)*nx
  gam_e = gam(np).*gam(np+1)./(bbeta*gam(np+1) + (1-bbeta)*gam(np));
  ae(np)   = gam_e/(dx(i)*dxw(i+1));
  aw(np+1) = gam_e/(dx(i+1)*dxw(i+1));
end

end