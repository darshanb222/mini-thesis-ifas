function [ap,ae,aw,an,as,b] = fvbc(x,xu,y,yv,con,src,ebc,wbc,nbc,sbc,ae,aw,an,as)
% fvbc  Modify coefficients of 5pt finite volume scheme to account for boundary conditions
%
% Synopsis:  
%
% Input:  x,xu,y,yv = vectors defining the grid.  See mesh routines
%         con = 
%
% Output:

%  Allocate memory and initilize coefficients
nx = length(x) - 1;  ny = length(y) - 1;   % nn = nx*ny;
ap = zeros(size(ae));
dx = diff(xu);
dy = diff(yv);
if isempty(src)
  b = zeros(size(ap));
else
  b = src;
end

%  printGrid(x,xu,y,yv,dx,dy)

% ------------------------------------
%  Coefficients on the east boundary
%  No harmonic mean is used to compute con_e because the east neighbor
%  (boundary point) is on the interface.  Compute the aeb value, use it
%  to eliminate the boundary value, then set the ae(np) to zero
dinv = 1.0 / ( (xu(nx+1)-xu(nx)) * (xu(nx+1)-x(nx)) );
dxi = xu(nx+1) - x(nx);    %  distance from near boundary node to boundary
for j=1:ny
  np = nx + (j-1)*nx;               %  i=nx
  ae(np) = 0.0;
  switch ebc(j,1)
    case 1  %  constant T
      aeb = con(np)*dinv;             %  true boundary coefficient
      b(np) = b(np) + aeb*ebc(j,2);
      ap(np) = ap(np) + aeb;
    case 2  %  constant q
      b(np) = b(np) + ebc(j,3)/dx(nx);
    case 3  %  Convection
      tmp = ebc(j,4)*con(np)/(ebc(j,4)*dxi + con(np));  %  h*k/(h*dyi + k)
      b(np) = b(np) + tmp*ebc(j,5)/dx(nx);              % + tmp*Tinf/deltax
      ap(np) = ap(np) + tmp/dx(nx);
    case 4  %  Symmetry
       %  do nothing
    otherwise
      error('BC type = %d not defined on east boundary',ebc(j,1));
  end
end

% ------------------------------------
%  Coefficients on the west boundary.  Analogous to east boundary
dinv =  1.0/( xu(2)*x(1) );    %  xu(1) = x(0) = 0
dxi = x(1) - xu(1);            %  distance from boundary to near boundary node
for j=1:ny
  np = 1 + (j-1)*nx;           %  i=1
  aw(np) = 0;
  switch wbc(j,1)
    case 1  %  constant T
      awb = con(np)*dinv;             %  true boundary coefficients
      b(np) = b(np) + awb*wbc(j,2);
      ap(np) = ap(np) + awb;
    case 2  %  constant q
      b(np) = b(np) + wbc(j,3)/dx(1);
    case 3  %  Convection
      tmp = wbc(j,4)*con(np)/(wbc(j,4)*dxi + con(np));  %  h*k/(h*dyi + k)
      b(np) = b(np) + tmp*wbc(j,5)/dx(1);               % + tmp*Tinf/deltax
      ap(np) = ap(np) + tmp/dx(1);
    case 4  %  Symmetry
      %  do nothing
    otherwise
      error('BC type = %d not defined on west boundary',wbc(j,1));
  end
end

% ------------------------------------
%  Coefficients on the north boundary.
%  No harmonic mean is used to compute con_n because the north neighbor
%  (boundary point) is on the interface.  Analogous to computation of aeb, above.
dinv = 1.0 / ( (yv(ny+1)-yv(ny)) * (yv(ny+1)-y(ny)) );
dyi = yv(ny+1) - y(ny);
for i=1:nx
  np = i + (ny-1)*nx;               %  j=ny
  an(np) = 0;
  switch nbc(i,1)
    case 1  %  constant T
      anb = con(np)*dinv;           %  true boundary coefficient
      b(np) = b(np) + anb*nbc(i,2);
      ap(np) = ap(np) + anb;
    case 2  %  constant q
      b(np) = b(np) + nbc(i,3)/dy(ny);
    case 3  %  Convection
      tmp = nbc(i,4)*con(np)/(nbc(i,4)*dyi + con(np));  %  h*k/(h*dyi + k)
      b(np) = b(np) + tmp*nbc(i,5)/dy(ny);              % + tmp*Tinf/deltay
      ap(np) = ap(np) + tmp/dy(ny);
    case 4  %  Symmetry
      %  do  nothing
    otherwise
      error('BC type = %d not defined on north boundary',nbc(i,1));
  end
end

%  Coefficients on the south boundary.  Analogous to north boundary
dinv =  1.0/( yv(2)*y(1) );    %  yv(1) = y(0) = 0
dyi = y(1) - yv(1);
for i=1:nx
  np = i;                           %  j=1
  as(np) = 0;
  switch sbc(i,1)
    case 1  %  constant T
      asb = con(np)*dinv;           %  true boundary coefficient
      b(np) = b(np) + asb*sbc(i,2);
      ap(np) = ap(np) + asb;
    case 2  %  constant q
      b(np) = b(np) + sbc(i,3)/dy(1);
    case 3  %  Convection
      tmp = sbc(i,4)*con(np)/(sbc(i,4)*dyi + con(np));
      b(np) = b(np) + tmp*sbc(i,5)/dy(1);
      ap(np) = ap(np) + tmp/dy(1);
    case 4  %  Symmetry
      %  do  nothing
    otherwise
      error('BC type = %d not defined on south boundary',sbc(i,1));
  end
end

%  Conservative scheme results in ap = sum of neighbors
ap = ap + ae + aw + an + as;
end