function demoShipWallHalf(nnx,nny,Lx,Ly)
% demoShipWallHalf  Model of 2D heat conduction in a ship wall.  Use symmetry
%                   through center of T beam to reduce the problem size
%
%  Synopsis:  demoShipWallHalf
%             demoShipWallHalf(nnx)
%             demoShipWallHalf(nnx,nny)
%
%  Input: nnx = (optional) 3-element vector specifying the number of
%                control volumes along 3 segments of the x direction.
%                Default:  nnx = [8 2 1]
%         nny = (optional) 5-element vector specifying the number of
%                control volumes along 4 segments of the y direction.
%                Default:  nnx = [4 1 6 1]
%         Lx = (optional) 3-element vector specifying the length of 5
%               segments defining geometric features in the x-direction
%               Default: Lx = [255 40 5]*1e-3 meters.  Each segment
%               Lx(1:5) is divided uniformally into nx(1:5) CV sides
%         Ly = (optional) 4-element vector specifying the length of 4
%               segments defining geometric features in the y-direction
%               Default: Ly = [50 10 140 10]*1e-3 meters.  Each segment
%               Ly(1:5) is divided uniformally into ny(1:5) CV sides

% --- Set defaults
if nargin<1,  nnx = [8 2 1];              end
if nargin<2,  nny = [4 1 6 1];                end
if nargin<3,  Lx  = [255 40 5]*1e-3;  end
if nargin<4;  Ly  = [50 10 140 10]*1e-3;      end
verbose = 1;

% --- Physical paramters
ksteel = 40;   %  thermal conductivity of selected steel, W/m/K
kins = 0.039;  %  thermal conductivity of insulation, W/m/K
Tair = -18;    %  degrees C
Tw   =  15;
hair = 20;     %  W/m^2/C
hw   = 3000;

% --- Define the mesh, properties, source terms, and boundary conditions
[x,xu,y,yv,src,con,ebc,wbc,nbc,sbc] = setupShipHalfWall(nnx,Lx,nny,Ly,ksteel,kins,Tair,Tw,hair,hw);
if verbose && (sum(nnx)*sum(nny)<500)   %  Don't show mesh if it's very fine
  showBC(xu,yv,ebc,wbc,nbc,sbc,con);
end

% --- Compute finite-volume coefficients
[ae,aw,an,as] = fvdiff(x,xu,y,yv,con);

% --- Adjust coefficients to enforce boundary conditions
[ap,ae,aw,an,as,b] = fvbc(x,xu,y,yv,con,src,ebc,wbc,nbc,sbc,ae,aw,an,as);

% --- Set up coefficient matrix
nx = sum(nnx);
A = fvAmatrix(nx,ap,ae,aw,an,as);

% --- Solve for temperature field at internal nodes
tic;
t = A\b;
et = toc;
n = sum(nnx)*sum(nny);
fprintf('\n\t %f seconds to solve %d by %d system\n',et,n,n);

% --- Compute energy balance and create T matrix for plotting
[T,~] = fvpost(x,xu,y,yv,con,src,ebc,wbc,nbc,sbc,t);

% --- Create xx and yy grid matrices, then make mesh, surface, and contour plots
xp = [0; x];  yp = flipud([0; y]);  [xx,yy] = meshgrid(xp,yp);
figure;  meshc(xx,yy,T);
figure;  contour(xx,yy,T); colorbar('vert');  axis('equal');
figure;  pcolor(xx,yy,T);  colorbar('vert');  shading('interp');   axis('equal');   axis('tight');
figure;  plot(xx,T(1,:)-Tw,'r-',xx,T(end,:)-Tair,'b-');
         xlabel('x  (mm)');  ylabel('\Delta T  ^\circ C');
         legend('T_{steel} - T_w','T_{ins}-T_{air}','Location','northwest');
end