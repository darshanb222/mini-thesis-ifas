function demoDuctHeating(V,ptype,ny,nx,H,L,verbose)
% mainConvectAngle  2D convection problem demonstrating false diffusion
%
% Synopsis:  demoDuctHeating
%            demoDuctHeating(V)
%            demoDuctHeating(V,ptype)
%            demoDuctHeating(V,ptype,nny)
%            demoDuctHeating(V,ptype,nny,nnx)
%            demoDuctHeating(V,ptype,nny,nnx,H)
%            demoDuctHeating(V,ptype,nny,nnx,H,L)
%
% Input: V = (optional) fluid velocity.  Default:  V = 0.01 m/s
%        ptype = (optional) type of velocity profile.  ptype = 1 for plug flow
%                ptype = 2 for fully developed flow;  Default: ptype=1
%        ny = (optional) 2-element vector analogous to nnx that specifies the number
%              of control volumes in the y direction.  Default:  nny = [2 8];
%        nnx = (optional) 2-element vector specifying the number of control volumes
%              in each of two segments in the x direction.  Default:  nnx = [2 8]
%        H = (optional) height of the duct
%        L = (optional) length of the duct
%        V = (optional) fluid velocity

% --- set defaults
if nargin<1 || isempty(V),       V = 0.03;         end
if nargin<2 || isempty(ptype),   ptype = 1;        end
if nargin<3 || isempty(ny),      ny = 32;          end
if nargin<4 || isempty(nx),      nx = 16;          end
if nargin<5 || isempty(H),       H = 6e-3;         end
if nargin<6 || isempty(L),       L = 17e-2;        end
if nargin<7 || isempty(verbose), verbose = false;  end

% --- Properties of water at 35 C
rho = 994;           %  kg/m^3
cp = 4178;           %  J/kg/K
k = 0.6248;          %  W/m/C
mu = 7.145e-4;       %  kg/m/s
Re = rho*V*2*H/mu;   %  Reynolds number
alpha = k/rho/cp;    %  thermal diffusivity,  m^2/s
Tin = 34;            %  Inlet temperature
Q = 10;              %  Power input for experiment (W)
W = 6.9e-2;          %  Width of true heater (m) used to compute heat input/width
% qn = Q/(W*L);        %  Heat flux on north boundary
% qn = Q/W
qn = (Q/W)/rho/cp;   %  W/m

warning('CVFD:notDone','\n\tCode is not reliable: energy balance does not close\n');

% --- Define the mesh, properties, source terms, and boundary conditions
switch ptype
  case 1
    uniFlow = false;
    flowType = sprintf('Plug flow: Re = %g',round(Re));
  case 2
    uniFlow = true;
    flowType = sprintf('Fully developed flow: Re = %g',round(Re));
  otherwise
    error('ptype = %d not allowed',ptype);
end
fprintf('\n%s\n',flowType);
[x,xu,y,yv,src,gam,ebc,wbc,nbc,sbc,u,v] = setupDuct(H,L,V,qn,Tin,nx,ny,alpha,uniFlow);

% if verbose,  showBC(xu,yv,ebc,wbc,nbc,sbc);  end

% --- Compute finite-volume coefficients
scheme = 'UDS';
if strcmp(upper(scheme),'UDS')
  [ae,aw,an,as] = fvUDS(x,xu,y,yv,rho,gam,u,v);
elseif strcmpi(scheme,'CDS')
  [ae,aw,an,as] = fvCDS(x,xu,y,yv,rho,gam,u,v);
else
  error('scheme = %s is not supported',scheme);
end
fprintf('%s advection scheme\n',scheme);

% --- Adjust coefficients to enforce boundary conditions
[ap,ae,aw,an,as,b] = fvbcflow(x,xu,y,yv,rho,gam,src,ebc,wbc,nbc,sbc,ae,aw,an,as,scheme);

% --- Set up coefficient matrix
if nx<8
  printFivePtCoeff(nx,ny,ae,aw,an,as,ap,b);
end
A = fvAmatrix(nx,ap,ae,aw,an,as);

% --- Solve for temperature field at internal nodes
t = A\b;

% --- Compute energy balance and create T matrix for plotting
[T,ebal,qe,qw,qn,qs,~] = fvpostFlow(x,xu,y,yv,rho,gam,src,ebc,wbc,nbc,sbc,t);

fprintf('\n\nEnergy balance in units of W/m\n\n');
fprintf('\trho*cp*qe   = %12.3f\n',rho*cp*qe);
fprintf('\trho*cp*qw   = %12.3f\n',rho*cp*qw);
fprintf('\trho*cp*qn   = %12.3f\n',rho*cp*qn);
fprintf('\trho*cp*qs   = %12.3f\n',rho*cp*qs);
fprintf('\t---------------------------\n');
fprintf('\trho*cp*ebal = %12.3f\n',rho*cp*ebal);

% --- Compute bulk energy flow as second check on energy balance
% ne = nx + ((1:ny)-1)*nx;  %  vector of indices of nodes adjacent to east boundary
  
dy = diff(yv);   %  dx = diff(xu);  
Tbout = 0;
flowOut = 0;
ue = ebc(:,6);               %  velocities on east face of cells on east boundary
Te = flipud(T(2:end-1,end)); %  updated ebc(:,2) values are not returned from fvpostFlow
for j=1:ny
  % np = nx + (j-1)*nx;
  flow = ue(j)*dy(j);
  flowOut = flowOut + flow;
  Tbout = Tbout + flow*Te(j);
end
Tbout = Tbout/flowOut;

Tbin = 0;
flowIn = 0;
uw = wbc(:,6);             %  velocities on west face of cells on west boundary
Tw = flipud(T(2:end-1,1)); %  updated wbc(:,2) values are not returned from fvpostFlow
for j=1:ny
  %  np = 1 + (j-1)*nx;
  flow = uw(j)*dy(j);
  flowIn = flowIn + flow;
  Tbin = Tbin + flow*Tw(j);
end
Tbin = Tbin/flowIn;

fprintf('\nBulk energy balance: mdot*cp*(Tout-Tin)\n')
fprintf('flowIn                     = %12.4e\n',flowIn);
fprintf('flowOut                    = %12.4e\n',flowOut);
fprintf('flowIn-flowOut             = %12.4e\n',flowIn-flowOut);
fprintf('TbIn, TbOut                = %9.4f  %9.4f\n',Tbin,Tbout);
fprintf('flowIn*rho*cp*(Tbout-Tbin) = %9.4f\n',flowIn*rho*cp*(Tbout-Tbin));

% ---
if verbose
  fprintf('\n\nVertical velocity and temperature profiles\n');
  fprintf('      y(j)     uw(j)    Tw(j)     ue(j)    Te(j)\n');
  for j=ny:-1:1
    fprintf('  %8.3f  %8.4f  %8.4f %8.4f  %8.4f\n',100*y(j),uw(j),Tw(j),ue(j),Te(j));
  end
end

% % --- heuristic fix for corner points
% T(1,1) = 1;      %  upper left corner
% T(end,1) = 0;    %  lower left corner
% T(1,end) = 1;
% T(end,end) = 0;
% T(T>5) = 2;
% T(T<-5) = -2;

% --- Make surface, and contour plots
xp = 100*[0; x];  yp = 100*flipud([0; y]);

[xx,yy] = meshgrid(xp,yp);
figure;  meshc(xx,yy,T);
xlabel('x  (cm)');   ylabel('y  (cm)');
set(gca,'DataAspectRatio',[1 1 5000]);

figure;  pcolor(xx,yy,T);  colorbar('vert');  shading('interp');   axis('equal');

% --- Plot temperature profile at exit  T(i,j) <-> T( y(i), x(j) )
figure;  plot(T(:,end),yp,'o-');  ylabel('y');  xlabel('T(y) at exit');
Te = T(:,end);
ye = yp;
if uniFlow
  fname = sprintf('Ty_uniform_ny%d',ny);
else
  fname = sprintf('Ty_fullyDev_ny%d',ny);
end
save(fname,'Te','ye','ue','Re','flowType');
fprintf('\nData saved in %s\n',fname);

% --- Plot temperature along heated wall  T(i,j) <-> T( y(i), x(j) )
figure;
Tn = T(1,:);
plot(xp,Tn,'o-');  xlabel('x');  ylabel('T(x) along heated surface');
if uniFlow
  fname = sprintf('Tn_uniform_ny%d',ny);
else
  fname = sprintf('Tn_fullyDev_ny%d',ny);
end
save(fname,'Tn','xp','Re','Q','L','W','flowType')
save(fname,'Te','ye','ue','Re','flowType');
end

% ===============================
function [x,xu,y,yv,src,gam,ebc,wbc,nbc,sbc,u,v] = setupDuct(H,L,V,qn,Tin,nnx,nny,alpha,uniFlow)
% setupConvectAngle  Create mesh, BC data structures, and flow field for convection test
%                    problem involving prescribed flow at angle to the mesh.

% --- Mesh consists of two blocks in each direction.  fvUniMeshBlocks does all the work
[x,xu] = fvUniMeshBlocks(nnx,L);
[y,yv] = fvUniMeshBlocks(nny,H);

% --- Velocity field is easy
nx = length(x) - 1;   ny = length(y) - 1;   nn = nx*ny;
u = V*ones(nn,1);
v = zeros(nn,1);

% --- Assign fully-developed flow profile
if ~uniFlow
  Ucl = V*3/2;    %  centerline velocity
  yH = y(1:end-1)/H;
  uy = 4*Ucl*( yH - yH.^2 );
  for i=1:nx-1
    np = i + ( ( (1:ny)' - 1) *nx );  %  for j=1:ny,  np = i + (j-1)*nx
    u(np) = uy;
  end
end

% --- Material properties andn internal source term
gam = alpha*ones(nn,1);
src = zeros(nn,1);

% --- Preallocate data structures by initializing to zeros
ebc = zeros(ny,6);  wbc = ebc;
nbc = zeros(nx,6);  sbc = nbc;

% --- Inflow BC on west, with step change in phi.
wbc(:,1) = 6*ones(ny,1);            %  inflow BC, type = 6
wbc(:,6) = u(1:nx:nn-nx+1);         %  normal velocity component, consistent with u
wbc(1:nny(1),2) = Tin*ones(ny,1);   %  Uniform T on inlet boundary

% --- Outflow BC on east
ebc(:,1) = 7*ones(ny,1);      %  set BC type = 7
ebc(:,6) = wbc(:,6);          %  normal velocity component

% --- Adiabatic BC on south wall
sbc(:,1) = 2*ones(nx,1);      %  Use constant q BC, type = 1
sbc(:,3) = zeros(nx,1);       %  zero heat flux

% --- Uniform heat flux on north
nbc(:,1) = 2*ones(nx,1);      %  Use constant q BC, type = 2
nbc(:,3) = qn*ones(nx,1);     %  heat flux per unit length
end