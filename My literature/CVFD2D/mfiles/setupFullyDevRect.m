function [x,xu,y,yv,src,con,ebc,wbc,nbc,sbc] = setupFullyDevRect(nx,Lx,ny,Ly,mu,dpdz)
% setupFullyDevRect  Create mesh and BC matrices for fully developed flow in a rectangle

% --- Mesh is uniform in both x and y directions
[x,xu] = fvUniformMesh(nx,Lx);
[y,yv] = fvUniformMesh(ny,Ly);

% --- Uniform diffusion coefficient and uniform source term
nx = length(x) - 1;   ny = length(y) - 1;   nn = nx*ny;
src = dpdz*ones(nn,1);
con = mu*ones(nn,1);

% --- Preallocate data structures by initializing to zeros
ebc = zeros(ny,5);  wbc = ebc;
nbc = zeros(nx,5);  sbc = nbc;

% --- No slip condition (w=0) on all boundaries:  type = 1 for constant w
nbc(:,1) = ones(nx,1);   % first column is type, no value needed
sbc(:,1) = nbc(:,1);
ebc(:,1) = ones(ny,1);
wbc(:,1) = ebc(:,1);

end