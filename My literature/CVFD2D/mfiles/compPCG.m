function compPCG(nx,conratio)
% compPCG  Compare performance of preconditioned conjugate gradient methods
%          for solving the a system of equaitons arising from applying the
%          CVFD method to a 2D heat conduciton problem with a non-uniform
%          conductivity
%
% Synopsis:  compPCG
%            compPCG(nx)
%            compPCG(nx,conratio)
%
% Input: nx = number of control volumes in the x direction.  Default:  nx = 16
%             Note: ny = nx for all problems
%        conratio = ratio of thermal conductivity in the core region to the
%                 thermal conductivity in outer region.  Default:  kratio = 4
%
% Output: res = (optional) vector of residuals for the iterative method

% --- Set defaults
if nargin<1,  nx = 16;          end
if nargin<2,  conratio = 4;     end

rcg = demoPCG(nx,nx,conratio,2);    %  method = 2 for plain CG
rdpcg = demoPCG(nx,nx,conratio,3);  %  method = 3 for diagonal PCG
ricpcg = demoPCG(nx,nx,conratio,4); %  method = 4 for Incomplete Cholesky PCG

figure('Name','Compare Conjugate Gradient Preconditioners')
semilogy((1:length(rcg))',rcg,'--',...
       (1:length(rdpcg))',rdpcg,'.',...
       (1:length(ricpcg))',ricpcg,'+');
xlabel('Iteration');  ylabel('Residual');
legend('Plain CG','Diagonal PCG','Incomplete Cholesky PCG');

end

