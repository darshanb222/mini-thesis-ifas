function compare_convect_scheme_details

nx=32; ny=32;

load(sprintf('%s_%dx%d.mat','UDS',nx,ny))
Au = A;
bu = b;
tu = t;

load(sprintf('%s_%dx%d.mat','CDS',nx,ny))
Ac = A;
bc = b;
tc = t;

fprintf('\nnorm(bc-bu) = %e\n',norm(bc-bu))
fprintf('\nnorm(tc-tu) = %e\n',norm(tc-tu))
fprintf('\nnorm(Ac-Au) = %e\n',norm(Ac-Au,1))

end