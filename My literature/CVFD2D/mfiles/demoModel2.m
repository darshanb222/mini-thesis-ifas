function demoModel2(nx,ny,Lx,Ly)
% demoModel2  2D heat conduction in a rectangle with Dirichlet BC and uniform
%             Source term.
%
% Synopsis:  demoModel2
%            demoModel2(nx)
%            demoModel2(nx,ny)
%            demoModel2(nx,ny,Lx)
%            demoModel2(nx,ny,Lx,Ly)
%
% Input: nx = number of control volumes in the x direction.  Default:  nx = 8
%        ny = number of control volumes in the y direction.  Default:  ny = 10
%        Lx = length of domain in the x direction.  Default:  Lx = 1.23
%        Ly = length of domain in the y direction.  Default:  Ly = 1.68

% --- set defaults
if nargin<1,  nx = 8;   end
if nargin<2,  ny = 10;  end
if nargin<3,  Lx  = 1;  end
if nargin<4;  Ly  = 2;  end
verbose = false;

% --- Define uniform mesh in x and y directions
[x,xu] = fvUniformMesh(nx,Lx);
[y,yv] = fvUniformMesh(ny,Ly);

% --- Uniform conductivity
nn = nx*ny;   con = ones(nn,1);

% --- Preallocate and define boundary data structures
%     All BC are Dirichlet type, so no more changes are needed
ebc = ones(ny,1)*[1 0 0 0 0];  wbc = ebc;
nbc = ones(nx,1)*[1 0 0 0 0];  sbc = nbc;

% --- Source term
src = ones(nx*ny,1);

if verbose,  showBC(xu,yv,ebc,wbc,nbc,sbc);  end

% --- Compute finite-volume coefficients
[ae,aw,an,as] = fvdiff(x,xu,y,yv,con);

% --- Adjust coefficients to enforce boundary conditions
[ap,ae,aw,an,as,b] = fvbc(x,xu,y,yv,con,src,ebc,wbc,nbc,sbc,ae,aw,an,as);

A = fvAmatrix(nx,ap,ae,aw,an,as);  % Set up coefficient matrix
t = A\b;                           % Solve for temperature field at internal nodes

% --- Compute energy balance and create T matrix for plotting
T = fvpost(x,xu,y,yv,con,src,ebc,wbc,nbc,sbc,t);

% --- Make surface, and contour plots
xp = [0; x];  yp = flipud([0; y]);  [xx,yy] = meshgrid(xp,yp);
figure;  meshc(xx,yy,T);  xlabel('x');  ylabel('y');  view(-122,16)
figure;  contour(xx,yy,T); colorbar('vert');  axis('equal');
figure;  pcolor(xx,yy,T);  colorbar('vert');  shading('interp');   axis('equal');
         xlabel('x');  ylabel('y');
figure;  plot(T(:,2),yp,'o-');   xlabel('T(y)');   ylabel('y');

end