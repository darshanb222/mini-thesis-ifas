function showGrid(xu,yv)
% showGrid  Plot locations of control volume faces of a 2D finite volume mesh

xmin = xu(1);  xmax = xu(end);  ymin = yv(1);  ymax = yv(end);

% --- draw the frame in blue
plot([0 xmax],[0 0],'b-',[0 xmax],[ymax ymax],'b-',...
     [0 0],[0 ymax],'b-',[xmax xmax],[0 ymax],'b-');

% --- draw constant x grid lines
hold on
for i=2:length(xu)
  plot([xu(i) xu(i)],[0 ymax],'g--');
end

% --- draw constant y grid lines
for j=2:length(yv)
  plot([0 xmax],[yv(j) yv(j)],'g--');
end
hold off
axis('equal');   %  use true aspect ratio

end