function demo1Dqx(nnx,nny,Lx,Ly)
% demo1Dqx  Verify 2D heat conduction code with a 1D problem.  Boundary conditions
%           are adjusted so that heat flow is in x-direction only.  Uniform q BC
%           is imposed on east boundary.  Constant T on west boundary.  Symmetry
%           on north and south.
%
% Synopsis:  demo1Dqx
%            demo1Dqx(nnx)
%            demo1Dqx(nnx,nny)
%            demo1Dqx(nnx,nny,Lx)
%            demo1Dqx(nnx,nny,Lx,Ly)
%
% Input: nnx = (optional) 2-element vector specifying the number of control volumes
%              in each of two segments in the x direction.  Default:  nnx = [16 2]
%        nny = (optional) 2-element vector analogous to nnx that specifies the number
%              of control volumes in the y direction.  Default:  nny = [2 2];
%        Lx = (optional) 2-element vector specifying lengths of domain segments in
%              the x direction.  Default:  Lx = [0.5 0.75]
%        Ly = (optional) 2-element vector specifying lengths of domain segments in
%              the y direction.  Default:  Ly = [0.5 0.5]

% --- set defaults
if nargin<1,  nnx = [16 2];      end
if nargin<2,  nny = [2 2];       end
if nargin<3,  Lx  = [0.5 0.75];  end
if nargin<4;  Ly  = [0.5 0.5];   end
verbose = 1;

% --- Define the mesh, properties, source terms, and boundary conditions
Te = 80;  Tw = 50;
[x,xu,y,yv,src,con,ebc,wbc,nbc,sbc] = test1qxSetup(nnx,Lx,nny,Ly,Te,Tw);
if verbose,  showBC(xu,yv,ebc,wbc,nbc,sbc);  end

% --- Compute finite-volume coefficients
[ae,aw,an,as] = fvdiff(x,xu,y,yv,con);

% --- Adjust coefficients to enforce boundary conditions
[ap,ae,aw,an,as,b] = fvbc(x,xu,y,yv,con,src,ebc,wbc,nbc,sbc,ae,aw,an,as);

% --- Set up coefficient matrix
nx = sum(nnx);  %  ny = sum(nny);
A = fvAmatrix(nx,ap,ae,aw,an,as);

% --- Solve for temperature field at internal nodes
t = A\b;

% --- Compute energy balance and create T matrix for plotting
[T,~] = fvpost(x,xu,y,yv,con,src,ebc,wbc,nbc,sbc,t);

% --- Make surface, and contour plots
xp = [0; x];  yp = flipud([0; y]);  [xx,yy] = meshgrid(xp,yp);
figure;  meshc(xx,yy,T);
figure;  contour(xx,yy,T); colorbar('vert');  axis('equal');
figure;  pcolor(xx,yy,T);  colorbar('vert');  shading('interp');   axis('equal');

% --- Plot temperature along constant y
figure;  plot(xp,T(2,:),'o-',[min(xp) max(xp)],[Te Te],'r--',[min(xp) max(xp)],[Tw Tw],'b--');
dT = 0.04*( max([Te Tw]) - min([Te Tw]) );
text(mean(xp),Te+dT,sprintf('Te = %-4.1f',Te));
text(mean(xp),Tw+dT,sprintf('Tw = %-4.1f',Tw));
xlabel('x');  ylabel('T(x)');
end

% ===============================
function [x,xu,y,yv,src,con,ebc,wbc,nbc,sbc] = test1qxSetup(nnx,Lx,nny,Ly,Te,Tw)
% test1cxSetup  Create mesh and BC data structures for 1D test problem
%              Convective BC on east and west boundaries.  Symmetry on North and south

% --- Define the mesh:  x and y are divided into two zones with different spacing
[x1,xu1] = fvUniformMesh(nnx(1),Lx(1));
[x2,xu2] = fvUniformMesh(nnx(2),Lx(2));

% --- Combine grids by adding offset to x2 and eliminating redundant points
del = Lx(1);     x2 = x2 + del;   xu2 = xu2 + del;
x  = [x1(1:end-1);  x2];
xu = [xu1(1:end-1); xu2];

% --- Repeat for y-direction mesh
[y1,yv1] = fvUniformMesh(nny(1),Ly(1));
[y2,yv2] = fvUniformMesh(nny(2),Ly(2));

del = Ly(1);    y2 = y2 + del;   yv2 = yv2 + del;
y  = [y1(1:end-1);  y2];
yv = [yv1(1:end-1); yv2];

% --- Uniform conductivity and zero source term for this problem
kond = 1;
nx = length(x) - 1;   ny = length(y) - 1;   nn = nx*ny;
src = zeros(nn,1);
con = ones(nn,1);

% --- Preallocate data structures by initializing to zeros
ebc = zeros(ny,5);  wbc = ebc;
nbc = zeros(nx,5);  sbc = nbc;

% --- Symmetry on north and south boundaries:  type = 4 for symmetry
nbc(:,1) = 4*ones(nx,1);   % first column is type, no value needed
sbc(:,1) = nbc(:,1);

% --- Uniform q BC on east, contant T on west.  type = 2 for heat flux BC
qe = kond*(Te-Tw)/sum(Lx);
vones = ones(ny,1);    %  avoid repeated re-creation of this vector
ebc(:,1) = 2*vones;    %  convection BC
ebc(:,3) = qe*vones;   %  h = 20 on east boundary

wbc(:,1) = vones;      %  constant T type BC
wbc(:,2) = Tw*vones;   %  T = 50
end