function [U,X,Y] = make2DijField(nx,ny,x,xu,y,yv,u,ue,uw,un,us)
% make2DijField  Convert vector of interior values to matrix where (i,j) <==> (y,x)
%
% Synopsis:  U = make2DijField(nx,ny,x,xu,y,yv,u,ue,uw,un,us)
%
% Input:  nx,ny = number of *interior* control volumes in the x and y direction
%         x,y,  = vectors of locations of control volume centers (nodes).  x(1) and y(1)
%                 are the (x,y) coordinates of the *first interior* CV, not the boundary
%                 node at the corner (x,y) = (0,0).  For convenience in computing the
%                 control-volume finite-difference coefficients, the location of the nodes
%                 on the "far" boundaries (east and north) are stored as x(nx+1), y(ny+1).
%                 Thus, the number of elements in x is nx+1, and the number of elements
%                 in y is ny+1, and x(nx+1) = Lx, y(ny+1) = Ly, where Lx and Ly are the
%                 lengths of the domain in the x and y directions, respectively.
%         xu,yv = vectors of locations of control volume faces.  xu(i) is the control
%                 volume face to the left (negative x-direction) of a node at x(i)
%                 yv(j) is the control volume face below (negative y-direction) of
%                 a node at y(j).  Thus, xu(1)=0 and yv(1)=0.  For convenience, we
%                 also define xu(nx+1) = Lx, and yv(nx1) = Ly
%         u     = vector of field values at the *interior* control volumes.  u(i,j)
%                 is the value of u at (x(i),y(j)).  The elements of u are stored in
%                 natural ordering:  u(n) = u(x(i),y(j)), where n = i + nx*(j-1)
%         ue,uw = column vector of boundary values for u on the east and west boundaries
%                 ue and uw *do not* contain values for the corner nodes.  For example,
%                 ue(1) is the value of the boundary node adjacent 
%                 ue(1) is value of corner node at (x,y) = (xmax,0), ue(end) is the value
%                 of the corner node at (x,y) = (xmax,ymax).
%         un,us = column vector of boundary values for u on the north and south boundaries
%                 un(1) is value of corner node at (x,y) = (0,ymax), un(end) is the value
%                 of the corner node at (x,y) = (xmax,ymax).  Note that ue(end)
%                 and un(end) are redundant.  However, the corner nodes are ill-defined
%
% Output: U = matrix constructed of interior and boundary values of the input u field.
%             The rows and columns of U correspond to a 2D Cartesian mesh the origin in
%             the lower left corner.  U is created in such a way that mesh(U), surf(U), etc
%             produce plots that look like they belong on the finite-volume mesh defined
%             by the x and y vectors input to this routine.
%         X,Y = (optional) matrices of mesh implied by the structure of U.  U(j,i) is the
%             value of the u field at X(j,i) and Y(j,i), which corresponds to the CV
%             located at x(i) and y(j).

% --- First make make a matrix version of the vector of interior unknowns
%
% Consider v = (1:6)' corresponding to nx = 3, ny = 2.  The elements of v are the
% indices of the interior unknowns with a "natural" ordering (x index varies most quickly)
% Transform v to a matrix VVV suitable for display by MATLAB with mesh, meshc, etc.
%  v --> V --> VV --> VVV
%
%       1
%       2                            1  4               1 2 3                       4 5 6
%  v =  3    V = reshape(v,nx,ny) =  2  5     VV = V' = 4 5 6    VVV = flipud(VV) = 1 2 3
%       4                            3  6
%       5
%       6
%
U = flipud(reshape(u,nx,ny)');

% --- Now add the boundary values around the edges
U = [0 un' 0;  flipud(uw) U flipud(ue); 0 us' 0];

% --- grid points used in the surface plot
xi = [0;  x(1:nx);  xu(nx+1)];
yj = [0;  y(1:ny);  yv(ny+1)];
nxp = nx+2;  nyp = ny+2;

%  --- Heuristic interpolation at corner points:
%  Corner points are not defined precisely.  Use 1D linear extrapolation along x and y
%  to get two values that are then averaged.  For a more general interpretation
%  of the corner value use two-dimensional interpolation.  That seemed excessive
%  in this demonstration program.

xrat = (xi(1)-xi(2))/(xi(3)-xi(2));
yrat = (yj(nyp)-yj(nyp-1))/(yj(nyp-1)-yj(nyp-2));
unwx = U(1,2) + xrat*(U(1,3) - U(1,2));           %  1D interpolation along x, northwest corner
unwy = U(2,1) + yrat*(U(2,1) - U(3,1));           %  1D interpolation along y
U(1,1) = (unwx+unwy)/2;                           %  average at NW corner

yrat = (yj(1)-yj(2))/(yj(3)-yj(2));
uswx = U(nyp,2) + xrat*(U(nyp,3) - U(nyp,2));      %  1D interpolation along x
uswy = U(nyp-1,1) + yrat*(U(nyp-2,1)-U(nyp-1,1));  %  1D interpolation along y
U(nyp,1) = (uswx+uswy)/2;                          %  average at SW corner

xrat = (xi(nxp)-xi(nxp-1))/(xi(nxp-2)-xi(nxp-1));
yrat = (yj(nyp)-yj(nyp-1))/(yj(nyp-1)-yj(nyp-2));
unex = U(1,nxp-1) + xrat*(U(1,nxp-2) - U(1,nxp-1));        %  1D interpolation along x
uney = U(2,nxp)   + yrat*(U(2,nxp)   - U(3,nxp));          %  1D interpolation along y
U(1,nxp) = (unex+uney)/2;                                  %  average at NE corner

yrat = (yj(1)-yj(2))/(yj(3)-yj(2));
usex = U(nyp,nxp-1) + xrat*(U(nyp,nxp-2) - U(nyp,nxp-1));  %  1D interpolation along x
usey = U(nyp-1,nxp) + yrat*(U(nyp-2,nxp) - U(nyp-1,nxp));  %  1D interpolation along y
U(nyp,nxp) = (usex+usey)/2;                                %  averate at SE corner

if nargout>1
  [X,Y] = meshgrid(xi,yj);
end

end