function [fReOut,DhOut] = demoFullyDevRect(nx,ny,Lx,Ly)
% demoFullyDevRect  Fully-developed laminar flow in a rectangular duct.
%
% Synopsis:  demoFullyDevRect
%            demoFullyDevRect(nx)
%            demoFullyDevRect(nx,ny)
%            demoFullyDevRect(nx,ny,Lx)
%            demoFullyDevRect(nx,ny,Lx,Ly)
%             fRe     = demoFullyDevRect(...)
%            [fRe,Dh] = demoFullyDevRect(...)
%
% Input: nx = number of control volumes in the x direction.  Default:  nx = 4
%        ny = number of control volumes in the y direction.  Default:  ny = nx
%        Lx = length of the duct in the x direction.    Default:  Lx = 1
%        Ly = length of the domain in the y direction.  Default:  Ly = Lx
%
% Output: fRe = (optional) product of f*Re obtained from solution.  If fRe is
%               returned, no print out or plots are created
%         Dh  = (optional) hydraulic diameter for the duct.

% --- Set defaults for optional inputs
if nargin<1,  nx = 4;   end
if nargin<2,  ny = nx;  end
if nargin<3,  Lx = 1;   end
if nargin<4;  Ly = Lx;  end

% --- Constants for this analysis
tic;                        %  Reset execution timer
dpdz = -pi;  mu = sqrt(5);  %  Arbitrary pressure gradient and viscosity values

% --- Define the mesh, material properties, source terms, and boundary conditions
[x,xu,y,yv,src,con,ebc,wbc,nbc,sbc] = setupFullyDevRect(nx,Lx,ny,Ly,mu,-dpdz);

% --- Compute finite-volume coefficients
[ae,aw,an,as] = fvdiff(x,xu,y,yv,con);

% --- Adjust coefficients to enforce boundary conditions
[ap,ae,aw,an,as,b] = fvbc(x,xu,y,yv,con,src,ebc,wbc,nbc,sbc,ae,aw,an,as);

% --- Set up coefficient matrix
A = fvAmatrix(nx,ap,ae,aw,an,as);
etSetup = toc;   %  measures time from preceding "tic"

% --- Solve for temperature field at internal nodes;  tic/toc measures execution time
tic;  w = A\b;   etSolve = toc;

% --- Extract engineering quantities
tic;                             %  reset execution timer
wbar = fvave(xu,yv,w);           %  average velocity
w = w/wbar;                      %  Normalize the velocity field.  w is now dimensionless
Dh = 4*(Lx*Ly)/(2*(Lx+Ly));      %  hydraulic diameter
fRe = -dpdz*2*Dh^2/(wbar*mu);    %  product of f and Re

% --- If user expects return values, supply them and return w/out plots
if nargout>0
  fReOut = fRe;
  if nargout>1,  DhOut = Dh;  end
  return
end

% --- Print and plot results
fprintf('wbar, Dh, fRe = %8.4f  %8.4f  %8.4f\n',wbar,Dh,fRe);
%  Create xx and yy grid matrices, then make mesh, surface, and contour plots
xp = [0; x];  yp = flipud([0; y]);  [xx,yy] = meshgrid(xp,yp);
W = fvpost(x,xu,y,yv,con,src,ebc,wbc,nbc,sbc,w,0);   %  matrix representation of w field
figure;  meshc(xx,yy,W);  shading('interp');  colorbar('vert');  axis('equal');
figure;  contour(xx,yy,W); colorbar('vert');  axis('equal');
figure;  pcolor(xx,yy,W);  colorbar('vert');  shading('interp');   axis('equal');
etPost = toc;
fprintf('\nElapsed times:\n');
fprintf('\tSetup       %8.3f\n\tSolve       %8.3f\n\tPost-process%8.3f\n',...
          etSetup,etSolve,etPost);
end