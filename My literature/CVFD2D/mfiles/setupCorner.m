function [x,xu,y,yv,ebc,wbc,nbc,sbc] = setupCorner(nx,Lx,ny,Ly)
% setupCorner  Finite volume mesh and BC for heat conduction in a corner
%
% Synopsis:  [x,xu,y,yv,ebc,wbc,nbc,sbc] = setupCorner(nx,Lx)
%            [x,xu,y,yv,ebc,wbc,nbc,sbc] = setupCorner(nx,Lx,ny)
%            [x,xu,y,yv,ebc,wbc,nbc,sbc] = setupCorner(nx,Lx,ny,Ly)
%
% Input:  nx = two-element vector;  nx(1) = number of control volumes in
%              x-direction grid on top surface at temperature T1.
%              nx(2) = number of CV in the second section (insulated corner)
%              of the x-direction grid
%         Lx = two-element vector;  Lx(1) = length of exposed top surface
%              at temperature T1.  Lx(2) = length of top surface covered by
%              insulation
%         ny = two-element vector;  ny(1) = number of control volumes in
%              y-direction grid on right surface at temperature T2.
%              ny(2) = number of CV in the second section (insulated corner)
%              of the y-direction grid
%         Ly = two-element vector;  Ly(1) = length of exposed right surface
%              at temperature T2.  Ly(2) = length of right surface covered by
%              insulation
%
% Output:  x,y = (vector) locations of center nodes of the mesh
%          xu,yv = (vector) locations of control volume interfaces in x and y
%                  directions, respectively.
%          ebc,wbc,nbc,sbc = matrices defining boundary conditions on east, west
%                            north and south boundaries, respectively

if nargin<2,  error('nx and Lx must be supplied');  end
if nargin<3,  ny = nx;  end
if nargin<4,  Ly = Lx;  end

% --- Temperature constants
T1 = 5;
T2 = 35;

% --- Mesh consists of two blocks in each direction.  fvUniMeshBlocks does all the work
[x,xu] = fvUniMeshBlocks(nx,Lx);
[y,yv] = fvUniMeshBlocks(ny,Ly);

% ------------------------------
% --- Initialize
ebc = zeros(sum(ny),5);  wbc = ebc;
nbc = zeros(sum(nx),5);  sbc = nbc;

% --- West boundary is insulated
wbc(:,1) = 2*ones(sum(ny),1);           %  type = 2 for constant q
wbc(:,3) = zeros(sum(ny),1);            %  zero heat flux at insulated boundary

% --- East boundary has constant T and insulated segments
ebc(1:ny(1),1) = ones(ny(1),1);        %  type = 1 for constant T
ebc(1:ny(1),2) = T1*ones(ny(1),1);     %  set T value on this boundary segment
ebc(ny(1)+1:end,1) = 2*ones(ny(2),1);  %  type = 2 for constant q
ebc(ny(1)+1:end,2) = zeros(ny(2),1);   %  q = 0 on sinulated boundary

% --- South boundary is insulated
sbc(:,1) = 2*ones(sum(nx),1);           %  type = 2 for constant q
sbc(:,3) = zeros(sum(nx),1);            %  zero heat flux at insulated boundary

% --- North boundary has constant T and insulated segments
nbc(1:nx(1),1) = ones(nx(1),1);        %  type = 1 for constant T
nbc(1:nx(1),2) = T2*ones(nx(1),1);     %  set T value on this boundary seqment
nbc(nx(1)+1:end,1) = 2*ones(nx(2),1);  %  type = 2 for constant q
nbc(nx(1)+1:end,2) = zeros(nx(2),1);   %  q = 0 on sinulated boundary

end