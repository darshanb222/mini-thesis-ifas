function rout = demoPCG(nx,ny,conratio,method,tol,maxit,verbose)
% demoPCG  Solve Poisson equation with pre-conditioned conjugate gradient algorithm
%
% Synopsis:  demoPCG
%            demoPCG(nx)
%            demoPCG(nx,ny)
%            demoPCG(nx,ny,conratio)
%            demoPCG(nx,ny,conratio,method)
%            demoPCG(nx,ny,conratio,method,tol)
%            demoPCG(nx,ny,conratio,method,tol,maxit)
%            demoPCG(nx,ny,conratio,method,tol,maxit,verbose)
%            res = demoPCG(...)
%
% Input: nx = number of control volumes in the x direction.  Default:  nx = 16
%        ny = number of control volumes in the y direction.  Default:  ny = 16
%        conratio = ratio of thermal conductivity in the core region to the
%                 thermal conductivity in outer region.  Default:  kratio = 4
%        method = (integer) used to select solution method from one of
%                 the following.  Default: method = 2,  plain conjugate gradient
%
%	      method       solution method
%       ------       -----------------
%          1         direct solution of A*u = b with sparse matrix A
%          2         plain conjugate gradient method
%          3         conjugate gradient with diagonal preconditioning
%          4         conjugate gradient with Incomplete Cholesky preconditioning
%
%       tol = convergence tolerance for iterative methods.  Default: tol = 5e-6
%       maxit = maximum number of iterations for iterative methods
%               Default: maxit = 1000
%
% Output: res = (optional) vector of residuals for the iterative method

% --- Set defaults
if nargin<1 || isempty(nx),       nx = 16;          end
if nargin<2 || isempty(ny),       ny = nx;          end
if nargin<3 || isempty(conratio), conratio = 4;     end
if nargin<4 || isempty(method),   method = 3;       end
if nargin<5 || isempty(tol),      tol = 5e-6;       end
if nargin<6 || isempty(maxit),    maxit = 1000;     end
if nargin<7,  verbose = false;                      end

% -- Constants for all problems
convergence_plot = true;
Lx = 1;
Ly = 1;

% -- Evaluation finite volume coefficients
[x,xu,y,yv,ebc,wbc,nbc,sbc,con,src,ap,ae,aw,an,as,b] = model3(nx,ny,Lx,Ly,conratio,verbose);

% -- Set up coefficient matrix
A = fvAmatrix(nx,ap,ae,aw,an,as);

% -- Solve the system
switch method
  
  case 1

    methodName = 'Gaussian elimination';
    tic;  t = A\b;  etime = toc;
    iter = 0;  relres = norm(b - A*t)/norm(b);   res = 0;  flag = 0;
    if verbose, figure('Name','Sparse matrix structure');  spy(A);  end

  case 2

    methodName = 'plain Conjugate Gradient';
    tic;
    [t,flag,relres,iter,res] = pcg(A,b,tol,maxit);
    etime = toc;

  case 3

    methodName = 'CG with diagonal preconditioning';
    tic;
    [t,flag,relres,iter,res] = pcg(A,b,tol,maxit,@prec_diag,[],[],ap);
    etime = toc;

  case 4

    methodName = 'CG with incomplete Cholesky preconditioning';

    % -- Form the Cholesky Decomposition of "A"
    tic;
    [mp,mew,mns] = LL5pt(nx,ny,ap,aw,as);
    n = nx*ny;
    L = sparse(1:n,1:n,mp,n,n) - sparse(2:n,1:n-1,mew(2:n),n,n) ...
                               - sparse(nx+1:n,1:n-nx,mns(nx+1:n),n,n);
    %   and solve
    [t,flag,relres,iter,res] = pcg(A,b,tol,maxit,L',L);
    etime = toc;

  otherwise
    error('method = %d not allowed',method)
end

if method>1   %  Common output for iterative methods
  if flag>0
    warning('PCG:convergence',...
            '%s did not converge: flag = %d\n\t solution from iteration %d',...
             methodName,flag,iter);
  end
  if convergence_plot
    fr = figure;  set(fr,'Name',sprintf('Residual from %s',methodName));
    semilogy((1:length(res))',res);  xlabel('Iteration');  ylabel('Residual');
  end
end

fprintf('\nCondition number estimate:  condest(A) = %11.3e\n',condest(A));
fprintf('\nAfter %d iterations of %s\n',length(res),methodName);
fprintf('\tstatus = %d, relres = %12.4e\n',flag,relres);
fprintf('\telapsed time = %6.3f\n',etime);

% --- Compute energy balance and create T matrix for plotting
[T,ebal] = fvpost(x,xu,y,yv,con,src,ebc,wbc,nbc,sbc,t,verbose);

if nargout>0, rout = res;  end
if ~verbose
  fprintf('\n\tenergy balance = %12.3e\n',ebal);
  return;
end

% --- Make surface, and contour plots
xp = [0; x];  yp = flipud([0; y]);  [xx,yy] = meshgrid(xp,yp);
figure;  meshc(xx,yy,T);   xlabel('x');       ylabel('y');  view(-122,16)
figure;  contour(xx,yy,T); colorbar('vert');  axis('equal');
figure;  pcolor(xx,yy,T);  colorbar('vert');  shading('interp');
         axis('equal');    xlabel('x');       ylabel('y','Rotation',0);
figure;  plot(T(:,2),yp,'o-');   xlabel('T(y)');   ylabel('y','Rotation',0);

end