function demo1Dcy(nnx,nny,Lx,Ly)
% demo1Dcy  Verify 2D heat conduction code with a 1D problem.  Boundary conditions
%           are adjusted so that heat flow is in y-direction only.  Convective BC
%           is imposed on north boudary.  Constant T on south boundary.  Symmetry
%           on east and west.
%
% Synopsis:  demo1Dcy
%            demo1Dcy(nnx)
%            demo1Dcy(nnx,nny)
%            demo1Dcy(nnx,nny,Lx)
%            demo1Dcy(nnx,nny,Lx,Ly)
%
% Input: nnx = (optional) 2-element vector specifying the number of control volumes
%              in each of two segments in the x direction.  Default:  nnx = [2 2]
%        nny = (optional) 2-element vector analogous to nnx that specifies the number
%              of control volumes in the y direction.  Default:  nny = [16 2];
%        Lx = (optional) 2-element vector specifying lengths of domain segments in
%              the x direction.  Default:  Lx = [0.5 0.5]
%        Ly = (optional) 2-element vector specifying lengths of domain segments in
%              the y direction.  Default:  Ly = [0.5 0.75]

% --- set defaults
if nargin<1,  nnx = [2 2];       end
if nargin<3,  nny = [16 2];      end
if nargin<2,  Lx  = [0.5 0.5];   end
if nargin<4;  Ly  = [0.5 0.75];  end
verbose = 1;

% --- Define the mesh, properties, source terms, and boundary conditions
[x,xu,y,yv,src,con,ebc,wbc,nbc,sbc] = test1cySetup(nnx,Lx,nny,Ly);
if verbose,  showBC(xu,yv,ebc,wbc,nbc,sbc);  end

% --- Compute finite-volume coefficients
[ae,aw,an,as] = fvdiff(x,xu,y,yv,con);

% --- Adjust coefficients to enforce boundary conditions
[ap,ae,aw,an,as,b] = fvbc(x,xu,y,yv,con,src,ebc,wbc,nbc,sbc,ae,aw,an,as);

% --- Set up coefficient matrix
nx = sum(nnx);   %  ny = sum(nny)
A = fvAmatrix(nx,ap,ae,aw,an,as);

% --- Solve for temperature field at internal nodes
t = A\b;

% --- Compute energy balance and create T matrix for plotting
[T,~] = fvpost(x,xu,y,yv,con,src,ebc,wbc,nbc,sbc,t);

% --- Make surface, and contour plots
xp = [0; x];  yp = flipud([0; y]);  [xx,yy] = meshgrid(xp,yp);
figure;  meshc(xx,yy,T);
figure;  contour(xx,yy,T); colorbar('vert');  axis('equal');
figure;  pcolor(xx,yy,T);  colorbar('vert');  shading('interp');   axis('equal');

figure;  plot(T(:,2),yp,'o-');   xlabel('T(y)');   ylabel('y');
end

% ===============================
function [x,xu,y,yv,src,con,ebc,wbc,nbc,sbc] = test1cySetup(nnx,Lx,nny,Ly)
% test1cySetup  Create mesh and BC data structures for 1D test problem
%               Convective BC on north.  Constant T on south.  Symmetry on east and west.

% --- Define the mesh:  x and y are divided into two zones with different spacing
[x1,xu1] = fvUniformMesh(nnx(1),Lx(1));
[x2,xu2] = fvUniformMesh(nnx(2),Lx(2));

% --- Combine grids by adding offset to x2 and eliminating redundant points
del = Lx(1);     x2 = x2 + del;   xu2 = xu2 + del;
x  = [x1(1:end-1);  x2];
xu = [xu1(1:end-1); xu2];

% --- Repeat for y-direction mesh
[y1,yv1] = fvUniformMesh(nny(1),Ly(1));
[y2,yv2] = fvUniformMesh(nny(2),Ly(2));

del = Ly(1);    y2 = y2 + del;   yv2 = yv2 + del;
y  = [y1(1:end-1);  y2];
yv = [yv1(1:end-1); yv2];

% --- Uniform conductivity and zero source term for this problem
nx = length(x) - 1;   ny = length(y) - 1;   nn = nx*ny;
src = zeros(nn,1);
con = ones(nn,1);

% --- Preallocate data structures by initializing to zeros
ebc = zeros(ny,5);  wbc = ebc;
nbc = zeros(nx,5);  sbc = nbc;

% --- Symmetry on east and west boundaries:  type = 4 for symmetry
ebc(:,1) = 4*ones(ny,1);   %  first column is type, no value needed
wbc(:,1) = ebc(:,1);

% --- Convective BC on north, constant T on south.  type = 3 for convection
vones = ones(nx,1);    %  temporary column vector
nbc(:,1) = 3*vones;    %  convection BC
nbc(:,4) = 20*vones;   %  h = 20 on east boundary
nbc(:,5) = 80*vones;   %  Tfluid = 80

sbc(:,1) = vones;      %  constant T type BC
sbc(:,2) = 50*vones;   %  T = 50

end