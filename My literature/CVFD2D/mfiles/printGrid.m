function printGrid(x,xu,y,yv,dx,dy)
% printGrid  Print the elements of a grid struct

if nargin<5,  dx = diff(xu);  end
if nargin<6,  dy = diff(yv);  end

fprintf('\n\nContents of grid variables\n');
nx = length(x);  Lx = xu(end);
ny = length(y);  Ly = yv(end);
fprintf('nx = %d    ny = %d    Lx = %f    Ly = %f\n',nx,ny,Lx,Ly);

fprintf('size(x)  = %d  %d\n',size(x));
fprintf('size(xu) = %d  %d\n',size(xu));
fprintf('size(dx) = %d  %d\n',size(dx));
fprintf('size(y)  = %d  %d\n',size(y));
fprintf('size(yv) = %d  %d\n',size(yv));
fprintf('size(y)  = %d  %d\n',size(dy));

fprintf('\n   i        x(i)       xu(i)      dx(i)\n');
for i=1:nx-1
  fprintf(' %3d    %8.3f    %8.3f    %8.3f\n',i,x(i),xu(i),dx(i));
end
for i=nx:nx
  fprintf(' %3d    %8.3f    %8.3f\n',i,x(i),xu(i));
end

fprintf('\n   j        y(j)       yv(j)     dy(j)\n');
for j=1:ny-1
  fprintf(' %3d    %8.3f    %8.3f    %8.3f\n',j,y(j),yv(j),dy(j));
end
for j=ny:ny
  fprintf(' %3d    %8.3f    %8.3f\n',j,y(j),yv(j));
end

end