function convergeModel1
% convergeModel1  Verify truncation error with exact solution to model 1

fprintf('\n  nx=ny   nx*ny    err/nn    err(i-1)/err(i)\n');
n=[5 10 20 40 80 160 320];
Lx = 1;  Ly = Lx;
dx = Lx./n;
err = zeros(size(n));
for i=1:length(n)
  close all;
  err(i) = demoModel1(n(i),n(i),Lx,Ly,false);
  fprintf(' %4d  %6d  %12.3e',n(i),n(i)^2,err(i));
  if i==1
    fprintf('\n');
  else
    fprintf(' %8.4f\n',err(i-1)/err(i));
  end
end

imid = fix(length(err)/2) - 1;       %  Middle index of the err vector
eref = err(imid)*(dx/dx(imid)).^3;   %  O(dx^2) variation of error
loglog(dx,err,'o',dx,eref,'r--');
xlabel('\Delta x');  ylabel('Measured error');
legend('Measured','Theoretical error ~ (\Delta x)^3','Location','Northwest')

end