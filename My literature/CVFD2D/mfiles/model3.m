function [x,xu,y,yv,ebc,wbc,nbc,sbc,con,src,ap,ae,aw,an,as,b] = model3(nx,ny,Lx,Ly,conratio,verbose)

% --- Check for mesh compatibility
if rem(nx,4) ~= 0
    error('Mesh cannot fit the model: value of nx must be divisible by 4');
end
if rem(ny,4) ~= 0
    error('Mesh cannot fit the model: value of ny must be divisible by 4');
end
% --- Indices used to subdivide the mesh
nx1 = nx/4;  nx2 = nx/2;   ny1 = ny/4;  ny2 = ny/2;  nn = nx*ny;

% --- Define uniform mesh in x and y directions
[x,xu] = fvUniformMesh(nx,Lx);
[y,yv] = fvUniformMesh(ny,Ly);

% --- Preallocate and define boundary data structures
%     All BC are Dirichlet type, so no more changes are needed
ebc = ones(ny,1)*[1 0 0 0 0];  wbc = ones(ny,1)*[1 20 0 0 0];
nbc = ones(nx,1)*[1 0 0 0 0];  sbc = ones(nx,1)*[1 10 0 0 0];

%  Allocate memory for gamma and source and set tentative values
k1 = 1.0;
k2 = conratio*k1;
con = k1*ones(nn,1);
src = zeros(nn,1);

%  Overwrite central region with different value of gamma and source
source = 1000;
for i=nx1+1:nx2+nx1
   for j=ny1+1:ny2+ny1
      ii = i + (j-1)*nx;
      con(ii) = k2;
      src(ii) = source;
   end
end

if verbose,  showBC(xu,yv,ebc,wbc,nbc,sbc);  end

% --- Compute finite-volume coefficients
[ae,aw,an,as] = fvdiff(x,xu,y,yv,con);

% --- Adjust coefficients to enforce boundary conditions
[ap,ae,aw,an,as,b] = fvbc(x,xu,y,yv,con,src,ebc,wbc,nbc,sbc,ae,aw,an,as);

end