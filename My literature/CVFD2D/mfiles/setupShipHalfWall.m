function [x,xu,y,yv,src,con,ebc,wbc,nbc,sbc] = setupShipHalfWall(nx,Lx,ny,Ly,ksteel,kins,Tair,Tw,hair,hw)
% setupShipHalfWall  Define mesh, source terms, boundary conditions for ship wall problem.
%                    Two dimensional conduction model.  Use symmetry on centerline of T beam
%
% Synopsis:  [xu,yv] = shipHalfWallSetup(nx,Lx,ny,Ly,ksteel,kins,Tair,Tw,hair,hw)
%
% Input:  nx = two-element vector;  nx(1) = number of control volumes in
%              x-direction grid on top surface at temperature T1.
%              nx(2) = number of CV in the second section (insulated corner)
%              of the x-direction grid
%         Lx = two-element vector;  Lx(1) = length of exposed top surface
%              at temperature T1.  Lx(2) = length of top surface covered by
%              insulation
%         ny = two-element vector;  ny(1) = number of control volumes in
%              y-direction grid on right surface at temperature T2.
%              ny(2) = number of CV in the second section (insulated corner)
%              of the y-direction grid
%         Ly = two-element vector;  Ly(1) = length of exposed right surface
%              at temperature T2.  Ly(2) = length of right surface covered by
%              insulation
%
% Output:  x,y = (vector) locations of center nodes of the mesh
%          xu,yv = (vector) locations of control volume interfaces in x and y
%                  directions, respectively.

if nargin<2,  error('nx and Lx must be supplied');  end
if nargin<3,  ny = nx;  end
if nargin<4,  Ly = Lx;  end

% --- Define mesh
[x,xu] = fvUniMeshBlocks(nx,Lx);
[y,yv] = fvUniMeshBlocks(ny,Ly);

% --- Define heat source term and thermal conductivity field
nn = sum(nx)*sum(ny);
src = zeros(nn,1);     %  no internal heat sources
con = shipHalfWallConductivity(nx,ny,ksteel,kins);

% --- Define boundary conditions
[ebc,wbc,nbc,sbc] = setBC(nx,ny,Tair,Tw,hair,hw);

end

% ==========================================================
function con = shipHalfWallConductivity(nx,ny,ksteel,kins)
% shipWallHalfConductivity  Define thermal conductivity when symmetry through T beam
%                           is exploited

nn = sum(nx)*sum(ny);
con = kins*ones(nn,1);   %  initialize with conducitivity of insulation

% --- Overwrite with conductivity of steel on outer wall
jwall = 1 + sum(ny(1:3));
numx = sum(nx);
vones = ones(numx,1);
for j=jwall:sum(ny)
  np = (1:numx) + (j-1)*numx;   %  both pairs of parenthesis are req'd
  con(np) = ksteel*vones;
end

% --- Overwrite with conductivity of steel on vertical segment of steel support
ibeg = 1 + sum(nx(1:2));
jbeg = 1 + sum(ny(1:2));
for i=ibeg:ibeg+nx(3)-1
  for j=jbeg:jbeg+ny(3)-1
    np = i + (j-1)*numx;
    con(np) = ksteel;
  end
end

% --- Overwrite with conductivity of steel on horizontal segment of steel support
ibeg = 1 + nx(1);
jbeg = 1 + ny(1);
for i=ibeg:ibeg+sum(nx(2:3))-1
  for j=jbeg:jbeg+ny(2)-1
    np = i + (j-1)*numx;
    con(np) = ksteel;
  end
end

end

% ==========================================================
function [ebc,wbc,nbc,sbc] = setBC(nnx,nny,Tair,Tw,hair,hw)
% setBC  Data structures for boundary conditions

% --- Initialize
nx = sum(nnx);  ny = sum(nny);
ebc = zeros(ny,5);  wbc = ebc;
nbc = zeros(nx,5);  sbc = nbc;

% --- East and West boundaries are treated as symmetry
wbc(:,1) = 4*ones(ny,1);           %  type = 4 for symmetry
ebc(:,1) = 4*ones(ny,1);

% --- North and South boundaries are convection type
vones = ones(nx,1);        %  avoid recreating this vector
sbc(:,1) = 3*vones;        %  type = 3 for convection
sbc(:,4) = hair*vones;     %  uniform convection coefficient
sbc(:,5) = Tair*vones;     %  uniform ambient temperature

nbc(:,1) = 3*vones;        %  type = 3 for convection
nbc(:,4) = hw*vones;       %  uniform convection coefficient
nbc(:,5) = Tw*vones;       %  uniform ambient temperature

end