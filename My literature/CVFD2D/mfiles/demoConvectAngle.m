function demoConvectAngle(scheme,angle,gcon,nnx,nny,Lx,Ly,vmag)
% demoConvectAngle  2D convection problem demonstrating false diffusion
%
% Synopsis:  demoConvectAngle
%            demoConvectAngle(scheme)
%            demoConvectAngle(scheme,angle)
%            demoConvectAngle(scheme,angle,gcon)
%            demoConvectAngle(scheme,angle,gcon,nnx)
%            demoConvectAngle(scheme,angle,gcon,nnx,nny)
%            demoConvectAngle(scheme,angle,gcon,nnx,nny,Lx)
%            demoConvectAngle(scheme,angle,gcon,nnx,nny,Lx,Ly)
%            demoConvectAngle(scheme,angle,gcon,nnx,nny,Lx,Ly,vmag)
%
% Input: scheme = (optional) string either 'UDS' for upwind difference scheme
%                 or 'CDS' for central difference scheme.  Default: scheme = 'UDS'
%        angle = (optional) angle in degrees that velocity vector makes with the mesh
%                Default:  angle = 45
%        gcon = (optional) value of uniform gamma in the flow field.  Default: gcon = 0.01
%               Try gcon = 0.1, 0.01, 0.001, 0.0001 to see effects of false diffusion
%               for UDS and wiggles for CDS
%        nnx = (optional) 2-element vector specifying the number of control volumes
%              in each of two segments in the x direction.  Default:  nnx = [2 8]
%        nny = (optional) 2-element vector analogous to nnx that specifies the number
%              of control volumes in the y direction.  Default:  nny = nnx;
%        Lx = (optional) 2-element vector specifying lengths of domain segments in
%              the x direction.  Default:  Lx = [0.25 0.75]
%        Ly = (optional) 2-element vector specifying lengths of domain segments in
%              the y direction.  Default:  Ly = Ly (square domain)
%        vmag = (optional) magnitude of velocity.  Default: vmag = 1

% --- set defaults
if nargin<1;  scheme = 'UDS';     end
if nargin<2;  angle = 45;         end
if nargin<3,  gcon = 0.01;        end
if nargin<4,  nnx = [2 8];        end
if nargin<5,  nny = nnx;          end
if nargin<6,  Lx  = [0.25 0.75];  end
if nargin<7;  Ly  = Lx;           end
if nargin<8,  vmag = 1;           end
verbose = 1;
rho = 1;

% --- Define the mesh, properties, source terms, and boundary conditions
[x,xu,y,yv,src,gam,ebc,wbc,nbc,sbc,u,v] = setupConvectAngle(angle,gcon,vmag,nnx,Lx,nny,Ly);
if verbose,  showBC(xu,yv,ebc,wbc,nbc,sbc);  end

% --- Compute finite-volume coefficients
switch upper(scheme)
  case 'UDS'
    [ae,aw,an,as] = fvUDS(x,xu,y,yv,rho,gam,u,v);
  case 'CDS'
    [ae,aw,an,as] = fvCDS(x,xu,y,yv,rho,gam,u,v);
  otherwise
    error('scheme = %s is not supported',scheme);
end

% --- Adjust coefficients to enforce boundary conditions
[ap,ae,aw,an,as,b] = fvbcflow(x,xu,y,yv,rho,gam,src,ebc,wbc,nbc,sbc,ae,aw,an,as,scheme);

% --- Set up coefficient matrix
nx = sum(nnx);
ny = sum(nny);
if nx<9
  printFivePtCoeff(nx,ny,ae,aw,an,as,ap,b);
end
A = fvAmatrix(nx,ap,ae,aw,an,as);
figure('name',sprintf('Sparse matrix structure for %s schemd on %d x %d mesh',scheme,nx,ny));
spy(A);

% --- Solve for temperature field at internal nodes
t = A\b;

save(sprintf('%s_%dx%d.mat',scheme,nx,ny),'b','t','A')

% --- Compute energy balance and create T matrix for plotting
[T,~] = fvpostFlow(x,xu,y,yv,rho,gam,src,ebc,wbc,nbc,sbc,t);

% --- heuristic fix for corner points
T(1,1) = 1;      %  upper left corner
T(end,1) = 0;    %  lower left corner
% T(1,end) = 1;
T(end,end) = 0;
T(T>5) = 2;
T(T<-5) = -2;

% --- Make surface, and contour plots
xp = [0; x];  yp = flipud([0; y]);  [xx,yy] = meshgrid(xp,yp);
f = figure;  set(f,'Name',sprintf('%s: %dX%x  T(x,y)',scheme,nx,ny));  meshc(xx,yy,T);
% figure;  contour(xx,yy,T); colorbar('vert');  axis('equal');
% figure;  pcolor(xx,yy,T);  colorbar('vert');  shading('interp');   axis('equal');

jmid = round(ny/2);
f = figure;  set(f,'Name',sprintf('%s: %dX%x  TCL(x)',scheme,nx,ny));
plot(xp,T(jmid,:),'o-');  xlabel('x');  ylabel('T(x)');

end

% ===============================
function [x,xu,y,yv,src,gam,ebc,wbc,nbc,sbc,u,v] = setupConvectAngle(angle,gcon,vmag,nnx,Lx,nny,Ly)
% setupConvectAngle  Create mesh, BC data structures, and flow field for convection test
%                    problem involving prescribed flow at angle to the mesh.

if angle<0 || angle>90
  error('Routine for BC setup cannot handle angle = %g;  Use 0 <= angle <= 90',angle);
end

% --- Mesh consists of two blocks in each direction.  fvUniMeshBlocks does all the work
[x,xu] = fvUniMeshBlocks(nnx,Lx);
[y,yv] = fvUniMeshBlocks(nny,Ly);

% --- Velocity field is easy
nx = length(x) - 1;   ny = length(y) - 1;   nn = nx*ny;
u = vmag*cos(angle*pi/180)*ones(nn,1);
v = vmag*sin(angle*pi/180)*ones(nn,1);

% --- Uniform conductivity and zero source term for this problem
src = zeros(nn,1);
gam = gcon*ones(nn,1);

% --- Preallocate data structures by initializing to zeros
ebc = zeros(ny,6);  wbc = ebc;
nbc = zeros(nx,6);  sbc = nbc;

% --- Values of phi at inflow boundaries
phi1 = 0;
phi2 = 1;

% --- Inflow BC on west, with step change in phi.
wbc(:,1) = 6*ones(ny,1);                      %  inflow BC, type = 6
wbc(:,6) = u(1:nx:nn-nx+1);                   %  normal velocity component, consistent with u
wbc(1:nny(1),2) = phi1*ones(nny(1),1);        %  phi = phi1 below the step change
wbc((nny(1)+1):end,2) = phi2*ones(nny(2),1);  %  phi = phi2 above the step change

% --- Outflow BC on east
ebc(:,1) = 7*ones(ny,1);      %  set BC type = 7
ebc(:,6) = wbc(:,6);          %  normal velocity component

% --- Inflow BC on south boundary
sbc(:,1) = 6*ones(nx,1);      %  Inflow BC, type = 6
sbc(:,2) = phi1*ones(nx,1);   %  phi = phi1 all along the south
sbc(:,6) = v(1:nx);           %  normal velocity component, consistent with v

% --- Outflow BC on north, type = 7
nbc(:,1) = 7*ones(nx,1);      %  set BC type = 7
nbc(:,6) = sbc(:,6);          %  normal velocity component

end
