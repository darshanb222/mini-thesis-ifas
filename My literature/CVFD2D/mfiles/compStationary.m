function compStationary(nx,pnum)
% compStationary  Compare performance of stationary relaxation methods for
%                 for solving the a system of equaitons arising from applying
%                 the CVFD method to a 2D heat conduction problem
%
% Synopsis:  compStationary
%            compStationary(nx)
%            compStationary(nx,pnum)
%
% Input: nx = number of control volumes in the x direction.  Default:  nx = 16
%             Note: ny = nx for all problems
%        pnum = integer value 1 through 4 used to indicate which boundary has
%               uniform heat flux. pnum=1 for heat flux on north boundary,
%               pnum=2 for heat flux on east boundary, pnum=3 for heat flux
%               on south boundary.  pnum=4 for heat flux on west boundary.
%
% Output: res = (optional) vector of residuals for the iterative method

% --- Set defaults
if nargin<1,  nx = 16;          end
if nargin<2,  pnum = 1;         end
% if nargin<2,  conratio = 4;     end

rGS = demoGaussSeidel(pnum,nx,[],[],[],false);
rJac = demoJacobi(pnum,nx,[],[],[],false);
rSOR = demoSOR(pnum,nx,[],[],[],false);

figure('Name',sprintf('Compare relaxation solvers for pnum = %d',pnum))
semilogy((1:length(rGS))',rGS,'--',...
       (1:length(rJac))',rJac,'.',...
       (1:length(rSOR))',rSOR,'+');
xlabel('Iteration');  ylabel('||r(i)|| / ||r(1)||');
legend('Gauss Seidel','Jacobi','SOR');

end