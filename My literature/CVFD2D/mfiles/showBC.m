function showBC(xu,yv,ebc,wbc,nbc,sbc,con)
% showBC  Diagnostic function to print values in BC data structures

verbose = false;
if verbose,  printBC(ebc,wbc,nbc,sbc);  end

figure;
hold('on')
% --- Use grayscale fill patterns to indicate thermal conductivity
if nargin>6
  nx = length(xu)-1;
  ny = length(yv)-1;
  cscale = 1 - (con-min(con))/(max(con)-min(con));    %  convert con values to 0 to 1 scale
  cscale = repmat( cscale, 1,3);
  colormap('gray');
  for i=1:nx
    for j=1:ny
      np = i + (j-1)*nx;
      fill([xu(i) xu(i+1) xu(i+1) xu(i) xu(i)],[yv(j) yv(j) yv(j+1) yv(j+1) yv(j)],cscale(np))
    end
  end
end

% --- Plot grid and color patches to indicate boundary condition type
showGrid(xu,yv)
bx = 0.03*max(xu);          %  x-direction width of box used for boundary condition
hold('on')

% --- East boundary
C = colorbc(ebc);
xbox = xu(end) + [0 bx bx 0 0];
for j=1:length(yv)-1
  fill(xbox,[yv(j) yv(j) yv(j+1) yv(j+1) yv(j)],C(j,:))
end

% --- West boundary
C = colorbc(wbc);
xbox = xu(1) - [bx 0 0 bx bx];
for j=1:length(yv)-1
  fill(xbox,[yv(j) yv(j) yv(j+1) yv(j+1) yv(j)],C(j,:))
end

% --- North boundary
C = colorbc(nbc);
ybox = yv(end) + [0 bx bx 0 0];
for i=1:length(xu)-1
  fill([xu(i) xu(i) xu(i+1) xu(i+1) xu(i)],ybox,C(i,:))
end

% --- South boundary
C = colorbc(sbc);
ybox = yv(1) - [bx 0 0 bx bx];
for i=1:length(xu)-1
  fill([xu(i) xu(i) xu(i+1) xu(i+1) xu(i)],ybox,C(i,:))
end

hold('off')
end

% ==================================
function C = colorbc(bcdata)
% colorbc  Obtain vector of color types used to indicate boundary conditions
%          Use separate color to indicate insulated boundary
%
% Input:  bcdata = matrix of boundary condition data.  bcdata is one of
%                  ebc, wbc, nbc, sbc
%
% Output:  C = matrix of rgb values for boundary data

ctype = [0 1 0;         %  pure green for constant temperature, type 1
         1 0 0;         %  pure red for constant q,             type 2
         0 0 1;         %  pure blue for convective bc,         type 3
         1 1 0;         %  yellow for symmetry bc,              type 4
         0.9 0.9 0.9;   %  gray for insulated type,             type 5
         0.5 1 1;       %  cyan for inflow BC,                  type 6
         1 0.5 0];      %  orange for outflow type,             type 7

% --- use array addressing to intialize C
C = ctype(bcdata(:,1),:);   %  array addressing

% --- overwrite color values of insulated nodes
iq = find(bcdata(:,1)==2);       %  index of nodes with constant q BC type
if ~isempty(iq)
  ins = iq;
  ins((bcdata(iq,2)~=0)) = [];   %  delete constant q nodes with q ~= 0
  if ~isempty(ins)
    for i = ins(:)'
      C(i,:) = ctype(5,:);
    end
    % C(ins,:) = reshape(ctype(5,:),length(ins),1);  %  reshape replicates
  end
end
end

% ===============================================
function printBC(ebc,wbc,nbc,sbc)
% printBC  Diagnostic function to print values in BC data structures
fprintf('\nEast Boundary Data:\n\n type   T       q       h      Tinf\n');
for i=1:size(ebc,1)   %  loop over rows
  fprintf('%3d  %6.2f  %6.2f  %6.2f  %6.2f\n',ebc(i,:))
end

fprintf('\nWest Boundary Data:\n\n type   T       q       h      Tinf\n');
for i=1:size(wbc,1)   %  loop over rows
  fprintf('%3d  %6.2f  %6.2f  %6.2f  %6.2f\n',wbc(i,:))
end

fprintf('\nNorth Boundary Data:\n\n type   T       q       h      Tinf\n');
for i=1:size(nbc,1)   %  loop over rows
  fprintf('%3d  %6.2f  %6.2f  %6.2f  %6.2f\n',nbc(i,:))
end

fprintf('\nSouth Boundary Data:\n\n type   T       q       h      Tinf\n');
for i=1:size(sbc,1)   %  loop over rows
  fprintf('%3d  %6.2f  %6.2f  %6.2f  %6.2f\n',sbc(i,:))
end
end