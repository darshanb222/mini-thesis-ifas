function plotProfiles(ny)
% plotProfiles  Plot vertical temperature profiles for duct heating problem

if nargin<1,  ny=16;  end
fnu = sprintf('Ty_uniform_ny%d',ny);
fnfd = sprintf('Ty_fullydev_ny%d',ny);

% --- Load data, copy to local variables and plot
load(fnfd)
Tefd = Te;  yefd = ye;  uefd = [0; ue; 0];
load(fnu)
ue = [0; ue; 0];
plot(Te,ye,'-',Tefd,yefd,'o--')
legend('Plug flow','Fully Developed');
title(sprintf('Re = %d',round(Re)));

figure
plot(ue,ye,'-',uefd,yefd,'o--')
legend('Plug flow','Fully Developed');
title(sprintf('Re = %d',round(Re)));

end