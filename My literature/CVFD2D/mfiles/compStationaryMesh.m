function compStationaryMesh(nnx,pnum)
% compStationaryMesh  Compare performance of stationary relaxation methods for
%                 for solving the a system of equaitons arising from applying
%                 the CVFD method to a 2D heat conduction problem
%
% Synopsis:  compStationary
%            compStationary(nx)
%            compStationary(nx,pnum)
%
% Input: nnx = vector sequence of number of control volumes in the x direction.
%              Default:  nnx = [4 8 16 32 64]
%              Note: ny = nx for all problems
%        pnum = integer value 1 through 4 used to indicate which boundary has
%               uniform heat flux. pnum=1 for heat flux on north boundary,
%               pnum=2 for heat flux on east boundary, pnum=3 for heat flux
%               on south boundary.  pnum=4 for heat flux on west boundary.
%
% Output: res = (optional) vector of residuals for the iterative method

% --- Set defaults
if nargin<1,  nnx = [4 8 16 32 64];  end
if nargin<2,  pnum = 1;              end

% --- Define string matrix of line styles that can be reused.
%     nsymb is the total number of line style strings (plot symbols)
%     In plot loop the statement isymb = 1 + rem(j-1,nsymb) is an
%     index in the range 1 <= isymb <= nsymb
lineSymb = {'k-';'b:';'r--';'m-.'};
nsymb = size(lineSymb,1);
legstr = cell(1,length(nnx));

% -- Gauss Seidel
j = 0;  %  j counter is needed to increment through the line styles
figure('Name','Gauss Seidel');
for nx = nnx
  r = demoGaussSeidel(pnum,nx,[],[],[],false);
  j = j+1;
  isymb = 1 + rem(j-1,nsymb);        %  cyclic index for line styles.
  semilogy(1:length(r),r,lineSymb{isymb,:})
  legstr{j} = sprintf('nx = %-d',nx);  %  Build legend string
  hold('on');
end
xlabel('Iteration');  ylabel('||r(i)|| / ||r(1)||');
legend(legstr,'Location','northeast');
hold('off')

% -- Jacobi
j = 0;  %  j counter is needed to increment through the line styles
figure('Name','Jacobi');
for nx = nnx
  r = demoJacobi(pnum,nx,[],[],[],false);
  j = j+1;
  isymb = 1 + rem(j-1,nsymb);        %  cyclic index for line styles.
  semilogy(1:length(r),r,lineSymb{isymb,:})
  legstr{j} = sprintf('nx = %-d',nx);  %  Build legend string
  hold('on');
end
xlabel('Iteration');  ylabel('||r(i)|| / ||r(1)||');
legend(legstr,'Location','northeast');
hold('off')

% -- SOR
j = 0;  %  j counter is needed to increment through the line styles
figure('Name','SOR');
for nx = nnx
  r = demoSOR(pnum,nx,[],[],[],false);
  j = j+1;
  isymb = 1 + rem(j-1,nsymb);        %  cyclic index for line styles.
  semilogy(1:length(r),r,lineSymb{isymb,:})
  legstr{j} = sprintf('nx = %-d',nx);  %  Build legend string
  hold('on');
end
xlabel('Iteration');  ylabel('||r(i)|| / ||r(1)||');
legend(legstr,'Location','northeast');
hold('off')

end