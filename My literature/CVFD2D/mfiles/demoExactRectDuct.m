function demoExactRectDuct
% demoExactRectDuct  Plot umax/uave and fRe versus a/b for fully-developed
%                    laminar flow in a rectangular duct.

% --- Define a vector of aspect ratios at which to evaluate umax/uave and fRe
% ar = [1 1.25 1.5 1.75 2 3 4 5 6 7 8 10 16 32 64 128 256 512];
ar = logspace(0,3);

% -- pre-allocate
uu = zeros(size(ar));  fRe = uu;

fprintf('\n    a/b      umax/ubar    fRe\n');
for i=1:length(ar)
  [u,f] = exactRectDuct(ar(i),5e-6,5000);
  uu(i) = u;  fRe(i) = f;
  fprintf(' %8.2f   %8.4f   %8.4f\n',ar(i),uu(i),fRe(i));
end

semilogx(ar,uu,'-');   xlabel('a/b');   ylabel('u_{max}/u_{ave}');
axis([1 1000 1 2.5]);   grid('on');

figure
semilogx(ar,fRe,'-');   xlabel('a/b');   ylabel('fRe');
axis([1 1000 50 100]);   grid('on');
end