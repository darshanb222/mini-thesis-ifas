function [x,xu,y,yv,src,con,ebc,wbc,nbc,sbc] = model2Da(pnum,nx,Lx,ny,Ly)
% model2Da  Create mesh and BC data structures for 2D test problem
%           Constant q on pnum boundary.  Constant T elsewhere

% --- Define uniform mesh in x and y directions
[x,xu] = fvUniformMesh(nx,Lx);
[y,yv] = fvUniformMesh(ny,Ly);

% --- Uniform conductivity and zero source term
nx = length(x) - 1;   ny = length(y) - 1;   nn = nx*ny;
src = zeros(nn,1);
con = ones(nn,1);

% --- Preallocate data structures by initializing to zeros
ebc = zeros(ny,5);  wbc = ebc;
nbc = zeros(nx,5);  sbc = nbc;

% --- Constant heat flux on one boundary, constant T on others
qb = sqrt(5);  %  specified heat flux BC
Tb = pi;       %  specified temperature BC
switch pnum
  case 1
    % --- Heat flux = qb on north BC.  Constant T = Tb elsewhere
    nbc(:,1) = 2*ones(nx,1);  nbc(:,3) = qb*ones(nx,1);
    sbc(:,1) = ones(nx,1);    sbc(:,2) = Tb*sbc(:,1);
    ebc(:,1) = ones(ny,1);    ebc(:,2) = Tb*ebc(:,1);
    wbc(:,1) = ebc(:,1);      wbc(:,2) = Tb*wbc(:,1);
  case 2
    % --- Heat flux = qb on east BC.  Constant T = Tb elsewhere
    ebc(:,1) = 2*ones(ny,1);  ebc(:,3) = qb*ones(ny,1);
    nbc(:,1) = ones(nx,1);    nbc(:,2) = Tb*nbc(:,1);
    sbc(:,1) = nbc(:,1);      sbc(:,2) = Tb*sbc(:,1);
    wbc(:,1) = ones(ny,1);    wbc(:,2) = Tb*wbc(:,1);
  case 3
    % --- Heat flux = qb on south BC.  Constant T = Tb elsewhere
    sbc(:,1) = 2*ones(nx,1);  sbc(:,3) = qb*ones(nx,1);
    nbc(:,1) = ones(nx,1);    nbc(:,2) = Tb*nbc(:,1);
    ebc(:,1) = ones(ny,1);    ebc(:,2) = Tb*ebc(:,1);
    wbc(:,1) = ebc(:,1);      wbc(:,2) = Tb*wbc(:,1);
  case 4
    % --- Heat flux = qb on west BC.  Constant T = Tb elsewhere
    wbc(:,1) = 2*ones(ny,1);  wbc(:,3) = qb*ones(ny,1);
    nbc(:,1) = ones(nx,1);    nbc(:,2) = Tb*nbc(:,1);
    sbc(:,1) = nbc(:,1);      sbc(:,2) = Tb*sbc(:,1);
    ebc(:,1) = ones(ny,1);    ebc(:,2) = Tb*ebc(:,1);
  otherwise
    error('pnum = %d not allowed\n',pnum);
end

end