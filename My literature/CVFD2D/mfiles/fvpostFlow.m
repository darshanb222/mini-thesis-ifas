function [T,ebout,qe,qw,qn,qs,qgen] = fvpostFlow(x,xu,y,yv,rho,gam,src,ebc,wbc,nbc,sbc,t,verbose)
% fvpostFlow  Post-process solution to extract boundary fluxes, energy balance
%
% Synopsis:  [T,ebal] = fvpostFlow(x,xu,y,yv,gam,src,wbc,nbc,sbc,t)
%            [T,ebal,qe,qw,qn,qs,qgen] = fvpostFlow(x,xu,y,yv,gam,src,wbc,nbc,sbc,t)
%
% Input:  x,xu,y,yv = vectors defining the grid.  See mesh routines
%         gam = vector of thermal conductivities for internal control volumes
%         src = vector of heat source terms for internal control volumes
%         ebc,wbc,nbc,sbc = data structures used to enforce boundary conditions
%         t = solution to the temperature field at the internal nodes
%         rhocp = product of rho and cp:  needed for thermal energy balance
%                 at flow boundaries
%
% Output:  T = matrix representation of the temperature field.  T(i,j) is the
%              temperature at x(i) and y(j), and includes the boundary conditions
%          ebal = energy balance.  If ebal is not zero, or very, very small,
%                 there is an error in the code.
%          qe = (optional) total heat transfer rate per unit depth (into page)
%               across the east boundary.
%          qw = (optional) total heat transfer rate per unit depth (into page)
%               across the west boundary.
%          qn = (optional) total heat transfer rate per unit depth (into page)
%               across the north boundary.
%          qs = (optional) total heat transfer rate per unit depth (into page)
%               across the south boundary.
%
% Sign Convention:   All heat flows into the domain are positive.  Thus, the
%                    net inflow of heat is qe + qw + qn + qs

if nargin<13,  verbose = 1;  end 

% --- Allocate memory and initilize coefficients
nx = length(x) - 1;  ny = length(y) - 1;
% qeast  = zeros(ny,1);   qwest  = qeast;   qetot = 0;  qwtot = 0;
% qnorth = zeros(nx,1);   qsouth = qnorth;  qntot = 0;  qstot = 0;
qsrc = 0;  dx = diff(xu);  dy = diff(yv);

% --- Contribution of heat source at interior control volumes
if any(src)
	for i=1:nx
      for j = 1:ny
        np = i + (j-1)*nx;
        qsrc = qsrc + dx(i)*dy(j)*src(np);
      end
	end
end

% --- Heat flux on east boundary
%     Heat flux is positive in the positive x-direction except for constant
%     q BC which user thinks is positive when into the domain
qetot = 0;
dxi = xu(nx+1) - x(nx);   %  distance between first internal node and boundary
ue = ebc(:,6);            %  normal component of velocity
for j=1:ny
  np = nx + (j-1)*nx;               %  i=nx
  switch ebc(j,1)
    case 1  %  constant T
      ebc(j,3) = gam(np)*(ebc(j,2)-t(np))/dxi;   %  local heat flux into domain
      qetot = qetot - ebc(j,3)*dy(j);            %  heat flow is out, so negative
    case 2  %  constant q
      ebc(j,2) = t(np) + ebc(j,3)*dxi/gam(np);   %  decorate temperature
      qetot = qetot - ebc(j,3)*dy(j);            %  user specifies positive ebc(j,3) for heating
    case 3  %  Convection
      kond = gam(np)/dxi;
      ebc(j,2) = (ebc(j,4)*ebc(j,5)+kond*t(np))/(ebc(j,4)+kond); % decorate temperature
      ebc(j,3) = ebc(j,4)*(ebc(j,2)-ebc(j,5));                   % compute q = h*(Tb - Tinf)
      qetot = qetot - ebc(j,3)*dy(j);            %  heat flow is out, so positive
    case 4  %  Symmetry
      ebc(j,2) = t(np);   %  near boundary and boundary values are equal, q = 0
    case 6  %  Inflow
      ebc(j,3) = rho*ue(j)*ebc(j,2) + gam(np)*(ebc(j,2)-t(np))/dxi;   %  convection + diffusion
      qetot = qetot + ebc(j,3)*dy(j);            %  positive inflow is heat addition
    case 7  %  Outflow
      ebc(j,2) = t(np);                 %  no diffusion at outflow
      ebc(j,3) = rho*ue(j)*ebc(j,2);    %  convective flux only
      qetot = qetot - ebc(j,3)*dy(j);   %  heat flow is out, so negative
    otherwise
      error('BC type = %d not defined on east boundary',ebc(j,1));
  end
end

% --- Heat flux on west boundary.  Analogous to east boundary
%     Heat flux is positive in the positive x-direction
dxi = x(1) - xu(1);   %  distance between first internal node and boundary
uw = wbc(:,6);        %  normal component of velocity
for j=1:ny
  np = 1 + (j-1)*nx;                %  i=1
  switch wbc(j,1)
    case 1  %  constant T
      wbc(j,3) = gam(np)*(wbc(j,2)-t(np))/dxi;   %  local heat flux into domain
    case 2  %  constant q
      wbc(j,2) = t(np) - wbc(j,3)*dxi/gam(np);   %  decorate temperature
    case 3  %  Convection
      % -- Tb = (h*Tinf + (k/delta)*Ti)/(h + (k/delta))
      kond = gam(np)/dxi;
      wbc(j,2) = (wbc(j,4)*wbc(j,5)+kond*t(np))/(wbc(j,4)+kond); % decorate temperature
      wbc(j,3) = wbc(j,4)*(wbc(j,5)-wbc(j,2));                   % compute q = h*(Tb - Tinf)
    case 4  %  Symmetry
      wbc(j,2) = t(np);   %  near boundary and boundary values are equal, q = 0
    case 6  %  Inflow
      wbc(j,3) = uw(j)*wbc(j,2) + gam(np)*(wbc(j,2)-t(np))/dxi;   %  convection + diffusion
    case 7  %  Outflow
      wbc(j,2) = t(np);            %  no diffusion at outflow
      wbc(j,3) = rho*uw(j)*wbc(j,2);   %  convective flux only
    otherwise
      error('BC type = %d not defined on west boundary',wbc(j,1));
  end
end

% --- Heat flux on north boundary
%     Heat flux is positive in the positive y-direction except for constant
%     q BC which user thinks is positive when into the domain
qntot = 0;
dyi = yv(ny+1) - y(ny);   %  distance between first internal node and boundary
vn = nbc(:,6);            %  normal component of velocity
for i=1:nx
  np = i + (ny-1)*nx;               %  j=ny
  switch nbc(i,1)
    case 1  %  constant T
      nbc(i,3) = gam(np)*(nbc(i,2)-t(np))/dyi;   %  local heat flux in positive x-dir
      qntot = qntot - nbc(i,3)*dx(i);            %  heat flow is out, so negative
    case 2  %  constant q
      nbc(i,2) = t(np) + nbc(i,3)*dyi/gam(np);   %  decorate temperature
      qntot = qntot + nbc(i,3)*dx(i);            %  user specifies positive nbc(i,3) for heating
    case 3  %  Convection
      % -- Tb = (h*Tinf + (k/delta)*Ti)/(h + (k/delta))
      kond = gam(np)/dyi;
      nbc(i,2) = (nbc(i,4)*nbc(i,5)+kond*t(np))/(nbc(i,4)+kond); % decorate temperature
      nbc(i,3) = nbc(i,4)*(nbc(i,2)-nbc(i,5));                   % compute q = h*(Tb-Tinf)
      qntot = qntot - nbc(i,3)*dx(i);            %  heat flow is out, so negative
    case 4  %  Symmetry
      nbc(i,2) = t(np);   %  near boundary and boundary values are equal, q = 0
    case 6  %  Inflow
      nbc(i,3) = rho*vn(i)*nbc(i,2) + gam(np)*(nbc(i,2)-t(np))/dyi;   %  convection + diffusion
      qntot = qntot + nbc(i,3)*dx(i);            %  positive inflow is heat addition
    case 7  %  Outflow
      nbc(i,2) = t(np);                 %  no diffusion at outflow
      nbc(i,3) = rho*vn(i)*nbc(i,2);        %  convective flux only
      qntot = qntot - nbc(i,3)*dx(i);   %  heat flow is out, so negative
    otherwise
      error('BC type = %d not defined on north boundary',nbc(i,1));
  end
end

%  --- Heat flux on south boundary.  Analogous to north boundary
dyi = y(1) - yv(1);   %  distance between first internal node and boundary
vs = sbc(:,6);        %  normal component of velocity
for i=1:nx
  np = i;                           %  j=1
  switch sbc(i,1)
    case 1  %  constant T
      sbc(i,3) = gam(np)*(sbc(i,2)-t(np))/dyi;   %  local heat flux into domain
    case 2  %  constant q
      sbc(i,2) = t(np) - sbc(i,3)*dyi/gam(np);   %  decorate temperature
    case 3  %  Convection
      % -- Tb = (h*Tinf + (k/delta)*Ti)/(h + (k/delta))
      kond = gam(np)/dyi;
      sbc(i,2) = (sbc(i,4)*sbc(i,5)+kond*t(np))/(sbc(i,4)+kond); % decorate temperature
      sbc(i,3) = sbc(i,4)*(sbc(i,5)-sbc(i,2));                   % compute q = h*(Tinf - Tb)
    case 4  %  Symmetry
      sbc(i,2) = t(np);   %  near boundary and boundary values are equal, q = 0
    case 6  %  Inflow
      sbc(i,3) = rho*vs(i)*sbc(i,2) + gam(np)*(sbc(i,2)-t(np))/dyi;   %  convection + diffusion
    case 7  %  Outflow
      sbc(i,2) = t(np);            %  no diffusion at outflow
      sbc(i,3) = rho*vs(i)*sbc(i,2);   %  convective flux only
    otherwise
      error('BC type = %d not defined on south boundary',sbc(i,1))
  end
end

% --- Heat fluxes are positive in positive coordinate direction
%     East and west boundaries are treated differently because constant q
%     boundaries are counterintuitive:  positive q is in when user
%     specifies BC, but really that heat flux is in negative x or y
%     direction
% qetot = sum(ebc(:,3).*dy);   %  qtot = sum( q(i).*dA(i) )
qwtot = sum(wbc(:,3).*dy);
% qntot = sum(nbc(:,3).*dx);
qstot = sum(sbc(:,3).*dx);

% --- Energy balance = heat generated - net outflow?
ebal = qsrc + ( qetot + qwtot + qntot + qstot );

if verbose
	fprintf('\nEnergy balance:\n');
	fprintf('\t qsrc    = %12.3e\n',qsrc);
	fprintf('\t Qeast   = %12.3e\n',qetot);
	fprintf('\t Qwest   = %12.3e\n',qwtot);
	fprintf('\t Qnorth  = %12.3e\n',qntot);
	fprintf('\t Qsouth  = %12.3e\n',qstot);
	fprintf('\t --------------------------\n');
	fprintf('\t Balance = %12.3e\n',ebal);
end

% --- Copy t field into T matrix
T= make2DijField(nx,ny,x,xu,y,yv,t,ebc(:,2),wbc(:,2),nbc(:,2),sbc(:,2));

if nargin>1,  ebout = ebal;  end
if nargout>2
  qgen = qsrc;
  qe = qetot;   qw = qwtot;    qn = qntot;  qs = qstot;
end

end