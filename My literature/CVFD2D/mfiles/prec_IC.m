function y = prec_IC(x)
% ICsolve  Solve the system (L*L')x = y, where L is the Incomplete
%          Cholesky  decomposition of a matrix. 
%

global L
z = L  \ x;
y = L' \ z;

end