function [x,xu,y,yv,ebc,wbc,nbc,sbc,ap,ae,aw,an,as,b,exact] = model1(nx,ny,Lx,Ly,verbose)

% --- Define uniform mesh in x and y directions
[x,xu] = fvUniformMesh(nx,Lx);  [y,yv] = fvUniformMesh(ny,Ly);  nn = nx*ny;

% --- Preallocate and define boundary data structures
%     All BC are Dirichlet type, so no more changes are needed
ebc = ones(ny,1)*[1 0 0 0 0];  wbc = ebc;
nbc = ones(nx,1)*[1 0 0 0 0];  sbc = nbc;

% --- Source term and exact solution
cx = pi/Lx;              cy = 2*pi/Ly;
tmpx = sin(cx*x(1:nx));  tmpy = sin(cy*y(1:ny));
f = tmpx*tmpy';             %  outer product
src = (cx^2 + cy^2)*f(:);   %  f(:) converts matrix to vector, by columns
exact = f(:);
if verbose,  showBC(xu,yv,ebc,wbc,nbc,sbc);  end

% --- Compute finite-volume coefficients
con = ones(nn,1);     %  uniform conductivity
[ae,aw,an,as] = fvdiff(x,xu,y,yv,con);

% --- Adjust coefficients to enforce boundary conditions
[ap,ae,aw,an,as,b] = fvbc(x,xu,y,yv,con,src,ebc,wbc,nbc,sbc,ae,aw,an,as);

end