function varargout = exactRectDuct(ab,tol,n,verbose)
% exactRectDuct   Evaluate analytical solution to fully-developed laminar flow in a rectangular duct
%
% Synopsis:  exactRectDuct 
%            exactRectDuct(ab)
%            exactRectDuct(ab,tol)
%            exactRectDuct(ab,tol,n)
%            exactRectDuct(ab,tol,n,verbose)
%            uu = exactRectDuct(...)
%            [uu,fRe] = exactRectDuct(...)
%            [uu,fRe,it] = exactRectDuct(...)
%
% Input:  ab = aspect ratio of the duct.  Default:  ab = 2
%         tol = (optional) tolerance on accumulated sum. Default:  tol = 5e-9
%                 Series is terminated when abs(T_k/S_k) < delta.   T_k is the
%                 kth term and S_k is the sum after the kth term is added.                
%         n   = (optional) maximum number of terms. Default: n = 50
%         verbose = (optional) flag to control whether each term in the sum is printed
%                   as it is added.  Default:  verbose = 0 (no printing)
%
% Output:  uu = umax/ubar, ratio of maximum velocity to average velocity
%          fRe = (optional) product of Darcy friction factor and Reynolds number
%          it = (optional) number of terms in the series needed to obtain the solution

% Reference:  Analytical solution is from F.M. White, "Viscous Flow", 1974, McGraw-Hill
%             New York, p. 123

if nargin<1,  ab = 2;      end
if nargin<2,  tol = 5e-9;  end
if nargin<3,  n = 50;      end
if nargin<4,  verbose=0;   end

% --- Define constants
ba = 1/ab;
c1 = pi/ab/2;
c2 = 192*ab/pi^5;

% --- Initialize the two series:  numsum for numerator (umax), densum for denominator (ubar)
%     ssum = series for umax/ubar
sgn = 1;
numsum = sgn*(1-1/cosh(c1));
densum = tanh(c1);
ssum = numsum/(1-c2*densum);                      %  Initialize series
if verbose
  fprintf('Fully-developed laminar flow in retangular duct with aspect ratio of %g\n\n',ab);
  fprintf('  k     numsum       densum        ssum          ds\n');
  fprintf('%3d  %11.3e  %11.3e  %12.8f\n',1,numsum,densum,ssum); 
end

converge = 0;
for k=3:2:(2*n-1)
  sumold = ssum;
  sgn = -sgn;
  numsum = numsum + sgn*(1-1/cosh(k*c1))/k^3;
  densum = densum + tanh(k*c1)/k^5;
  ssum = numsum/(1-c2*densum);
  ds = ssum - sumold;
  if verbose
    fprintf('%3d  %11.3e  %11.3e  %12.8f  %11.3e\n',k,numsum,densum,ssum,ds); 
  end
  if abs(ds/ssum)<tol, converge=1;  break;  end
end
umaxubar = (48/pi^3)*ssum;
fRe = 96/(ab+1)^2/(1-c2*densum);
if nargout==1
	varargout = {umaxubar};
elseif nargout==2
	varargout= {umaxubar,fRe};
elseif nargout==2
	varargout = {umaxubar,fRe,k};
else
  fprintf('\nAfter %d terms, umax/ubar = %8.5f   fRe = %8.5f\n',(k+1)/2,umaxubar,fRe);
  fprintf('Relative change in umax/ubar formula for last term = %11.8f\n',ds/ssum);
end
if ~converge,  warning(sprintf('No convergence after %d terms\n',(k+1)/2));  end

end