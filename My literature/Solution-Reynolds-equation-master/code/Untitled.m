%% Hydrodynamic lubrication journal bearing 
clear all;

R = 100e-03;                                                    % Radial diameter of bearing
X = 2*pi;                                                       % Circumference of bearing
Z = 160e-03;                                                    % Length of the bearing
n = 50;                                                         % No of nodes on x direction
m = 50;                                                         % No of nodes on z direction
del_x = 1/n;
del_z = 1/m;
eta_fd = 0.1586;
x_bar = 0:del_x:1;
z_bar = 0:del_z:1;
rps = 1200/60;
U_fd = R*rps;                                                   % average wedge thickness
C = 0.18e-03;
e_p = 0.7;
e = e_p*C;
fun_hbar = @(theta)(e_p*cos(theta) + 1);
theta = x_bar*2*pi;
del_theta = theta(end)-theta(end-1);
h_bar_x = fun_hbar(theta)';                                   % non dimensionalized film thickness
dh = (1+e_p*cos(theta));


fun_hhalf = @(h)(-((h(2)-h(1))/2) + h(2));
fun_hhalf2 = @(h)(((h(2)-h(1))/2) + h(1));
h_halfminus = zeros(size(x_bar,2),1);
for i=2:length(h_bar_x)-1
   h_halfminus(i) = fun_hhalf([h_bar_x(i-1) h_bar_x(i)]);
end
h_halfplus = zeros(size(x_bar,2),1);
for i=2:length(h_bar_x)-1
   h_halfplus(i) = fun_hhalf2([h_bar_x(i) h_bar_x(i+1)]);
end

h_plus = h_halfplus.^3;
h_minus = h_halfminus.^3;

h_bar = h_bar_x;
% h_bar = h_bar';
X_Z_const = R^2/Z^2;
nu = 0.305;
E_s = 180*10^9;
iter = 5000;
p_bar_fd = zeros(length(x_bar),length(z_bar));
A_fun = @(h,h_p,h_m)(h_p*del_z^2/((h_p + h_m)*del_z^2 + 2*X_Z_const*h^3*del_theta^2));
B_fun = @(h,h_p,h_m)(h_m*del_z^2/((h_p + h_m)*del_z^2 + 2*X_Z_const*h^3*del_theta^2));
C_fun = @(h,h_p,h_m)(X_Z_const*h^3*del_theta^2/((h_p + h_m)*del_z^2 + 2*X_Z_const*h^3*del_theta^2));
E_fun = @(theta,h,h_p,h_m)(e_p*sin(theta)*del_z^2*del_theta^2/((h_p + h_m)*del_z^2 + 2*X_Z_const*h^3*del_theta^2));
p_init = p_bar_fd;
error_target = 0.0001;
h = waitbar(0,'Please wait...');
for k=1:iter
    for j=2:size(p_bar_fd,2)-1
        for i=2:size(p_bar_fd,1)-1
            p_bar_fd(i,j) = A_fun(h_bar(i),h_plus(i),h_minus(i))*p_bar_fd(i+1,j) +...
                B_fun(h_bar(i),h_plus(i),h_minus(i))*p_bar_fd(i-1,j) +...
                C_fun(h_bar(i),h_plus(i),h_minus(i))*p_bar_fd(i,j+1) +...
                C_fun(h_bar(i),h_plus(i),h_minus(i))*p_bar_fd(i,j-1) +...
                E_fun(theta(i),h_bar(i),h_plus(i),h_minus(i));
            if p_bar_fd(i,j) < -0.017
                p_bar_fd(i,j) =-0.017;
            end
        end
    end
    error = (sum(p_bar_fd(:)) - sum(p_init(:)))/sum(p_bar_fd(:));
    p_init = p_bar_fd;
    if error < error_target
        break;
    end
    waitbar(k / iter)
    
end
close(h);
x_scale = x_bar;
z_scale = z_bar.*Z;
colormap(jet);
surf(z_scale,theta,p_init);
title('Pressure profile');
xlabel('Y(m)');
ylabel('\theta');
zlabel('Pressure');
