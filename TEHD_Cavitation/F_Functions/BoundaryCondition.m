function [f] = BoundaryCondition(Node,x)

global  n_VariablesNode  offsetnode node

p = x((Node.No -1) * n_VariablesNode +offsetnode.p,1);

f = node(Node.No).p - p;

end