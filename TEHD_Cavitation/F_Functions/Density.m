function f = Density(Node,x)

global n_VariablesNode offsetnode
 
p = x((Node.No -1)* n_VariablesNode +offsetnode.p,1);
rho = x((Node.No -1)* n_VariablesNode +offsetnode.rho,1);
% theta = x((Node.No -1)* n_VariablesNode +offsetnode.theta,1);

rho_0 = 874.5;
rho_gas = 10;
beta = 1/(15000*1e5);


% f = rho_0/(1-beta*p)*(1-theta) - rho;
f = rho_0/(1-beta*p) - rho;
end