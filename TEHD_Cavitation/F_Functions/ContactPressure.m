function f = ContactPressure(Node,x)

    global n_VariablesNode offset_Qc  n_node offset_Qp n_VariablesElement offsetnode node

    p_c = x((Node.No -1) * n_VariablesNode +offsetnode.p_c,1);
    h = x((Node.No -1)* n_VariablesNode +offsetnode.h,1);
    
    K = 3.e-3;            % Ref: Dissertation Lang, S.33
    E1 = 210000.0*1.e6;   % Young's modulus steel in N_m^2
    E2 = 100000.0*1.e6;   % Young's  brass in N_m^2
    E2 = 210000.0*1.e6;   % Young's modulus steel in N_m^2
    ny1 = (0.27+0.3)*0.5; % Poisson number steel (Ref: wikipedia)
    ny2 = 0.37;           % Poisson number brass (Ref: wikipedia)
    ny2 = (0.27+0.3)*0.5; % Poisson number steel (Ref: wikipedia)
    meanE = 2.0*E1*E2 / ( E1*(1-ny2*ny2)+E2*(1-ny1*ny1) );

    A = K*meanE;          % constant
    B = 4.4086*1.e-5;     % constant
    C = 4.;               % constant
    D = h;
    E = 1.0*1.e-6;        % Sigma
    F = 6.804;            % konst
    M = 1.e10;            % Gradient tanh
    N = 0.25*1.e-6;        % Cutoff

    
%     if h > N
%         f = -p_c;
%     else
        f = A*B*((C-D/E)*(-0.5*tanh(M*(D-N))+0.5))^F - p_c;
%     end
end