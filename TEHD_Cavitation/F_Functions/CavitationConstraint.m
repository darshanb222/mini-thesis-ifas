function [f] = CavitationConstraint(NodeNum,x)
global offsetnode n_VariablesNode  Trigger node dx r_1 del  z
 
p = x((NodeNum -1)* n_VariablesNode +offsetnode.p,1);
theta = x((NodeNum -1)* n_VariablesNode +offsetnode.theta,1);
h = node(NodeNum).h;

    switch Trigger.caviation
        
        case 'on'       % Cavitation model, JFO Function / No Cavitation (no negative pressure)

%           f = p + 1-theta - sqrt((p)^2 + (1-theta)^2 + 2*del^2 ) ;
           f = p + (1-theta) - (abs(p)^z + abs((1-theta))^z + 2*del^2)^(1/z) ;

        case 'off'      % No Cavitation model,(with negative pressure)
            
            f = 1 - theta;    


    end
    
end
