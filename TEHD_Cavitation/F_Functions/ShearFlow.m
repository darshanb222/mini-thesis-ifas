function [f] = ShearFlow(element,Node,x)

global  n_VariablesNode offsetElement n_node n_VariablesElement node offsetnode v omega r_1
NodeA = element.NodeA ;
NodeB = element.NodeB ;

h1 = node(NodeA).h;
h2 = node(NodeB).h;
eta1 = node(NodeA).eta;
eta2 = node(NodeB).eta;
rho1 = node(NodeA).rho;
rho2 = node(NodeB).rho;
theta1 = x((NodeA -1)* n_VariablesNode +offsetnode.theta,1);
theta2 = x((NodeB -1)* n_VariablesNode +offsetnode.theta,1);


Qc = x(n_node * n_VariablesNode + (element.No -1) * n_VariablesElement + offsetElement.Qc,1);

    if element.direction == 0       % x-direction
       v1 = v.x.top /(omega*r_1);
       v2 = v.x.bottom /(omega*r_1);
    elseif element.direction == 1   % y-direction
       v1 = v.y.top /(omega*r_1);
       v2 = v.y.bottom /(omega*r_1);
    end
    
    b = element.width;
    l = element.length;

    f = (theta1+theta2)*0.5 * (rho1 + rho2)*0.5 * ((h1+h2)*0.5) *(v1+v2)*0.5- Qc;
%     f = (theta*(rho1 + rho2)*0.5 * ((h1+h2)*0.5) * v *b * 0.5 - Qc;
end