function [f] = VolumeFlowBalance(node2element,x,NodeNum)

global n_VariablesNode offsetElement  n_node n_VariablesElement offsetnode e dx del Trigger


switch Trigger.Dimension
    
    case '1D' 
        indexQc.PosX = n_node * n_VariablesNode + (node2element.PosX-1)*n_VariablesElement + offsetElement.Qc;
        indexQc.NegX = n_node * n_VariablesNode + (node2element.NegX-1)*n_VariablesElement + offsetElement.Qc;

        indexQp.PosX = n_node * n_VariablesNode + (node2element.PosX-1)*n_VariablesElement + offsetElement.Qp;
        indexQp.NegX = n_node * n_VariablesNode + (node2element.NegX-1)*n_VariablesElement + offsetElement.Qp;

    case '2D' 
        indexQc.PosX = n_node * n_VariablesNode + (node2element.PosX-1)*n_VariablesElement + offsetElement.Qc;
        indexQc.NegX = n_node * n_VariablesNode + (node2element.NegX-1)*n_VariablesElement + offsetElement.Qc;
        indexQc.PosY = n_node * n_VariablesNode + (node2element.PosY-1)*n_VariablesElement + offsetElement.Qc;
        indexQc.NegY = n_node * n_VariablesNode + (node2element.NegY-1)*n_VariablesElement + offsetElement.Qc;

        indexQp.PosX = n_node * n_VariablesNode + (node2element.PosX-1)*n_VariablesElement + offsetElement.Qp;
        indexQp.NegX = n_node * n_VariablesNode + (node2element.NegX-1)*n_VariablesElement + offsetElement.Qp;
        indexQp.PosY = n_node * n_VariablesNode + (node2element.PosY-1)*n_VariablesElement + offsetElement.Qp;
        indexQp.NegY = n_node * n_VariablesNode + (node2element.NegY-1)*n_VariablesElement + offsetElement.Qp;
end

fieldIndexQc = fieldnames(indexQc);

    for i = 1:numel(fieldIndexQc)
        
        value = indexQc.(fieldIndexQc{i});
        index = value;
        Qc.(fieldIndexQc{i}) = x(index,1);

        value2 = indexQp.(fieldIndexQc{i});
        index = value2;
        Qp.(fieldIndexQc{i}) = x(index,1);
        
    end
    
switch Trigger.Dimension
    
    case '1D'
        
    sumQc = -Qc.PosX...
            +Qc.NegX;
    sumQp = -Qp.PosX...
            +Qp.NegX;

    case '2D' 
        
    sumQc = -Qc.PosX ...
            +Qc.NegX ...
            -Qc.PosY ...
            +Qc.NegY ;

    sumQp = -Qp.PosX ...
            +Qp.NegX ...
            -Qp.PosY...
            +Qp.NegY;

end


% f = sumQc + sumQp  ;
 f = sumQc + sumQp ;

end
