function f = P_cor(Node,x)

global  n_VariablesNode  offsetnode 

p = x((Node.No -1) * n_VariablesNode +offsetnode.p,1);
p_cor = x((Node.No -1) * n_VariablesNode +offsetnode.p_cor,1);

unit = 1e5;
c = -0.3;
del = 0.1*unit;

f =sqrt((-0.5*p).^2 + c^2+ 2*del)-(c - 0.5*p) - p_cor;

end