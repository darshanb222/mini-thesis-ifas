function [f] = Pressureflow(element,x)

global offsetnode offset_element n_VariablesNode offsetElement node n_VariablesElement

NodeA = element.NodeA;
NodeB = element.NodeB;

h1 = node(NodeA).h;
h2 = node(NodeB).h;
eta1 = node(NodeA).eta;
eta2 = node(NodeB).eta;
rho1 = node(NodeA).rho;
rho2 = node(NodeB).rho;

p1 = x((NodeA -1)* n_VariablesNode +offsetnode.p,1);
p2 = x((NodeB -1)* n_VariablesNode +offsetnode.p,1);

theta1 = x((NodeA -1)* n_VariablesNode +offsetnode.theta,1);
theta2 = x((NodeB -1)* n_VariablesNode +offsetnode.theta,1);
 
Qp = x(offset_element + (element.No -1) * n_VariablesElement + offsetElement.Qp,1);


b = element.width;
l = element.length;
k = 12;

f = (theta1+theta2)*0.5 * (rho1+rho2)*0.5 *((h1+h2)*0.5)^3  / (k*(eta1 + eta2)*0.5 * l) * (p1-p2) - Qp ;
% f =  (rho1+rho2)*0.5 *((h1+h2)*0.5)^3 / (k*(eta1 + eta2)*0.5 * l) * (p1-p2) - Qp ;

end
