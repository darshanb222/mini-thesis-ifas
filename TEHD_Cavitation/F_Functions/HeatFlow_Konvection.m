function [f] = HeatFlow_Konvection(element,Node,x)

global offsetnode offset_element n_VariablesNode offsetElement node n_VariablesElement Cp

NodeA = element.NodeA;
NodeB = element.NodeB;

h1 = node(NodeA).h;
h2 = node(NodeB).h;
eta1 = node(NodeA).eta;
eta2 = node(NodeB).eta;
rho1 = node(NodeA).rho;
rho2 = node(NodeB).rho;

T1 = x((NodeA -1)* n_VariablesNode +offsetnode.T,1);
T2 = x((NodeB -1)* n_VariablesNode +offsetnode.T,1);

T_conv = x(offset_element + (element.No -1) * n_VariablesElement + offsetElement.T_conv,1);
Qc = x(offset_element + (element.No -1) * n_VariablesElement + offsetElement.Qc,1);
Qp = x(offset_element + (element.No -1) * n_VariablesElement + offsetElement.Qp,1);

if element.direction == 0       % x-direction
   v1 = Node(NodeA).v_x;
   v2 = Node(NodeB).v_x;
elseif element.direction == 1   % y-direction
   v1 = Node(NodeA).v_y;
   v2 = Node(NodeB).v_y;
end

v = (v1+v2)*0.5;
b = element.width;
l = element.length;

f = (Qc+Qp) * (T1-T2)/l- T_conv ;

end
