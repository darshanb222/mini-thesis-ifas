function [f] = Dissipation(node2element,x,NodeNum)

global n_VariablesNode offsetElement  n_node n_VariablesElement offsetnode  node density

Phi = x((NodeNum -1)* n_VariablesNode +offsetnode.Phi,1);

indexVelocity.PosX = n_node * n_VariablesNode + (node2element.PosX-1)*n_VariablesElement + offsetElement.Velocity;
indexVelocity.NegX = n_node * n_VariablesNode + (node2element.NegX-1)*n_VariablesElement + offsetElement.Velocity;
indexVelocity.PosY = n_node * n_VariablesNode + (node2element.PosY-1)*n_VariablesElement + offsetElement.Velocity;
indexVelocity.NegY = n_node * n_VariablesNode + (node2element.NegY-1)*n_VariablesElement + offsetElement.Velocity;

fieldIndexVelocity = fieldnames(indexVelocity);

    for i = 1:numel(fieldIndexVelocity)
        
        value = indexVelocity.(fieldIndexVelocity{i});
        index = value;
        Velocity.(fieldIndexVelocity{i}) = x(index,1);

    end

h = node(NodeNum).h;    
    
    f = (((Velocity.NegX - Velocity.PosX)/(h/2))^2 + ((Velocity.NegY - Velocity.PosY)/(h/2))^2)*density + Phi;


end
