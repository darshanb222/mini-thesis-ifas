function [f] = Velocity(element,x)

global offsetnode offset_element n_VariablesNode offsetElement node n_VariablesElement density v r_1 omega

NodeA = element.NodeA;
NodeB = element.NodeB;

h1 = node(NodeA).h;
h2 = node(NodeB).h;
eta1 = node(NodeA).eta;
eta2 = node(NodeB).eta;
rho1 = node(NodeA).rho;
rho2 = node(NodeB).rho;

p1 = x((NodeA -1)* n_VariablesNode +offsetnode.p,1);
p2 = x((NodeB -1)* n_VariablesNode +offsetnode.p,1);

 
Velocity = x(offset_element + (element.No -1) * n_VariablesElement + offsetElement.Velocity,1);


if element.direction == 0       % x-direction
   v1 = v.x.top /(omega*r_1);
   v2 = v.x.bottom /(omega*r_1);
elseif element.direction == 1   % y-direction
   v1 = v.y.top /(omega*r_1);
   v2 = v.y.bottom /(omega*r_1);
end

b = element.width;
l = element.length;
h = (h1+h2)*.5;


f = (p1-p2)/l *(1/density) *0.5*(-h^2/4)+ v1/2 + v2/2  - Velocity ;

% f =  (rho1+rho2)*0.5 *((h1+h2)*0.5)^3 / (k*(eta1 + eta2)*0.5 * l) * (p1-p2) - Qp ;

end
