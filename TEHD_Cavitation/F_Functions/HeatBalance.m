function [f] = HeatBalance(node2element,x,NodeNum)

global n_VariablesNode offsetElement  n_node n_VariablesElement offsetnode e dx del Trigger


indexT_conv.PosX = n_node * n_VariablesNode + (node2element.PosX-1)*n_VariablesElement + offsetElement.T_conv;
indexT_conv.NegX = n_node * n_VariablesNode + (node2element.NegX-1)*n_VariablesElement + offsetElement.T_conv;
indexT_conv.PosY = n_node * n_VariablesNode + (node2element.PosY-1)*n_VariablesElement + offsetElement.T_conv;
indexT_conv.NegY = n_node * n_VariablesNode + (node2element.NegY-1)*n_VariablesElement + offsetElement.T_conv;

fieldIndexT_conv = fieldnames(indexT_conv);
Dissipation = x((NodeNum -1)* n_VariablesNode +offsetnode.Phi,1);

    for i = 1:numel(fieldIndexT_conv)
        
        value = indexT_conv.(fieldIndexT_conv{i});
        index = value;
        T_conv.(fieldIndexT_conv{i}) = x(index,1);

    end

    sumT_conv = -T_conv.PosX ...
                +T_conv.NegX ...
                -T_conv.PosY ...
                +T_conv.NegY ;
 
    f =  sumT_conv -Dissipation;


end
