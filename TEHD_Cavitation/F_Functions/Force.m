function f = Force(Node,x)

global  n_VariablesNode  offsetnode node dx dy
p = x((Node.No -1) * n_VariablesNode +offsetnode.p,1);
p_c = x((Node.No -1) * n_VariablesNode +offsetnode.p_c,1);

Fx = x((Node.No -1) * n_VariablesNode +offsetnode.Fx,1);
Fy = x((Node.No -1) * n_VariablesNode +offsetnode.Fy,1);

phi = node(Node.No).phi;


A = dx*dy; %Todo: This Area must be changed because of moving mesh


f(1) = (p+p_c)*A*cos(phi) - Fx;     % Fx: sum of Force X = 0
f(2) = (p+p_c)*A*sin(phi) - Fy;     % Fy: sum of Force Y = 0

end
