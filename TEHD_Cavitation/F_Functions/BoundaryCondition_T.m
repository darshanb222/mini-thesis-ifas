function [f] = BoundaryCondition_T(Node,x)

global  n_VariablesNode  offsetnode node

T = x((Node.No -1) * n_VariablesNode +offsetnode.T,1);

f = node(Node.No).T - T;

end