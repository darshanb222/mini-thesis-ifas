clear all; clc; close all;

global n_node n_element n_VariablesNode offsetnode n_VariablesElement offset_element VariablesTotalNum offsetElement 
global node e  Trigger nx ny dx dy density nue node2element offset_variables n_VariablesSum offsetSum epsilon h_G r_1 del alpha lamda z


addpath('./F_Functions/'); % add function path
addpath('./J_Functions/'); % add function path
addpath('./Help_Functions/'); % add function path
addpath('./Solver/'); % add function path

epsilon = 0.6;
r_1 = 25/2*1e-3;
h_G = 15*1e-6;

B = r_1*2*pi;  nx = 400; dx = B/(nx-1);  %B = pi*5;
L = 60*1e-3; ny = 1; dy = L/(ny-1);

rpm = 800;
omega = -rpm*2*pi/60;  % rotational speed in 1/s(5000)

del = (dx^2)*1*0; alpha = 1e-10; z = 4;
%% Equation Parameter

n_node = nx * ny;
n_element = (nx)*ny;


Trigger.caviation = 'on';
Trigger.Dimension = '1D';

%--------------------------------------------------------------------------
offsetnode.p     = 1;
offsetnode.theta = 2;

n_VariablesNode = numel(fieldnames(offsetnode));

offset_element = n_node * n_VariablesNode;

%--------------------------------------------------------------------------
offsetElement.Qc = 1;
offsetElement.Qp = 2;

n_VariablesElement = numel(fieldnames(offsetElement)); 

offset_variables = offset_element + n_element * n_VariablesElement;

%--------------------------------------------------------------------------


VariablesTotalNum = n_node * n_VariablesNode + n_element*n_VariablesElement ;

% var = zeros(VariablesTotalNum,1);



% Oil: 46 cSt, 900 kg/m3
nue = 46e-6;     % kinematic viscosity
density = 900;   % density


%% Node Info
for i = 1 : nx
    for j = 1 : ny
        x(i,j) = -B/2 + (i-1)*dx;
        phi(i,j) = x(i,j)/r_1;
        h(i,j) = h_G*(1 + epsilon*cos(phi(i,j)   ));
        eta(i,j) = nue*density;
        rho(i,j) = density;
        v_x(i,j) = 1*omega*r_1;

    end
end

%% Dimensionless

X = x/r_1;
H = h/h_G;
ETA = eta/(nue*density);

%% Boundary Condition

p = zeros(nx,ny) ;
p_c = zeros(nx,ny) ;

factor = zeros(nx,ny)+1;

BC = zeros(nx,ny);

p(1,:) = 1/(nue*density*(-r_1*omega)*r_1/(h_G)^2*1e-5);
BC(1,:) = 1;

p(nx,:) = 1/(nue*density*(-r_1*omega)*r_1/(h_G)^2*1e-5);
BC(nx,:) = 1;

%%
% x-y coordinate -> 1D coordinate

for i = 1 : nx
    for j = 1 : ny
        
    node(i+(j-1)*nx).No =  i+(j-1)*nx;   %Node Number
    node(i+(j-1)*nx).x = X(i,j) ;
    node(i+(j-1)*nx).h = H(i,j) ;
    node(i+(j-1)*nx).eta = ETA(i,j);
    node(i+(j-1)*nx).rho = rho(i,j)/density;
    node(i+(j-1)*nx).v_x = v_x(i,j)/(r_1*omega);
    node(i+(j-1)*nx).p = p(i,j);
    node(i+(j-1)*nx).BC = BC(i,j);
    
    end
end

% Each node has N - equations.

%% Make Element

% X Direction Num_element = (nx-1)*ny
for i = 1:nx
    for j = 1:ny
        
    e(i+(j-1)*(nx-1)).No = i+(j-1)*(nx-1);                                 % Element No
    e(i+(j-1)*(nx-1)).NodeA = i+(j-1)*nx;                                  % Number of NodeA

    if i+(j-1)*nx+1 > nx
        e(i+(j-1)*(nx-1)).NodeB = 1;                                % Number of NodeB
    else
        e(i+(j-1)*(nx-1)).NodeB = i+(j-1)*nx+1;                                % Number of NodeB
    end
    
    e(i+(j-1)*(nx-1)).length = dx/r_1;                      % TODO: Length and Width must be calculated for irregular mesh
    e(i+(j-1)*(nx-1)).width  = dy/L;
    
    e(i+(j-1)*(nx-1)).direction = 0; % 0 == x direction
    
    end
end


% Find Element which is connected with node(x)                  

for i = 1:nx-1
    for j = 1:ny
        
        node2element(i+(j-1)*nx).PosX = i+(j-1)*nx;
        
        if i-1+(j-1)*nx == 0
            node2element(i+(j-1)*nx).NegX = nx;
        else
            node2element(i+(j-1)*nx).NegX = i-1+(j-1)*nx;
        end
            
    end
end


%% Initialization

var = zeros(VariablesTotalNum,1);
% 
for i = 1 : n_node
    
    var((i-1) * n_VariablesNode + offsetnode.p,1) = node(i).p;
    var((i-1) * n_VariablesNode + offsetnode.theta,1) = 1;

end

% %% Make Equations
%     
    [xx,xy] = NewtonMethod(@F,@J,var);
for i = 1 : nx
    for j = 1 : ny
     p(i,j)  =xx((j-1)*nx*n_VariablesNode+(i-1)*n_VariablesNode+offsetnode.p);
     fac(i,j)=xx((j-1)*nx*n_VariablesNode+(i-1)*n_VariablesNode+offsetnode.theta);
    end
end

for i = 1 : nx
    for j = 1 : ny
     Qc(i,j)= xx(offset_element+(i-1)*n_VariablesElement+offsetElement.Qc);
     Qp(i,j)= xx(offset_element+(i-1)*n_VariablesElement+offsetElement.Qp);
    end
end



%% Figure
set_fig(6, 4, 3)

figure(1)
plot(X,H*h_G*1e6)
title('Gap Height');

figure(2)
plot(X,p*nue*density*(-r_1*omega)*r_1/(h_G)^2*1e-5)
title('Pressure Field');

hold on;
% plot(X,fac)

figure(3)
plot(X,fac)
title('Film Fraction');
% ylim([0,1.2]);

figure(4)
plot(Qp)
title('Qp');

figure(5)
plot(Qc)
title('Qc');
figure(6)
plot(Qp+Qc)
title('Q');
% ylim([0,1.2]);
% xlabel('x axis'); ylabel('y axis');
% s.FaceLighting = 'gouraud';
% % shading interp zlim([0,1]); set(gca,'YTick',[0 : 0.1 : 1]);
% 
% hold on;
% 
% % xx = [0.04 0.04 -0.04 -0.04]; yy = [-0.04 0.04 0.04 -0.04]; zz = [1 1 1
% % 1]; patch(xx,yy,zz,'b','FaceAlpha',0.2);
% 
% hold on;
% 
% % xx = [0.04 0.04 -0.04 -0.04]; yy = [-0.04 0.04 0.04 -0.04]; zz = [0 0 0
% % 0]; patch(xx,yy,zz,'b','FaceAlpha',0.2);
% 
% % zlim([0,1]); set(gca,'ZTick',[0 : 0.1 : 1]); zlabel('z axis');
% figure(4)
% surf(x,y,h*1e6)
% % hold on; surf(x,y,h1*1e6)
% 
% view([-45 -45 50])
% title('Gap Height');
% xlabel('x axis'); ylabel('y axis');
% zlim([0,13]); set(gca,'ZTick',[0 : 2 : 12]); zlabel('Gap Height');
% % shading interp
% 
% figure(5)
% surf(x,y,rho)
% view([-45 -45 50])
% title('Density');
% xlabel('x axis'); ylabel('y axis');
% % zlim([0,13]); set(gca,'ZTick',[0 : 1 : 13]); zlabel('Gap Height');
% % shading interp
% 
% figure(6)
% surf(x,y,Fx)
% view([-45 -45 50])
% title('Fx');
% xlabel('x axis'); ylabel('y axis');
% % shading interp
% 
% figure(7)
% surf(x,y,Fy)
% view([-45 -45 50])
% title('Fy');
% xlabel('x axis'); ylabel('y axis');
% % shading interp grid on, grid minor
% 
% % figure(8) surf(x,y,p_cor*1e-5) view([-45 -45 50]) % view([90 0 0])
% % 
% % title('Pressure Field'); xlabel('x axis'); ylabel('y axis');
% 
% figure(9)
% surf(x,y,(p_c+p)*1e-5)
% view([-45 -45 50])
% % view([90 0 0])
% 
% title('P + P_c');
% xlabel('x axis'); ylabel('y axis');
% 
% %----------------------------------------
% E1 = 210000.0*1.e6;   % Young's modulus steel in N_m^2
% E2 = 100000.0*1.e6;   % Young's  brass in N_m^2
% ny1 = (0.27+0.3)*0.5; % Poisson number steel (Ref: wikipedia)
% ny2 = 0.37;           % Poisson number brass (Ref: wikipedia)
% meanE = 2.0*E1*E2 / ( E1*(1-ny2*ny2)+E2*(1-ny1*ny1) );
% deformation = (p+p_c).*r_1/    meanE;
% 
% figure(10)
% surf(x,y,deformation*1e6)
% view([-45 -45 50])
% % view([90 0 0])
% 
% title('Deformation');
% xlabel('x axis'); ylabel('y axis');
% 
% % shading interp xlim([-8,8]); set(gca,'XTick',[-8 : 1 : 8]); xlabel('x
% % axis'); ylim([-8,8]); set(gca,'YTick',[-8 : 1 : 8]); ylabel('y axis');