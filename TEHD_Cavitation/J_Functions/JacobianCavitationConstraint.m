function [i_index,j_index,J] = JacobianCavitationConstraint(NodeNum,x)
global offsetnode n_VariablesNode  Trigger  node dx r_1 del z
p = x((NodeNum -1)* n_VariablesNode +offsetnode.p,1) ;
theta = x((NodeNum -1)* n_VariablesNode +offsetnode.theta,1) ;

h = node(NodeNum).h;
cf = h^8/(3*(dx/r_1)^8)*5;
e = 1e-3;

    switch Trigger.caviation
        
        case 'on'
%             f = 1 +cf*p*(1+p/sqrt(p^2+e^2))*0.5 - theta;  
            i_index(1) = (NodeNum-1) * n_VariablesNode + offsetnode.theta;
            j_index(1) = (NodeNum -1)* n_VariablesNode +offsetnode.p;
%             J(1) = 1 - p/sqrt(p^2+(1-theta)^2 + 2*del^2);
            J(1) = 1 - p^(z-1)*(p^z+(1-theta)^z + 2*del^2)^(1/z-1);
            
            i_index(2) = (NodeNum-1) * n_VariablesNode + offsetnode.theta;
            j_index(2) = (NodeNum -1)* n_VariablesNode +offsetnode.theta;
%             J(2) = -1 +(1- theta)/sqrt(p^2+(1-theta)^2 + 2*del^2);
            J(2) = -1 - (1-theta)^(z-1)*(-(abs(p)^z+abs(1-theta)^z + 2*del^2)^(1/z-1));

        case 'off'
            i_index(1) = (NodeNum -1) * n_VariablesNode + offsetnode.theta;
            j_index(1) = (NodeNum -1) * n_VariablesNode + offsetnode.theta;
            J(1) = - 1;
            
    end
end