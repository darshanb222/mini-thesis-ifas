function [i_index, j_index, J] = JacobianP_cor(Node,x)

global  n_VariablesNode  offsetnode 

p = x((Node.No -1) * n_VariablesNode +offsetnode.p,1);
p_cor = x((Node.No -1) * n_VariablesNode +offsetnode.p_cor,1);

unit = 1e5;
c = -0.3;
del = 0.1*unit;

f =sqrt((-0.5*p).^2 + c^2+ 2*del)-(c - 0.5*p) - p_cor;

i_index(1) = (Node.No -1) * n_VariablesNode + offsetnode.p_cor;
j_index(1) = (Node.No -1) * n_VariablesNode +offsetnode.p_cor;
J(1) = -1;

i_index(2) = (Node.No -1) * n_VariablesNode + offsetnode.p_cor;
j_index(2) = (Node.No -1) * n_VariablesNode +offsetnode.p;
J(2) = 0.5*p/sqrt((-0.5*p).^2 + c^2+ 2*del) + 0.5;

end