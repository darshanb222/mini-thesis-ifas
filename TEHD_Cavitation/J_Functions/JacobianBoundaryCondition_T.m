function [i_index, j_index, J] = JacobianBoundaryCondition_T(Node)
% 
global  n_VariablesNode  offsetnode VariablesTotalNum

% J = sparse(1,VariablesTotalNum);
% J(1,(Node.No -1) * n_VariablesNode +offsetnode.p) = -1;

i_index = (Node.No -1) * n_VariablesNode + offsetnode.T;
j_index = (Node.No -1) * n_VariablesNode +offsetnode.T;
J = -1;

end