function [i_index,j_index,J] = JacobianForce(Node,x)

global  n_VariablesNode  offsetnode node dx dy
p = x((Node.No -1) * n_VariablesNode +offsetnode.p,1);
p_c = x((Node.No -1) * n_VariablesNode +offsetnode.p_c,1);

Fx = x((Node.No -1) * n_VariablesNode +offsetnode.Fx,1);
Fy = x((Node.No -1) * n_VariablesNode +offsetnode.Fy,1);

phi = node(Node.No).phi;

A = dx*dy; %Todo: This Area must be changed because of moving mesh

i_index(1) = (Node.No-1) * n_VariablesNode + offsetnode.Fx;
j_index(1) = (Node.No-1) * n_VariablesNode + offsetnode.Fx;
J(1)=  -1;

i_index(2) = (Node.No-1) * n_VariablesNode + offsetnode.Fx;
j_index(2) = (Node.No-1) * n_VariablesNode + offsetnode.p;
J(2) =  A*cos(phi);

i_index(3) = (Node.No-1) * n_VariablesNode + offsetnode.Fx;
j_index(3) = (Node.No-1) * n_VariablesNode + offsetnode.p_c;
J(3) =  A*cos(phi);

i_index(4) = (Node.No-1) * n_VariablesNode + offsetnode.Fy;
j_index(4) = (Node.No-1) * n_VariablesNode + offsetnode.Fy;
J(4) =  -1;

i_index(5) = (Node.No-1) * n_VariablesNode + offsetnode.Fx;
j_index(5) = (Node.No-1) * n_VariablesNode + offsetnode.p;
J(5) =  A*sin(phi) ;

i_index(6) = (Node.No-1) * n_VariablesNode + offsetnode.Fx;
j_index(6) = (Node.No-1) * n_VariablesNode + offsetnode.p_c;
J(6) =  A*sin(phi) ;
end