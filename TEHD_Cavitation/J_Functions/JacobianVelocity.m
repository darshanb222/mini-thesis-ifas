function [i_index,j_index,J] = JacobianVelocity(element,x)
% 
    global offsetnode offset_element n_VariablesNode offsetElement VariablesTotalNum n_VariablesElement n_node v omega
    global node r_1 density

NodeA = element.NodeA;
NodeB = element.NodeB;

h1 = node(NodeA).h;
h2 = node(NodeB).h;
eta1 = node(NodeA).eta;
eta2 = node(NodeB).eta;
rho1 = node(NodeA).rho;
rho2 = node(NodeB).rho;

p1 = x((NodeA -1)* n_VariablesNode +offsetnode.p,1);
p2 = x((NodeB -1)* n_VariablesNode +offsetnode.p,1);

 
Velocity = x(offset_element + (element.No -1) * n_VariablesElement + offsetElement.Velocity,1);


if element.direction == 0       % x-direction
   v1 = v.x.top /(omega*r_1);
   v2 = v.x.bottom /(omega*r_1);
elseif element.direction == 1   % y-direction
   v1 = v.y.top /(omega*r_1);
   v2 = v.y.bottom /(omega*r_1);
end

b = element.width;
l = element.length;
h = (h1+h2)*.5;


    i_index(1) = n_node * n_VariablesNode+(element.No-1)*n_VariablesElement + offsetElement.Velocity;
    j_index(1) = offset_element + (element.No-1)*n_VariablesElement + offsetElement.Velocity;            % Qp
    J(1) = -1;

    i_index(2) = n_node * n_VariablesNode+(element.No-1)*n_VariablesElement + offsetElement.Velocity;
    j_index(2) = (NodeA-1) * n_VariablesNode + offsetnode.p;                                % P1
    J(2) =  1 /l *(1/density) *0.5*(-h^2/4) ;

    i_index(3) = n_node * n_VariablesNode+(element.No-1)*n_VariablesElement + offsetElement.Velocity;
    j_index(3) = (NodeB-1) * n_VariablesNode + offsetnode.p;                                % P2
    J(3) = - 1 /l *(1/density) *0.5*(-h^2/4) ;

end