function [i_index,j_index,J] = JacobianVolumeFlowBalance(node2element,NodeNum,x)
% 
global n_VariablesNode offsetElement  n_node VariablesTotalNum  n_VariablesElement offsetnode e Trigger

switch Trigger.Dimension
    
    case '1D' 
        indexQc.PosX = n_node * n_VariablesNode + (node2element.PosX-1)*n_VariablesElement + offsetElement.Qc;
        indexQc.NegX = n_node * n_VariablesNode + (node2element.NegX-1)*n_VariablesElement + offsetElement.Qc;

        indexQp.PosX = n_node * n_VariablesNode + (node2element.PosX-1)*n_VariablesElement + offsetElement.Qp;
        indexQp.NegX = n_node * n_VariablesNode + (node2element.NegX-1)*n_VariablesElement + offsetElement.Qp;

    case '2D' 
        indexQc.PosX = n_node * n_VariablesNode + (node2element.PosX-1)*n_VariablesElement + offsetElement.Qc;
        indexQc.NegX = n_node * n_VariablesNode + (node2element.NegX-1)*n_VariablesElement + offsetElement.Qc;
        indexQc.PosY = n_node * n_VariablesNode + (node2element.PosY-1)*n_VariablesElement + offsetElement.Qc;
        indexQc.NegY = n_node * n_VariablesNode + (node2element.NegY-1)*n_VariablesElement + offsetElement.Qc;

        indexQp.PosX = n_node * n_VariablesNode + (node2element.PosX-1)*n_VariablesElement + offsetElement.Qp;
        indexQp.NegX = n_node * n_VariablesNode + (node2element.NegX-1)*n_VariablesElement + offsetElement.Qp;
        indexQp.PosY = n_node * n_VariablesNode + (node2element.PosY-1)*n_VariablesElement + offsetElement.Qp;
        indexQp.NegY = n_node * n_VariablesNode + (node2element.NegY-1)*n_VariablesElement + offsetElement.Qp;
end

fieldIndexQc = fieldnames(indexQc);

    for i = 1:numel(fieldIndexQc)
        
        value = indexQc.(fieldIndexQc{i});
        index = value;
        Qc.(fieldIndexQc{i}) = x(index,1);

        value2 = indexQp.(fieldIndexQc{i});
        index = value2;
        Qp.(fieldIndexQc{i}) = x(index,1);
        
    end

switch Trigger.Dimension
    
    case '1D' 
    i_index(1) = (NodeNum-1) * n_VariablesNode + offsetnode.p;
    j_index(1) = indexQc.PosX;
    J(1) = -1;

    i_index(2) = (NodeNum-1) * n_VariablesNode + offsetnode.p;
    j_index(2) = indexQc.NegX;
    J(2) = +1;

    i_index(3) = (NodeNum-1) * n_VariablesNode + offsetnode.p;
    j_index(3) = indexQp.PosX;
    J(3) = -1;

    i_index(4) = (NodeNum-1) * n_VariablesNode + offsetnode.p;
    j_index(4) = indexQp.NegX;
    J(4) = +1;

    case '2D' 
        
    i_index(1) = (NodeNum-1) * n_VariablesNode + offsetnode.p;
    j_index(1) = indexQc.PosX;
    J(1) = -1;

    i_index(2) = (NodeNum-1) * n_VariablesNode + offsetnode.p;
    j_index(2) = indexQc.PosY;
    J(2) = -1;

    i_index(3) = (NodeNum-1) * n_VariablesNode + offsetnode.p;
    j_index(3) = indexQc.NegX;
    J(3) = +1;

    i_index(4) = (NodeNum-1) * n_VariablesNode + offsetnode.p;
    j_index(4) = indexQc.NegY;
    J(4) = +1;

    i_index(5) = (NodeNum-1) * n_VariablesNode + offsetnode.p;
    j_index(5) = indexQp.PosX;
    J(5) = -1;

    i_index(6) = (NodeNum-1) * n_VariablesNode + offsetnode.p;
    j_index(6) = indexQp.PosY;
    J(6) = -1;

    i_index(7) = (NodeNum-1) * n_VariablesNode + offsetnode.p;
    j_index(7) = indexQp.NegX;
    J(7) = +1;

    i_index(8) = (NodeNum-1) * n_VariablesNode + offsetnode.p;
    j_index(8) = indexQp.NegY;
    J(8) = +1;
end




end