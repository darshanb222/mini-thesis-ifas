function [i_index,j_index,J] = JacobianDissipation(node2element,NodeNum,x)

global n_VariablesNode offsetElement  n_node n_VariablesElement offsetnode e dx del Trigger density node


Phi = x((NodeNum -1)* n_VariablesNode +offsetnode.Phi,1);

indexVelocity.PosX = n_node * n_VariablesNode + (node2element.PosX-1)*n_VariablesElement + offsetElement.Velocity;
indexVelocity.NegX = n_node * n_VariablesNode + (node2element.NegX-1)*n_VariablesElement + offsetElement.Velocity;
indexVelocity.PosY = n_node * n_VariablesNode + (node2element.PosY-1)*n_VariablesElement + offsetElement.Velocity;
indexVelocity.NegY = n_node * n_VariablesNode + (node2element.NegY-1)*n_VariablesElement + offsetElement.Velocity;

fieldIndexVelocity = fieldnames(indexVelocity);

    for i = 1:numel(fieldIndexVelocity)
        
        value = indexVelocity.(fieldIndexVelocity{i});
        index = value;
        Velocity.(fieldIndexVelocity{i}) = x(index,1);

    end

h = node(NodeNum).h;    

    i_index(1) = (NodeNum-1) * n_VariablesNode + offsetnode.Phi;
    j_index(1) = indexVelocity.NegX;
    J(1) = 8*(Velocity.NegX - Velocity.PosX)/h^2*density;

    i_index(2) = (NodeNum-1) * n_VariablesNode + offsetnode.Phi;
    j_index(2) = indexVelocity.PosX;
    J(2) = -8*(Velocity.NegX - Velocity.PosX)/h^2*density;

    i_index(3) = (NodeNum-1) * n_VariablesNode + offsetnode.Phi;
    j_index(3) = indexVelocity.NegY;
    J(3) = 8*(Velocity.NegY - Velocity.PosY)/h^2*density;

    i_index(4) = (NodeNum-1) * n_VariablesNode + offsetnode.Phi;
    j_index(4) = indexVelocity.PosY;
    J(4) = -8*(Velocity.NegY - Velocity.PosY)/h^2*density;

    i_index(5) = (NodeNum-1) * n_VariablesNode + offsetnode.Phi;
    j_index(5) = (NodeNum-1) * n_VariablesNode + offsetnode.Phi;
    J(5) = +1;
end
