function [i_index, j_index, J] = JacobianBoundaryCondition(Node)
% 
global  n_VariablesNode  offsetnode VariablesTotalNum

i_index = (Node.No -1) * n_VariablesNode + offsetnode.p;
j_index = (Node.No -1) * n_VariablesNode +offsetnode.p;
J = -1;

end