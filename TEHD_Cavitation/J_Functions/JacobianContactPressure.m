
function [i_index,j_index,J] = JacobianContactPressure(Node,x)

    global n_VariablesNode VariablesTotalNum offsetnode node
    h = x((Node.No -1)* n_VariablesNode +offsetnode.h,1);
    
    K = 3.e-3;            % Ref: Dissertation Lang, S.33
    E1 = 210000.0*1.e6;   % Young's modulus steel in N_m^2
    E2 = 100000.0*1.e6;   % Young's  brass in N_m^2
    E2 = 210000.0*1.e6;   % Young's modulus steel in N_m^2
    ny1 = (0.27+0.3)*0.5; % Poisson number steel (Ref: wikipedia)
    ny2 = 0.37;           % Poisson number brass (Ref: wikipedia)
    ny2 = (0.27+0.3)*0.5; % Poisson number steel (Ref: wikipedia)
    meanE = 2.0*E1*E2 / ( E1*(1-ny2*ny2)+E2*(1-ny1*ny1) );

    A = K*meanE;          % constant
    B = 4.4086*1.e-5;     % constant
    C = 4.;               % constant
    D = h;
    E = 1.0*1.e-6;        % Sigma
    F = 6.804;            % konst
    M = 1.e10;            % Gradient tanh
    N = 0.25*1.e-6;        % Cutoff
    
    Reduce1 = C-D/E;
    Reduce2 = M*(D-N);
    Reduce3 = 0.5*(1-tanh(Reduce2)); 
        
        i_index(1) = (Node.No -1) * n_VariablesNode + offsetnode.p_c;
        j_index(1) = (Node.No -1) * n_VariablesNode + offsetnode.p_c;
        J(1) = -1;
        
        i_index(2) = (Node.No -1) * n_VariablesNode + offsetnode.p_c;
        j_index(2) = (Node.No -1) * n_VariablesNode + offsetnode.h;
        J(2) = A*B*F*(Reduce1*Reduce3)^(F-1)*(-1)*(Reduce3)/E;

end