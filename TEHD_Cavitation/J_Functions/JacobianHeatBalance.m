function [i_index,j_index,J] = JacobianHeatBalance(node2element,NodeNum,x)

global n_VariablesNode offsetElement  n_node n_VariablesElement offsetnode e dx del Trigger


indexT_conv.PosX = n_node * n_VariablesNode + (node2element.PosX-1)*n_VariablesElement + offsetElement.T_conv;
indexT_conv.NegX = n_node * n_VariablesNode + (node2element.NegX-1)*n_VariablesElement + offsetElement.T_conv;
indexT_conv.PosY = n_node * n_VariablesNode + (node2element.PosY-1)*n_VariablesElement + offsetElement.T_conv;
indexT_conv.NegY = n_node * n_VariablesNode + (node2element.NegY-1)*n_VariablesElement + offsetElement.T_conv;

fieldIndexT_conv = fieldnames(indexT_conv);

    for i = 1:numel(fieldIndexT_conv)
        
        value = indexT_conv.(fieldIndexT_conv{i});
        index = value;
        T_conv.(fieldIndexT_conv{i}) = x(index,1);

    end


    i_index(1) = (NodeNum-1) * n_VariablesNode + offsetnode.T;
    j_index(1) = indexT_conv.PosX;
    J(1) = -1;

    i_index(2) = (NodeNum-1) * n_VariablesNode + offsetnode.T;
    j_index(2) = indexT_conv.PosY;
    J(2) = -1;

    i_index(3) = (NodeNum-1) * n_VariablesNode + offsetnode.T;
    j_index(3) = indexT_conv.NegX;
    J(3) = +1;

    i_index(4) = (NodeNum-1) * n_VariablesNode + offsetnode.T;
    j_index(4) = indexT_conv.NegY;
    J(4) = +1;

    i_index(5) = (NodeNum-1) * n_VariablesNode + offsetnode.T;
    j_index(5) = (NodeNum -1)* n_VariablesNode +offsetnode.Phi;
    J(5) = -1;
end
