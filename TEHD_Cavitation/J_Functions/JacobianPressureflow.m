function [i_index,j_index,J] = JacobianPressureflow(element,x)
% 
    global offsetnode offset_element n_VariablesNode offsetElement VariablesTotalNum n_VariablesElement n_node
    global node

    NodeA = element.NodeA;
    NodeB = element.NodeB;

    h1 = node(NodeA).h;
    h2 = node(NodeB).h;
    eta1 = node(NodeA).eta;
    eta2 = node(NodeB).eta;
    rho1 = node(NodeA).rho;
    rho2 = node(NodeB).rho;

    p1 = x((NodeA -1) * n_VariablesNode + offsetnode.p);
    p2 = x((NodeB -1) * n_VariablesNode + offsetnode.p);
% p1 = (sign(p1)*p1+p1)/2;
% p2 = (sign(p2)*p2+p2)/2;
    theta1 = x((NodeA -1)* n_VariablesNode +offsetnode.theta,1);
    theta2 = x((NodeB -1)* n_VariablesNode +offsetnode.theta,1);

    Qp = x(offset_element + (element.No -1) * n_VariablesElement + offsetElement.Qp,1);

    b = element.width;
    l = element.length;
    k = 12;

    i_index(1) = n_node * n_VariablesNode+(element.No-1)*n_VariablesElement + offsetElement.Qp;
    j_index(1) = offset_element + (element.No-1)*n_VariablesElement + offsetElement.Qp;            % Qp
    J(1) = -1;

    i_index(2) = n_node * n_VariablesNode+(element.No-1)*n_VariablesElement + offsetElement.Qp;
    j_index(2) = (NodeA-1) * n_VariablesNode + offsetnode.p;                                % P1
    J(2) =   (theta1+theta2)*0.5 * (rho1+rho2)*0.5* ((h1+h2)*0.5)^3 / (k*(eta1 + eta2)*0.5 * l) ;

    i_index(3) = n_node * n_VariablesNode+(element.No-1)*n_VariablesElement + offsetElement.Qp;
    j_index(3) = (NodeB-1) * n_VariablesNode + offsetnode.p;                                % P2
    J(3) = - (theta1+theta2)*0.5 * (rho1+rho2)*0.5* ((h1+h2)*0.5)^3 / (k*(eta1 + eta2)*0.5 * l) ;

    i_index(4) = n_node * n_VariablesNode+(element.No-1)*n_VariablesElement + offsetElement.Qp;
    j_index(4) = (NodeA-1) * n_VariablesNode + offsetnode.theta;                                % P1
    J(4) =    0.5 * (rho1+rho2)*0.5 *((h1+h2)*0.5)^3 / (k*(eta1 + eta2)*0.5 * l) * (p1-p2) ;

    i_index(5) = n_node * n_VariablesNode+(element.No-1)*n_VariablesElement + offsetElement.Qp;
    j_index(5) = (NodeB-1) * n_VariablesNode + offsetnode.theta;                                % P2
    J(5) = 0.5 * (rho1+rho2)*0.5 *((h1+h2)*0.5)^3 / (k*(eta1 + eta2)*0.5 * l) * (p1-p2) ;
end