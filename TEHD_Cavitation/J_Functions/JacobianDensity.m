function [i_index,j_index,J] = JacobianDensity(Node,x)

global n_VariablesNode offsetnode offset_element n_VariablesElement offset_theta
 
p = x((Node.No -1)* n_VariablesNode +offsetnode.p,1);
rho = x((Node.No -1)* n_VariablesNode +offsetnode.rho,1);
% theta = x(offset_element + (element.No -1) * n_VariablesElement + offset_theta,1);

rho_0 = 874.5;
beta = 1/(15000*1e5);
rho_gas = 10;

% f = rho_0/(1-beta*p)*(1-theta) - rho;

i_index(1) = (Node.No -1) * n_VariablesNode + offsetnode.rho;
j_index(1) = (Node.No -1) * n_VariablesNode + offsetnode.rho;
J(1) =  -1;

i_index(2) = (Node.No -1) * n_VariablesNode + offsetnode.rho;
j_index(2) = (Node.No -1) * n_VariablesNode + offsetnode.p;
% J(2) = -beta*rho_0/(1-beta*p)^2*(1-theta);
J(2) = -beta*rho_0/(1-beta*p)^2;

% i_index(3) = (Node.No -1) * n_VariablesNode + offsetnode.rho;
% j_index(3) = (Node.No -1) * n_VariablesNode + offsetnode.theta;
% J(3) = -rho_0/(1-beta*p);
% 

end
