% clear; clc;




figure(1)
grid off
box off
m = mesh(x,y,x*0);
m.FaceColor = [204 204 204]/255;
m.EdgeColor = [0 0 0]/255;
m.FaceAlpha = 0.6;


for k = 1 : length(c1_i_min)
    i = c1_i_min(k);
    j = c1_j_min(k);
    plot3(x(i,j),y(i,j),x(i,j)*0,'.b','MarkerSize',10)
end
for k = 1 : length(c2_i_min)
    i = c2_i_min(k);
    j = c2_j_min(k);
    plot3(x(i,j),y(i,j),x(i,j)*0,'.r','MarkerSize',10)
end

plot3(Rec_X_in,Rec_Y_in,Rec_X_in*0,'.b','MarkerSize',10)
for k = 1 : length(i_min)
    i = i_min(k);
    j = j_min(k);
    plot3(x(i,j),y(i,j),x(i,j)*0,'.r','MarkerSize',10)
end




% for k = 1 : length(i_index_inner)
%     i = i_index_inner(k);
%     j = j_index_inner(k);
% end