function y = F(x)
 
global n_element e node n_node n_VariablesNode n_VariablesElement node2element offsetnode offsetElement    VariablesTotalNum
global offset_variables offsetSum
   y = sparse(VariablesTotalNum,1);
    for i = 1 : n_node
            y((i-1) * n_VariablesNode + offsetnode.theta  ,1) = CavitationConstraint(i,x);
%             y((i-1) * n_VariablesNode + offsetnode.Phi,1) = Dissipation(node2element(i),x,i);
        switch node(i).BC
            case 0
            y((i-1) * n_VariablesNode + offsetnode.p  ,1) = VolumeFlowBalance(node2element(i),x,i);
%             y((i-1) * n_VariablesNode + offsetnode.T , 1) = HeatBalance(node2element(i),x,i);
            case 1
            y((i-1) * n_VariablesNode + offsetnode.p , 1) = BoundaryCondition(node(i),x);
%             y((i-1) * n_VariablesNode + offsetnode.T , 1) = BoundaryCondition_T(node(i),x);
            
        end
    end
    
    for i = 1 : n_element
       y(n_node * n_VariablesNode +(i-1)*n_VariablesElement + offsetElement.Qp,1) = Pressureflow(e(i),x);
       y(n_node * n_VariablesNode +(i-1)*n_VariablesElement + offsetElement.Qc,1) = ShearFlow(e(i),node,x);
%        y(n_node * n_VariablesNode +(i-1)*n_VariablesElement + offsetElement.Velocity,1) = Velocity(e(i),x);
%        y(n_node * n_VariablesNode +(i-1)*n_VariablesElement + offsetElement.Velocity,1) = HeatFlow_Konvection(e(i),node,x);
    end
 
end
