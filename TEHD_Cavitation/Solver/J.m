function A = J(x)

global n_element e n_node n_VariablesNode n_VariablesElement  offsetnode offsetElement node2element node VariablesTotalNum

i_index = []; j_index = []; value = [];

    for i = 1 : n_node
        
            [i_index_cav,j_index_cav,J_cav] = JacobianCavitationConstraint(i,x);
%             [i_index_Phi,j_index_Phi,J_Phi] = JacobianDissipation(node2element(i),i,x);
           
        switch node(i).BC
            case 0
            [i_index_BC,j_index_BC,J_BC]    = JacobianVolumeFlowBalance(node2element(i),i,x);
%             [i_index_BC_T,j_index_BC_T,J_BC_T]    = JacobianHeatBalance(node2element(i),i,x);

            case 1
            [i_index_BC,j_index_BC,J_BC]    = JacobianBoundaryCondition(node(i));
%             [i_index_BC_T,j_index_BC_T,J_BC_T]    = JacobianBoundaryCondition_T(node(i));
        end

%         i_index = [i_index; i_index_cav';i_index_BC';i_index_Phi'; i_index_BC_T'];
%         j_index = [j_index; j_index_cav';j_index_BC';j_index_Phi'; j_index_BC_T'];
%         value = [value;J_cav';J_BC';J_Phi'; J_BC_T'];
        i_index = [i_index; i_index_cav';i_index_BC'];
        j_index = [j_index; j_index_cav';j_index_BC'];
        value = [value;J_cav';J_BC'];

    end
   
    for i = 1 : n_element

        [i_index_Qc,j_index_Qc,J_Qc]    = JacobianShearFlow(e(i),node,x);
        [i_index_Qp,j_index_Qp,J_Qp]    = JacobianPressureflow(e(i),x);
%         [i_index_Vel,j_index_Vel,J_Vel]    = JacobianVelocity(e(i),x);
%         [i_index_Tkonv,j_index_Tkonv,J_Tkonv]    = JacobianHeatFlow_Konvection(e(i),node,x);
        
        i_index = [i_index; i_index_Qc';i_index_Qp'];
        j_index = [j_index; j_index_Qc';j_index_Qp'];
        value = [value; J_Qc';J_Qp'];
%         i_index = [i_index; i_index_Qc';i_index_Qp';i_index_Vel';i_index_Tkonv'];
%         j_index = [j_index; j_index_Qc';j_index_Qp';j_index_Vel';j_index_Tkonv'];
%         value = [value; J_Qc';J_Qp';J_Vel';J_Tkonv'];

    end

    A = sparse(i_index,j_index,value,VariablesTotalNum,VariablesTotalNum);
    
end