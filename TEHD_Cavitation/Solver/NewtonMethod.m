function [x_new,xx] = NewtonMethod(funcF,JacobianF,var)

global n_node VariablesTotalNum offsetnode n_VariablesNode  n_element density nue

F = funcF;
J = JacobianF;

x_new = var + 1e-10; % set initial to (0,...,0)      % Initial Condition

Iter = 1;
MaxIter = 15;
TOL = 5e-5;

lamda = zeros(VariablesTotalNum,1) +1;        % relaxation factor    %TODO: Fixed factor? or flexible


    while Iter < MaxIter
        xx(:,Iter) = x_new; 
        x_old = x_new;
        for i = 1 : n_VariablesNode
            if x_old((i-1)* n_VariablesNode +offsetnode.theta,1) >=1
                x_old((i-1)* n_VariablesNode +offsetnode.theta,1) = 1;
            end
        end
        y = - J(x_old)\ F(x_old)  ;
        x_new = x_old + y -(1-lamda).*y; 


                
        [Tol] = Residual(y);
        
        yy(:,Iter) = x_new;

        
        disp(['Iter = ' num2str(Iter) ', Residual = ' num2str(Tol(1)), ' ', num2str(Tol(2))]);
       

        if (Tol(2)<1e-3)|| (Iter >= MaxIter)
            break;
        end
        
        Iter = Iter+1;
    end
    
    if Iter >= MaxIter
    disp('Maximum number of iteration exceeded!');
    end
end



function [Tol] = Residual(y)

global n_node VariablesTotalNum n_VariablesNode offsetnode

    for i = 1 : n_node
         R_p(i)  =y((i-1)*n_VariablesNode+offsetnode.p);
    end
    for i = n_node+1 : VariablesTotalNum
        
          R_Q(i)=y(i);
    end
                 
                 

    Tol(1) = norm(R_p);
    Tol(2) = norm(R_Q);

end
