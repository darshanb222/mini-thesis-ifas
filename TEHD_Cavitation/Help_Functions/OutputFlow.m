function [Qc_out,Qp_out] = OutputFlow(Contour_X,Contour_Y,xx)

global nx node2element
    for i = 1 : length(Contour_X)
        NodeNum = Contour_X(i)+(Contour_Y(i)-1)*nx;
        [sumQc(i), sumQp(i)] = VolumeFlow(node2element(NodeNum),xx);
    end

    Qc_out = sum(sumQc);
    Qp_out = sum(sumQp);

end


function [sumQc, sumQp] = VolumeFlow(node2element,xx)
        
        
global n_VariablesNode offsetElement  n_node n_VariablesElement 

    indexQc.PosX = n_node * n_VariablesNode + (node2element.PosX-1)*n_VariablesElement + offsetElement.Qc;
    indexQc.NegX = n_node * n_VariablesNode + (node2element.NegX-1)*n_VariablesElement + offsetElement.Qc;
    indexQc.PosY = n_node * n_VariablesNode + (node2element.PosY-1)*n_VariablesElement + offsetElement.Qc;
    indexQc.NegY = n_node * n_VariablesNode + (node2element.NegY-1)*n_VariablesElement + offsetElement.Qc;

    indexQp.PosX = n_node * n_VariablesNode + (node2element.PosX-1)*n_VariablesElement + offsetElement.Qp;
    indexQp.NegX = n_node * n_VariablesNode + (node2element.NegX-1)*n_VariablesElement + offsetElement.Qp;
    indexQp.PosY = n_node * n_VariablesNode + (node2element.PosY-1)*n_VariablesElement + offsetElement.Qp;
    indexQp.NegY = n_node * n_VariablesNode + (node2element.NegY-1)*n_VariablesElement + offsetElement.Qp;

    fieldIndexQc = fieldnames(indexQc);

    for i = 1:numel(fieldIndexQc)

        value = indexQc.(fieldIndexQc{i});
        index = value;
        Qc.(fieldIndexQc{i}) = xx(index,1);

        value2 = indexQp.(fieldIndexQc{i});
        index = value2;
        Qp.(fieldIndexQc{i}) = xx(index,1);

    end


    sumQc = -Qc.PosX ...
            +Qc.NegX ...
            -Qc.PosY ...
            +Qc.NegY ;

    sumQp = -Qp.PosX ...
            +Qp.NegX ...
            -Qp.PosY...
            +Qp.NegY;

   
end