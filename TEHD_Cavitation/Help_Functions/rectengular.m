function [i_min,j_min,Index_in,x,y] = rectengular(rec, x, y)

global nx ny

x1_rec = linspace(rec.a/2+rec.cx,-rec.a/2 +rec.cx,200);
y1_rec = linspace(rec.b/2+rec.cy,-rec.b/2 +rec.cy,200);

lin_1_x = x1_rec;
lin_1_y = x1_rec*0 + rec.cy + rec.b/2;

lin_2_x = y1_rec*0 + rec.cx - rec.a/2;
lin_2_y = y1_rec;

lin_3_x = fliplr(x1_rec);
lin_3_y = x1_rec*0 + rec.cy - rec.b/2;

lin_4_x = y1_rec*0 + rec.cx + rec.a/2;
lin_4_y = y1_rec;


Rec_X = [lin_1_x lin_2_x lin_3_x lin_4_x];
Rec_Y = [lin_1_y lin_2_y lin_3_y lin_4_y];

[Index_in] = inpolygon(x,y,Rec_X,Rec_Y);       %   get node in the rectangular

     for k = 1 :  length(Rec_X)

        for i = 1 : nx
            for j = 1 : ny

                squar_dist(i,j) = (Rec_X(k)-x(i,j))^2+(Rec_Y(k)-y(i,j))^2;
            end
        end

        min_dist(k) =  min(min(squar_dist));

        [i_min(k),j_min(k)] = find(squar_dist==min_dist(k),1,'last');

     end

         index = [i_min;j_min];

         index = unique(index','row')';
         i_min = index(1,:);
         j_min = index(2,:);

end