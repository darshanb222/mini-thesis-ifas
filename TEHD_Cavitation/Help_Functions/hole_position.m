function [i_min,j_min,i_index_inner,j_index_inner,x,y] = hole_position(c, x, y)

global nx ny

    theta = linspace(0,2*pi,200);

    x_c = c.r * sin(theta) + c.cx;
    y_c = c.r * cos(theta) + c.cy;

     for k = 1 :  length(x_c)

        for i = 1 : nx
            for j = 1 : ny

                squar_dist(i,j) = (x_c(k)-x(i,j))^2+(y_c(k)-y(i,j))^2;
            end
        end

        min_dist(k) =  min(min(squar_dist));

        [i_min(k),j_min(k)] = find(squar_dist==min_dist(k),1,'last');

     end

         index = [i_min;j_min];

         index = unique(index','row')';
         i_min = index(1,:);
         j_min = index(2,:);
         
    a = c.cx;
    b = c.cy;
    r = c.r;

    for i =1: length(i_min)

    u = x(i_min(i),j_min(i));
    v = y(i_min(i),j_min(i));
    
    x_new(i) = a + r *(u-a)/sqrt((u-a)^2+(v-b)^2); 
    y_new(i) = b + r *(v-b)/sqrt((u-a)^2+(v-b)^2);

%     x(i_min(i),j_min(i)) = x_new(i);
%     y(i_min(i),j_min(i)) = y_new(i);
    end


    %% Node Inner Circle
    k=1;
    for i = 1 : nx
        for j = 1 : ny
            squar_dist = (c.cx - x(i,j))^2+(c.cy - y(i,j))^2;
            if squar_dist <c.r^2

                i_index_inner(k) = i;
                j_index_inner(k) = j;
                k=k+1;
            end
        end
    end

end