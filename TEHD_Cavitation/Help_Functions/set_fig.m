%% Setting the Figure properties

function set_fig(num_Fig, devide_w, devide_h)

    % num_Fig : Max. Figure
    for iFigure=1:num_Fig
        hfig(iFigure) = figure(iFigure);
        set( hfig(iFigure), 'Color', [ 1 1 1 ] ); %,'MenuBar','none'
        gcf = figure(iFigure);
        axes('Parent',gcf,'FontSize',12);
        grid on; % grid minor;
        hold on;
        box on;
    end

%% Locate the Figures

    a = get(0,'ScreenSize');            % get a Screensize

    % devide_w : the number of Figures on horizontal line 
    % devide_h : the number of Figures on vertical line 
 
    fig_width = a(3)/devide_w;          % figure width
    fig_height = a(4)/devide_h;         % figure height
    num_screen = devide_w *devide_h;
    
    loop = fix(num_Fig/num_screen);
    
    
    for h=0:loop
        k = 0;
    for i= 1:devide_h
        for j=1:devide_w
           k = k+1;
           pos_fig(k+h*num_screen,:) = [fig_width*(j-1) fig_height*(devide_h-i) fig_width fig_height];  % get a position of the each figure
        end
    end
    end
    
    for i= 1:num_Fig
        figure(i);
        set(figure(i),'Position',pos_fig(i,:));                                      % set a position of the each figure
    end    

end %end function