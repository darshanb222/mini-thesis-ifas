clear all; clc; close all;

global n_node n_element n_VariablesNode offsetnode n_VariablesElement offset_element VariablesTotalNum offsetElement 
global node e  Trigger nx ny dx dy density nue node2element offset_variables   epsilon h_G omega r_1 del alpha  z Cp v

restoredefaultpath;
addpath('./F_Functions/');      % add function path
addpath('./J_Functions/');      % add function path
addpath('./Help_Functions/');   % add function path
addpath('./Solver/');           % add function path

epsilon = 0.6;
r_1 = 25/2*1e-3;
h_G = 15*1e-6;

B = 60*1e-3;  nx = 50; dx = B/(nx-1);  %B = pi*5;
L = r_1*2*pi; ny = 50; dy = L/(ny-1);

rpm = 800;
omega = -rpm*2*pi/60;  % rotational speed in 1/s(5000)

% Wall Velocity in X direction
v.x.top = 0*omega*r_1;             
v.x.bottom = 0*omega*r_1;  

% Wall Velocity in Y direction
v.y.top = 1*omega*r_1;
v.y.bottom = 0*omega*r_1;             

% Oil: 46 cSt, 900 kg/m3
nue = 46e-6;     % kinematic viscosity
density = 900;   % density
Cp = 2000 /900;      % Heat Capacity, J/kg*K 

del = 1e-2*0; %dx*dy/r_1^2; %(1e-3)*1*1;
z = 4;


%% Equation Parameter

n_node = nx * ny;
n_element = (2*nx-1)*ny;

Trigger.caviation = 'on';
Trigger.Dimension = '2D';

%--------------------------------------------------------------------------
offsetnode.p     = 1;
offsetnode.theta = 2;
% offsetnode.T = 3;

n_VariablesNode = numel(fieldnames(offsetnode));

offset_element = n_node * n_VariablesNode;

%--------------------------------------------------------------------------
offsetElement.Qc = 1;
offsetElement.Qp = 2;
% offsetElement.T_conv = 3;        % Heat Konvection

n_VariablesElement = numel(fieldnames(offsetElement)); 

offset_variables = offset_element + n_element * n_VariablesElement;

%--------------------------------------------------------------------------


VariablesTotalNum = n_node * n_VariablesNode + n_element*n_VariablesElement ;




%% Node Info
for i = 1 : nx
    for j = 1 : ny
        x(i,j) = -B/2 + (i-1)*dx;
        y(i,j) = -L/2 + (j-1)*dy;
        phi(i,j) = y(i,j)/r_1;
        h(i,j) = h_G*(1 + epsilon*cos(phi(i,j) +50*pi/180 ));
        eta(i,j) = nue*density;
        rho(i,j) = density;
        v_x(i,j) = 0*omega*r_1;
        v_y(i,j) = 1*omega*r_1;
        T(i,j) = 40;
        
    end
end

%% Define Objects
pos_A = 0.25;     % in y-direction (0.65)               % ND
Do = 15e-3;       % Diameter of bore A (15e-3)
c1.cx = 0;
c1.cy = 2*pi*r_1*pos_A - L/2;
c1.r = Do/2;

pos_B = 0.75;     % in y-direction (0.45)               % HD
Du = 2e-3;        % Diameter of bore B (3e-3)
c2.cx = 0;
c2.cy = 2*pi*r_1*pos_B - L/2;
c2.r = Du/2;

rec.a = 15e-3;
rec.b = 10e-3;
rec.cx = 0;
rec.cy = 2*pi*r_1*pos_B - L/2;

[c1_i_min,c1_j_min,c1_i_index_inner,c1_j_index_inner,x,y] = hole_position(c1, x, y);
% [c2_i_min,c2_j_min,c2_i_index_inner,c2_j_index_inner,x,y] = hole_position(c2, x, y);
[r1_i_min,r1_j_min,r1_Index_in,x,y] = rectengular(rec, x, y);

set_fig(1,2,1);

figure(1)
grid off
box off
m = mesh(x,y,x*0);
m.FaceColor = [204 204 204]/255;
m.EdgeColor = [0 0 0]/255;
m.FaceAlpha = 0.6;


for k = 1 : length(c1_i_min)
    i = c1_i_min(k);
    j = c1_j_min(k);
    plot3(x(i,j),y(i,j),x(i,j)*0,'.b','MarkerSize',10)
end
% for k = 1 : length(c2_i_min)
%     i = c2_i_min(k);
%     j = c2_j_min(k);
%     plot3(x(i,j),y(i,j),x(i,j)*0,'.r','MarkerSize',10)
% end
for k = 1 : length(r1_i_min)
    i = r1_i_min(k);
    j = r1_j_min(k);
    plot3(x(i,j),y(i,j),x(i,j)*0,'.r','MarkerSize',10)
end

%% Question for next step after Meshing
answer = questdlg('Do you want to continue?','MeshCheck','Yes','No','Yes');
% Handle response
switch answer
    case 'Yes'
        disp([answer '-> Simulation Continue'])
    case 'No'
        disp([answer '-> Simulation Stop'])
    return
end

%% Dimensionless

X = x/B;
Y = y/r_1;

H = h/h_G;


ETA = eta/(nue*density);

%% Boundary Condition

p = zeros(nx,ny) ;
p_c = zeros(nx,ny) ;

factor = zeros(nx,ny)+1;

BC = zeros(nx,ny);

% Edge

p(1,:) = 1/(nue*density*(-r_1*omega)*r_1/(h_G)^2*1e-5);
T(1,:) = 0;
BC(1,:) = 1;

p(nx,:) = 1/(nue*density*(-r_1*omega)*r_1/(h_G)^2*1e-5);
T(nx,:) = 0;
BC(nx,:) = 1;

% 
% % Inlet port
% % 
% for k = 1 : length(c1_i_index_inner)
%     
%     i = c1_i_index_inner(k);
%     j = c1_j_index_inner(k);
%     
%     p(i,j) = 1/(nue*density*(-r_1*omega)*r_1/(h_G)^2*1e-5);
%     T(i,j) = 40;
%     BC(i,j) = 1;
% end
%     
% for k = 1 : length(c1_i_min)
%     
%     i = c1_i_min(k);
%     j = c1_j_min(k);
%     
%     p(i,j) = 1/(nue*density*(-r_1*omega)*r_1/(h_G)^2*1e-5);
%     T(i,j) = 40;
%     BC(i,j) = 1;    
% end
% 
% % Outlet port
% 
% p(r1_Index_in) = 1/(nue*density*(-r_1*omega)*r_1/(h_G)^2*1e-5);
% T(r1_Index_in) = 60;
% BC(r1_Index_in) = 1;
%     
% for k = 1 : length(r1_i_min)
%     
%     i = r1_i_min(k);
%     j = r1_j_min(k);
%     
%     p(i,j) = 1/(nue*density*(-r_1*omega)*r_1/(h_G)^2*1e-5);
%     T(i,j) = 60;
%     BC(i,j) = 1;    
% end

%%
nz = 5;
% x-y coordinate -> 1D coordinate
for k = 1 : nz
    for i = 1 : nx
        for j = 1 : ny
        node(i+(j-1)*nx+ (k-1)*nx*ny).No =  i+(j-1)*nx + (k-1)*nx*ny;   %Node Number
        node(i+(j-1)*nx+ (k-1)*nx*ny).x = X(i,j) ;
        node(i+(j-1)*nx+ (k-1)*nx*ny).y = Y(i,j) ;
        node(i+(j-1)*nx+ (k-1)*nx*ny).h = H(i,j) ;
        node(i+(j-1)*nx+ (k-1)*nx*ny).eta = ETA(i,j);
        node(i+(j-1)*nx+ (k-1)*nx*ny).rho = rho(i,j)/density;
        node(i+(j-1)*nx+ (k-1)*nx*ny).v_x = v_x(i,j)/(r_1*omega);
        node(i+(j-1)*nx+ (k-1)*nx*ny).v_y = v_y(i,j)/(r_1*omega);
        node(i+(j-1)*nx+ (k-1)*nx*ny).p = p(i,j);
        node(i+(j-1)*nx+ (k-1)*nx*ny).BC = BC(i,j);
        node(i+(j-1)*nx+ (k-1)*nx*ny).T = T(i,j);
        end
    end
end 
% Each node has N - equations.

%% Make Element

% X Direction Num_element = (nx-1)*ny
for k = 1 : nz
    for i = 1:nx-1
        for j = 1:ny
        e(i+(j-1)*(nx-1)+(k-1)*ny*(2*nx-1)).No = i+(j-1)*(nx-1) + (k-1)*ny*(2*nx-1);                                 % Element No

        e(i+(j-1)*(nx-1)+(k-1)*ny*(2*nx-1)).NodeA = i+(j-1)*nx + (k-1)*ny*(nx);                                  % Number of NodeA
        e(i+(j-1)*(nx-1)+(k-1)*ny*(2*nx-1)).NodeB = i+(j-1)*nx+1 + (k-1)*ny*(nx);                                % Number of NodeB

        e(i+(j-1)*(nx-1)+(k-1)*ny*(2*nx-1)).length = dx/r_1;                      % TODO: Length and Width must be calculated for irregular mesh
        e(i+(j-1)*(nx-1)+(k-1)*ny*(2*nx-1)).width  = dy/r_1;

        e(i+(j-1)*(nx-1)+(k-1)*ny*(2*nx-1)).direction = 0; % 0 == x direction
        e(i+(j-1)*(nx-1)+(k-1)*ny*(2*nx-1)).layer  = k;  % 1 == y direction
        end
    end
    % Y Direction
    for i = 1:nx
        for j = 1:ny
        e(i+(j-1)*(nx) + (nx-1)*ny +(k-1)*ny*(2*nx-1)).No = i+(j-1)*(nx) + (nx-1)*ny +(k-1)*ny*(2*nx-1);             % Element No

        e(i+(j-1)*(nx) + (nx-1)*ny +(k-1)*ny*(2*nx-1)).NodeA = i+(j-1)*nx +(k-1)*ny*(nx);                        % Number of NodeA

        if mod(i+(j)*nx , n_node)==0
            e(i+(j-1)*(nx) + (nx-1)*ny +(k-1)*ny*(2*nx-1)).NodeB = i+(j)*nx +(k-1)*ny*(nx);                  % Number of NodeB
        else
            e(i+(j-1)*(nx) + (nx-1)*ny +(k-1)*ny*(2*nx-1)).NodeB = mod(i+(j)*nx , n_node) +(k-1)*ny*(nx);
        end

        e(i+(j-1)*(nx) + (nx-1)*ny +(k-1)*ny*(2*nx-1)).length = dy/r_1;
        e(i+(j-1)*(nx) + (nx-1)*ny +(k-1)*ny*(2*nx-1)).width  = dx/r_1;

        e(i+(j-1)*(nx) + (nx-1)*ny +(k-1)*ny*(2*nx-1)).direction  = 1;  % 1 == y direction
        e(i+(j-1)*(nx) + (nx-1)*ny +(k-1)*ny*(2*nx-1)).layer  = k;  % 1 == y direction
        end
    end
end


% Find Element which is connected with node(x)                  
for k = 1 : nz
    for i = 1:nx
        for j = 1:ny

            node2element(i+(j-1)*nx+ (k-1)*nx*ny).PosX = i+(j-1)*nx - floor((i+(j-1)*nx )/nx)  + ny*(2*nx-1)*(k-1);
            node2element(i+(j-1)*nx+ (k-1)*nx*ny).NegX = i-1+(j-1)*nx - floor((i+(j-1)*nx)/nx)  + ny*(2*nx-1)*(k-1);
            node2element(i+(j-1)*nx+ (k-1)*nx*ny).PosY = i+(j-1)*(nx) + (nx-1)*ny  + ny*(2*nx-1)*(k-1);

            if j == 1
                node2element(i+(j-1)*nx+ (k-1)*nx*ny).NegY = i+(j-2)*(nx) + (nx-1)*ny + nx*ny  + ny*(2*nx-1)*(k-1);
            else
                node2element(i+(j-1)*nx+ (k-1)*nx*ny).NegY = i+(j-2)*(nx) + (nx-1)*ny  + ny*(2*nx-1)*(k-1);
            end

        end
    end
end
%% Initialization

var = zeros(VariablesTotalNum,1);
% 
for i = 1 : n_node
    
    var((i-1) * n_VariablesNode + offsetnode.p,1) = node(i).p;
    var((i-1) * n_VariablesNode + offsetnode.theta,1) = 1;

end

%% Make Equations
    
[xx,xy] = NewtonMethod(@F,@J,var);

    
%% Answer to node information
for i = 1 : nx
    for j = 1 : ny
     p(i,j)  =xx((j-1)*nx*n_VariablesNode+(i-1)*n_VariablesNode+offsetnode.p);
     fac(i,j)=xx((j-1)*nx*n_VariablesNode+(i-1)*n_VariablesNode+offsetnode.theta);
    end
end

for i = 1 : nx-1
    for j = 1 : ny
        
     Qc_x(i,j)= xx(offset_element+(i-1+(j-1)*(nx-1))*n_VariablesElement+offsetElement.Qc);
     Qp_x(i,j)= xx(offset_element+(i-1+(j-1)*(nx-1))*n_VariablesElement+offsetElement.Qp);
    end
end

for i = 1 : nx
    for j = 1 : ny
        
     Qc_y(i,j)= xx(offset_element+(i-1+(j-1)*(nx) + (nx-1)*ny)*n_VariablesElement+offsetElement.Qc);
     Qp_y(i,j)= xx(offset_element+(i-1+(j-1)*(nx) + (nx-1)*ny)*n_VariablesElement+offsetElement.Qp);
    end
end


% Answer to node
for k = 1 : nz
    for i = 1 : nx
        for j = 1 : ny
        node(i+(j-1)*nx+ (k-1)*nx*ny).p = p(i,j)*nue*density*(-r_1*omega)*r_1/(h_G)^2;
        end
    end
end 

for i = 1 : (2*nx-1)*ny*nz
    
    p1 = node(e(i).NodeA).p;
    p2 = node(e(i).NodeB).p;

    h1 = node(e(i).NodeA).h *h_G;
    h2 = node(e(i).NodeB).h *h_G;

    l = e(i).length;
    
    layer = e(i).layer;
    h = (h1+h2)*0.5;
    
    dz = h/(nz-1);
    
    z = (layer-1)*dz;
    if e(i).direction == 0
        e(i).u = 1/(nue*density) *(p1-p2)/dx*0.5*(z^2-z*h) + v.x.bottom*(1-z/h) + v.x.top*z/h;
        e(i).v = 0;
    elseif e(i).direction == 1
        e(i).u = 0;
        e(i).v = 1/(nue*density) *(p1-p2)/dy*0.5*(z^2-z*h) + v.y.bottom*(1-z/h) + v.y.top*z/h;
    end

end

for k = 1 : nz
for i = 1:nx-1
    for j = 1:ny

        Ux(i,j,k) = e(i+(j-1)*(nx-1)+(k-1)*ny*(2*nx-1)).u;
    end
end
end
    
for k = 1 : nz
for i = 1:nx
    for j = 1:ny

        Vx(i,j,k) = e(i+(j-1)*(nx) + (nx-1)*ny +(k-1)*ny*(2*nx-1)).v;
    end
end
end





[Qc_out,Qp_out] = OutputFlow(r1_i_min,r1_j_min,xx);
Qp_y(1,:);
%% Figure
set_fig(7, 3, 3)
figure(1)
surf(X,Y/pi*180,H)
% view([-45 -45 50])
title('Gap Height');
xlabel('x axis'); ylabel('y axis');
% shading interp
view([90 0 0])

figure(2)
s = surf(X,Y/pi*180,p*nue*density*(-r_1*omega)*r_1/(h_G)^2*1e-5);
% view([-45 -45 50])
view(83,2)
s.FaceAlpha = 0.6;
title('Pressure Field');
xlabel('x axis'); ylabel('y axis');

for k = 1 : length(c1_i_min)
    
    i = c1_i_min(k);
    j = c1_j_min(k);
    
    plot3(X(i,j),Y(i,j)/pi*180,p(i,j)*nue*density*(-r_1*omega)*r_1/(h_G)^2*1e-5,'.g')
    
end
% for k = 1 : length(c2_i_min)
%     
%     i = c2_i_min(k);
%     j = c2_j_min(k);
%     
%     plot3(X(i,j),Y(i,j)/pi*180,p(i,j)*nue*density*(-r_1*omega)*r_1/(h_G)^2*1e-5,'.r')
%     
% end

% shading interp
% xlim([-8,8]); set(gca,'XTick',[-8 : 1 : 8]); xlabel('x axis');
% ylim([-8,8]); set(gca,'YTick',[-8 : 1 : 8]); ylabel('y axis');
figure(3)
% s = surf(X,Y/pi*180,fac);
s = surf(X,Y/pi*180,-(fac-1));
view(90,-90)

title('Film Fraction');
xlabel('x axis'); ylabel('y axis');
s.FaceLighting = 'gouraud';

% shading interp
zlim([0,1]); set(gca,'ZTick',[0 : 0.1 : 1]);
cbar = colorbar;
cbar.Limits = [0 1];
colormap jet


% figure(4)
% plot(Y(nx/2,:),p(nx/2,:)*nue*density*(-r_1*omega)*r_1/(h_G)^2*1e-5)
% title('Pressure Distribution at midlane');

figure(5)
title('Qp in x-direction');

m = mesh(Qp_x);
m.FaceAlpha = 0.5;
view(-40,60)

% quiv = quiver(x(1:2:80,1:5:150),y(1:2:80,1:5:150),Qp_x(1:2:80,1:5:150),Qp_y(1:2:80,1:5:150));
% quiv.MaxHeadSize = 0.5;
% quiv.AutoScaleFactor = 2;

% hold on;


figure(6)
title('Qp in y-direction');
mesh(Qp_y)
view(-40,60)

% quiv = quiver(y(1:2:78,1:10:150),x(1:2:78,1:10:150),Qc_y(1:2:78,1:10:150),Qc_x(1:2:78,1:10:150));
% quiv.MaxHeadSize = 0.1;
% quiv.AutoScaleFactor = 2;

figure(7)
mesh(T * r_1*nue*density/(h_G^2*density*Cp))
title('Temperature');
view(-40,60)

figure(8)
for k = 1 : nz
hold on;

mesh(Vx(:,:,k))
end
figure(9)

for k = 1 : nz
    for j = 1 : ny
        V(k,j) = Vx(25,j,k);
        hold on;
        plot(V(:,j))
        
    end
end
figure(10)
for k = 1 : nz
hold on;

mesh(Ux(:,:,k))
U(k) = Ux(25,25,k);
end
figure(11)
plot(U)

